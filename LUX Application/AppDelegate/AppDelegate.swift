import GoogleSignIn//
//  AppDelegate.swift
//  LUX Application
//
//  Created by Yogita on 30/01/18.
//  Copyright © 2018 WildnetTechnologies. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import GoogleSignIn
import Fabric
import Crashlytics
import GoogleMaps
import GooglePlaces
import UserNotifications
import Firebase
import Stripe

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate  {
    var window: UIWindow?
    var userLocation : CLLocation?
    var userAddress : String?
    var fromAddress : String?
    var toAddress : String?
    var fromLocation : CLLocation?
    var toLocation : CLLocation?
    var numberOfPassenger : String?
    var getDrivers = false
    var timer = Timer()
    var token : String?
    var isTimerStarted = false
    var timeRemaining : String?
    var distanceRemaining : Int?
    var expireTimer : Timer?
    var swiftySideMenuVC : SwiftySideMenuViewController?
    var isLive = 1

    var navigationController = UINavigationController()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        ConnectionManager.sharedInstance.observeReachability()
        configureStripe()
        Fabric.with([Crashlytics.self])
        FIRApp.configure()
        GMSPlacesClient.provideAPIKey(googleKEY)
        GMSServices.provideAPIKey("AIzaSyDm5YMCSoGGj9zBu0FnOw6RDoAMSYWTnXc")//"AIzaSyC8VQ2AcreL3Y6XoGZU_bF2JySLjxmJ42w")
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        setNavigationController()
        UIApplication.shared.statusBarStyle = .lightContent
        GIDSignIn.sharedInstance().clientID =  "260468840011-c81gr5id1bnje9kjq0dpn3j18hp3ph4v.apps.googleusercontent.com"//"1099500316187-1c3hbcep51mlr987g3utgs1qgivim4lf.apps.googleusercontent.com"
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let loginVC : LoginViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        let navigationController = UINavigationController(rootViewController: loginVC)
        navigationController.navigationBar.isHidden = true
        navigationController.interactivePopGestureRecognizer?.isEnabled = false
        self.timer = Timer.scheduledTimer(timeInterval: TimeInterval(3), target: self, selector: #selector(self.updateUserLocation), userInfo: nil, repeats: true)
        setupGoogleMapsAPIKeys()
        
        // iOS 10 support
        if #available(iOS 10, *) {
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            application.registerForRemoteNotifications()
             UNUserNotificationCenter.current().delegate = self
        }
            // iOS 9 support
        else if #available(iOS 9, *) {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
            // iOS 8 support
        else if #available(iOS 8, *) {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
            // iOS 7 support
        else {
            application.registerForRemoteNotifications(matching: [.badge, .sound, .alert])
        }
         UIApplication.shared.applicationIconBadgeNumber = 0
         UIApplication.shared.statusBarStyle = .lightContent
        return true
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation) || GIDSignIn.sharedInstance().handle(url,                                                                                                                                                                                    sourceApplication: sourceApplication,                                                                                                                                                   annotation: annotation)
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        return  FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String, annotation: options[UIApplicationOpenURLOptionsKey.annotation]) || GIDSignIn.sharedInstance().handle(url,
                                                 sourceApplication:options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                 annotation: [:])
    }
    
    // Called when APNs has assigned the device a unique token
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // Convert token to string
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        token = deviceTokenString
        // Print it to console
      //  print("APNs device token: \(deviceTokenString)")
        
        // Persist it in your backend in case it's new
    }
    
    // Push notification received
    func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any]) {
        // Print notification payload data
        print("Push notification received: \(data)")
    }
    
    // Called when APNs failed to register the device for push notifications
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // Print the error to console (you should alert the user that registration failed)
        print("APNs registration failed: \(error)")
    }
   
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
   
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        NotificationCenter.default.post(name: Notification.Name(K.KEY.refreshBookingData), object: nil)
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func setNavigationController()
    {
        window = UIWindow(frame: UIScreen.main.bounds)
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let loginVC : LoginViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        let homeVC : HomeViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let mainController : UIViewController
        if UserDefaultManager.getIsLogin()!
        {
            mainController = homeVC
        }
        else
        {
            mainController = loginVC
        }
        let navigationController = UINavigationController(rootViewController: mainController)
        navigationController.navigationBar.isHidden = true
        navigationController.interactivePopGestureRecognizer?.isEnabled = false
        swiftySideMenuVC = storyboard.instantiateViewController(withIdentifier: "SwiftySideMenuViewController") as! SwiftySideMenuViewController
        let leftVC = storyboard.instantiateViewController(withIdentifier: "SideMenuViewController")  as! SideMenuViewController
        swiftySideMenuVC?.centerViewController = navigationController
        swiftySideMenuVC?.leftViewController = leftVC
        self.window?.rootViewController = swiftySideMenuVC
        self.window?.makeKeyAndVisible()
    }
    
    func setupGoogleMapsAPIKeys()
    {
        GMSServices.provideAPIKey("AIzaSyDEIbZSHmKJLyrZ4gVLbAcvkBQWM97BmhE")
    }
    
    //UpdateDeviceToken
    
    @objc func updateUserLocation()
    {
        if (userLocation != nil && UserDefaultManager.getIsLogin()!){
            let params : [String:AnyObject] = ["user_lat" :  appDelegate.userLocation?.coordinate.latitude as AnyObject,"user_long" : appDelegate.userLocation?.coordinate.longitude as AnyObject,"socket_status" : 1 as AnyObject]
            WebServiceHelper.sharedInstance.postFormDataWithHeader(input: params as AnyObject, image: nil, completeUrl: K.URL.BaseUrl + K.URL.RiderLocation) { (success, response) in
                if(success == true){
                    guard let response = response else {
                        return
                    }
                    guard let dic = response as? NSDictionary else {
                        return
                    }
                    guard let status = dic["status"] else {
                        return
                    }
                    if ((status as! String) == "success")
                    {
                       
                    }
                    else{
                       
                        guard let status = dic["status"] else {
                            return
                        }
                        guard let msg = dic["message"] else {
                            return
                        }
                        guard let code = dic["code"] else {
                            return
                        }
                        if (UserDefaultManager.getIsLogin()!){
                            if (code as? Int) == 420{
                                if let nav = self.swiftySideMenuVC?.centerViewController as? UINavigationController{
                                    if  let vc = (nav as? UINavigationController)?.viewControllers.last {
                                        Utility.logOut(vc:vc, message: msg as! String)
                                    }
                                }
                                return
                            }
                        }
                    }
                  
                }
                else{
                    
                   
                }
                
            }
        }
        
    }
   
    func updateDeviceToken(deviceToken:String)
    {
        if (UserDefaultManager.getIsLogin()!){
            let params : [String:String] = ["device_token" :  deviceToken, "isdevice" : "1","login_type" : "rider" ]
            WebServiceHelper.sharedInstance.postFormDataWithHeader(input: params as AnyObject, image: nil, completeUrl: K.URL.BaseUrl + K.URL.UpdateDeviceToken) { (success, response) in
                if(success == true){
                    guard let response = response else {
                        return
                    }
                    guard let dic = response as? NSDictionary else {
                        return
                    }
                    guard let status = dic["status"] else {
                        return
                    }
                    if ((status as! String) == "success")
                    {
                        
                    }
                    else{
                    }
                    
                }
                
            }
        }
        
    }

}

@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        print(userInfo)
        if let nc =  swiftySideMenuVC?.centerViewController{
            if (nc as? UINavigationController)?.viewControllers.last is HomeViewController{
                NotificationCenter.default.post(name: Notification.Name(K.KEY.refreshBookingData), object: nil)
            }
        }
        
        // Print message ID.
        // Print full message.
      //  print(userInfo)
        // Change this to your preferred presentation option
        completionHandler([.alert,.sound,.badge])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
      // let userInfo = response.notification.request.content.userInfo
        completionHandler()
}
    
    
    func configureStripe()
    {
        if(self.isLive == 1){
            STPPaymentConfiguration.shared().publishableKey = K.KEY.stripeLiveKey
        }
        else{
            STPPaymentConfiguration.shared().publishableKey = K.KEY.stripeTestKey
        }
    }
}


