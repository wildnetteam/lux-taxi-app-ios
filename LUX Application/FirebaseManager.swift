//
//  FirebaseManager.swift
//  GoodKarma
//
//  Created by Anuj Jha on 03/11/17.
//  Copyright © 2017 Teclalabs. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase

typealias completion = (_ info:NSDictionary)->Void
protocol FireBaseManagerDelegate {
    func driverDidMoveWith(lat:Double,long:Double)
}

class FireBaseManager: NSObject {
    static let defaultManager = FireBaseManager()
    var  delegate : FireBaseManagerDelegate?
    let bookingIDlocRef: FIRDatabaseReference = FIRDatabase.database().reference().child("bookingIDs")
  
    func getDriverLatLong(bookingId:String,completionBlock:@escaping completion){
       bookingIDlocRef.child(bookingId).child("Driver").child("location").observe(.value, with: { (snapshot) in
            if let driverData = snapshot.value, !(driverData is NSNull) {
                let driverData = snapshot.value as! NSDictionary
                completionBlock(driverData as NSDictionary)
            }
        })
    }
    
    func removeObservers(bookingId:String){
        bookingIDlocRef.child(bookingId).child("Driver").child("location").removeAllObservers()
    }
}


