//
//  SearchTableViewCell.swift
//  LUX Application
//
//  Created by Yogita on 15/02/18.
//  Copyright © 2018 WildnetTechnologies. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {

    @IBOutlet weak var primaryLabel: UILabel!
    @IBOutlet weak var secondaryLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    func setData(primaryAdd:String,secondaryAdd:String)
    {
        self.primaryLabel.text = primaryAdd
        self.secondaryLabel.text = secondaryAdd

    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
