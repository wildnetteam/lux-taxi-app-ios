//
//  SearchViewController.swift
//  LUX Application
//
//  Created by Yogita on 15/02/18.
//  Copyright © 2018 WildnetTechnologies. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class SearchViewController: UIViewController {
    
    var resultsArray = [String]()
    var primaryAddArray = [String]()
    var secondaryAddArray = [String]()
    var searchResults: [String]!
    var selectedPassenger = -1
    
    var activeTextField : UITextField?
    var activeText = 1

    @IBOutlet weak var datePickerBottomLayoutConstraint: NSLayoutConstraint!

    @IBOutlet weak var locView: UIView!
    @IBOutlet var locationTableView: UITableView!
    @IBOutlet var textFieldFromLocation: UITextField!
    @IBOutlet var textFieldToLocation: UITextField!
    @IBOutlet weak var textFieldPassengers: UITextField!
    @IBOutlet weak var pickerBG: UIView!
    @IBOutlet weak var pickerView: UIPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchResults = Array()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setUpUIElements()
       
    }
    
    // MARK: - setUpUIElements

    func setUpUIElements()
    {
        Utility.addBottomShadow(view: locView)
        setUpData()
        if (activeText == 1){
         activeTextField = textFieldFromLocation
        self.activeTextField?.becomeFirstResponder()
        }
        if (activeText == 2){
        activeTextField = textFieldToLocation
        self.activeTextField?.becomeFirstResponder()
        }
        if (activeText == 3){
            self.showPicker()
        }
        
        /*if(textFieldToLocation.text?.characters.count == 0)
        {
            activeTextField = textFieldToLocation
        }
        else if(textFieldFromLocation.text?.characters.count == 0)
        {
            activeTextField = textFieldFromLocation
        }
        else if(textFieldPassengers.text?.characters.count == 0)
        {
            self.showPicker()
        }
        self.activeTextField?.becomeFirstResponder()*/
        
    }

    func setUpData()
    {
        if  let toAddress = appDelegate.fromAddress
        {
            textFieldFromLocation.text = toAddress
        }
        else if let userAddress = appDelegate.userAddress
        {
            textFieldFromLocation.text = userAddress
        }
        textFieldToLocation.text = appDelegate.toAddress
        textFieldPassengers.text = appDelegate.numberOfPassenger
    }

    // MARK: - IBAction

    @IBAction func noOfPassengersButtonTapped(_ sender: UIButton) {
      showPicker()
    }
    
    @IBAction func backButtonTapped(sender: UIButton) {
        Utility.popViewController(fromVC: self)
    }
    
    @IBAction func doneButtonTapped(_ sender: UIButton) {
       if(selectedPassenger != -1)
       {
        textFieldPassengers.text = passengerArr[selectedPassenger]
        }
        else{
        selectedPassenger = 0
        textFieldPassengers.text = passengerArr[selectedPassenger]
        }
        self.datePickerBottomLayoutConstraint.constant = 0
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: Double(0.5), animations: {
            self.datePickerBottomLayoutConstraint.constant = -self.pickerView.frame.size.height
            self.view.layoutIfNeeded()
        })
        { (true) in
            self.pickerBG.isHidden = true
            appDelegate.numberOfPassenger = self.textFieldPassengers.text
            if(((self.textFieldPassengers.text?.characters.count)! > 0) && ((self.textFieldFromLocation.text?.characters.count)! > 0) && ((self.textFieldToLocation.text?.characters.count)! > 0))
            {
                appDelegate.getDrivers = true
                Utility.popViewController(fromVC: self)
            }
        }
    }
    
    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        self.datePickerBottomLayoutConstraint.constant = 0
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: Double(0.5), animations: {
            self.datePickerBottomLayoutConstraint.constant = -self.pickerView.frame.size.height
            self.view.layoutIfNeeded()
        })
        { (true) in
            self.pickerBG.isHidden = true
        }
    }
    
    func showPicker()
    {
        self.view.endEditing(true)
        self.pickerBG.isHidden = false
        self.datePickerBottomLayoutConstraint.constant = -self.pickerView.frame.size.height
        self.view.layoutIfNeeded()
        
        UIView.animate(withDuration: Double(0.5), animations: {
            self.datePickerBottomLayoutConstraint.constant = 0
            self.view.layoutIfNeeded()
        })
    }
    
}

extension SearchViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let correctedAddress:String! = self.searchResults[indexPath.row].addingPercentEncoding(withAllowedCharacters: .symbols)
        Utility.showIndicator()
        let url = NSURL(string: "https://maps.googleapis.com/maps/api/geocode/json?address=" + correctedAddress + "&key=" + K.KEY.GoogleAPIKey)
        let task = URLSession.shared.dataTask(with: url! as URL) { (data, response, error) -> Void in
            // 3
            do {
                if data != nil{
                    let dic = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves) as! NSDictionary
                    print(dic)
                    var lat = 0.0
                    var lon = 0.0

                     let latArr = (((((dic["results"] as AnyObject).value(forKey: "geometry") as AnyObject).value(forKey: "location") as AnyObject).value(forKey: "lat") as AnyObject)as! NSArray)
                        if latArr.count > 0
                        {
                         lat = latArr[0] as! Double
                        }
                   
                    let lonArr = (((((dic["results"] as AnyObject).value(forKey: "geometry") as AnyObject).value(forKey: "location") as AnyObject).value(forKey: "lng") as AnyObject) as! NSArray)
                    if lonArr.count > 0
                    {
                      lon = lonArr[0] as! Double
                    }
                   let location = CLLocation(latitude:lat , longitude: lon)
                    DispatchQueue.main.async(execute: {
                        self.activeTextField?.text = self.searchResults[indexPath.row]
                        Utility.hideIndicator()
                    }
                    )
                    self.setAddress(location: location)
                    
                    
                }
                
            }catch {
                print("Error")
            }
           
        }
        
        // 5
        task.resume()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.searchResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchTableViewCell")
            as! SearchTableViewCell
        cell.selectionStyle = .none
        cell.contentView.backgroundColor = UIColor.clear
        cell.backgroundView?.backgroundColor = UIColor.clear
        cell.backgroundColor = UIColor.clear
        cell.setData(primaryAdd: self.primaryAddArray[indexPath.row], secondaryAdd: self.secondaryAddArray[indexPath.row])
        return cell
    }
    
    func reloadDataWithArray(array:[String],primaryArr:[String],secondaryArr:[String]){
        self.searchResults = array
        self.locationTableView.reloadData()
    }
    
    func jumpToNextTextfield()
    {
        if(((self.textFieldPassengers.text?.characters.count)! > 0) && ((self.textFieldFromLocation.text?.characters.count)! > 0) && ((self.textFieldToLocation.text?.characters.count)! > 0))
        {
            DispatchQueue.main.async {
                appDelegate.getDrivers = true
                Utility.popViewController(fromVC: self)
            }
        }
        else{
        DispatchQueue.main.async(execute: {
            if(self.activeTextField == self.textFieldFromLocation)
            {
                self.activeTextField = self.textFieldToLocation
                self.activeTextField?.becomeFirstResponder()
            }
            else if(self.activeTextField == self.textFieldToLocation)
            {
                self.activeTextField?.resignFirstResponder()
                self.activeTextField = nil
                self.showPicker()
            }
        })
    }
       
    }
    func setAddress(location:CLLocation)
    {
            DispatchQueue.main.async(execute: {
                if(self.activeTextField == self.textFieldFromLocation)
                {
                    appDelegate.fromAddress = self.activeTextField?.text
                    appDelegate.fromLocation = location
                    self.jumpToNextTextfield()
                }
                else if(self.activeTextField == self.textFieldToLocation)
                {
                    if(appDelegate.fromLocation != nil){
                        if(Double((appDelegate.fromLocation?.distance(from: location))!) > 50.0){
                            appDelegate.toAddress = self.activeTextField?.text
                            appDelegate.toLocation = location
                            self.jumpToNextTextfield()
                        }
                        else{
                            Utility.showAlertWithMessage(vc: self, message: "Pickup and Drop cannot be same.")
                        }
                    }
                    else{
                        if(Double((appDelegate.userLocation?.distance(from: location))!) > 50.0){
                            appDelegate.toAddress = self.activeTextField?.text
                            appDelegate.toLocation = location
                            self.jumpToNextTextfield()
                        }
                        else{
                            Utility.showAlertWithMessage(vc: self, message: "Pickup and Drop cannot be same.")
                        }
                    }
                    
                }
            })
        
        }
        
    }
    
    

extension SearchViewController:UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = ""
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (!(isBackSpace == -92 && textField.text?.characters.count == 1))
        {
            activeTextField = textField
            let filter = GMSAutocompleteFilter()
            //filter.type = .address
            filter.country = "US"
            let placesClient = GMSPlacesClient()
            placesClient.autocompleteQuery(textField.text! + string , bounds: nil, filter: filter)
            { (results, error) in
                self.primaryAddArray.removeAll()
                self.secondaryAddArray.removeAll()
                self.resultsArray.removeAll()
                if results == nil {
                    return
                }
                
                for result in results!{
                    if let result = result as? GMSAutocompletePrediction{
                         self.reloadDataWithArray(array: self.resultsArray,primaryArr: self.primaryAddArray,secondaryArr: self.secondaryAddArray)
                        self.resultsArray.append(result.attributedFullText.string)
                        self.primaryAddArray.append(result.attributedPrimaryText.string)
                        if let secondAdd = result.attributedSecondaryText
                        {
                            self.secondaryAddArray.append((secondAdd.string))
                        }
                        else
                        {
                            self.secondaryAddArray.append("")
                            
                        }
                        
                    }
                }
        }
           
        }
        else
        {
        self.primaryAddArray.removeAll()
        self.secondaryAddArray.removeAll()
        self.resultsArray.removeAll()
         self.reloadDataWithArray(array: self.resultsArray,primaryArr: self.primaryAddArray,secondaryArr: self.secondaryAddArray)
        }
        
        return true
    }
}

extension SearchViewController:UIScrollViewDelegate{
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.activeTextField?.resignFirstResponder()
    }
}

extension SearchViewController:UIPickerViewDataSource,UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return passengerArr.count;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
      return passengerArr[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedPassenger = row
    }
}
