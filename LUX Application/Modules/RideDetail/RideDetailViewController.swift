//
//  RideDetailViewController.swift
//  LUX Application
//
//  Created by Yogita on 19/02/18.
//  Copyright © 2018 WildnetTechnologies. All rights reserved.
//

import UIKit
import HCSStarRatingView

class RideDetailViewController: UIViewController {

    var driverViewModel :  DriverListViewModel?

    @IBOutlet weak var toLocationLabel: UILabel!
    @IBOutlet weak var fromLocationLabel: UILabel!
    @IBOutlet weak var riderImageView: UIImageView!
    @IBOutlet weak var ratingView: HCSStarRatingView!
    @IBOutlet weak var driverName: UILabel!
    @IBOutlet weak var carDescription: UILabel!
    @IBOutlet weak var fromLocation: UITextField!
    @IBOutlet weak var toLocation: UITextField!
    @IBOutlet weak var numberOfPassengers: UITextField!
    @IBOutlet weak var servicesTextField: UILabel!
    @IBOutlet weak var totalDistanceTextField: UITextField!
    
    @IBOutlet weak var totalPriceLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        Utility.roundView(view: self.riderImageView, radius: self.riderImageView.frame.size.width/2)
        setData()

    }
    
    override func viewWillAppear(_ animated: Bool) {
     
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func confirmBookingButton(_ sender: UIButton) {
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        Utility.popViewController(fromVC: self)
    }
    
    func setData()
    {
        if driverViewModel != nil
        {
            if let image = driverViewModel?.image
            {
                if let url = URL(string : image)
                {
                    self.riderImageView.setImageWith(url, placeholderImage: #imageLiteral(resourceName: "avatar-placeholder"))
                }
            }
            if let price = driverViewModel?.totalPrice{
                self.totalPriceLabel.text = "$" + price
            }
            
            if let dist = driverViewModel?.totalDistance{
                self.totalDistanceTextField.text = dist
            }
            self.servicesTextField.text = driverViewModel?.amenities
            self.driverName.text = driverViewModel?.driverName
            if let rate = driverViewModel?.rating{
                if let n = NumberFormatter().number(from: rate) {
                    ratingView.value = CGFloat(truncating: n)
                }
            }
            
            self.carDescription.text = driverViewModel?.carDetails
            guard let toAdd = appDelegate.toAddress else {
                return
            }
            self.toLocationLabel.text = toAdd
            //self.toLocation.text = toAdd
            guard let fromAdd = appDelegate.fromAddress else {
                return
            }
            self.fromLocationLabel.text = fromAdd
            //self.fromLocation.text = fromAdd
            guard let noOfPassenger = appDelegate.numberOfPassenger else {
                return
            }
            //totalDistanceTextField
            self.numberOfPassengers.text = noOfPassenger
            
        }
    }
    
   
   
    @IBAction func confirmBookingButtonTapped(_ sender: UIButton) {
        guard let pickUpLocation = appDelegate.fromAddress else {
            return
        }
        guard let dropLocation = appDelegate.toAddress else {
            return
        }
        guard let fromLocation = appDelegate.fromLocation else {
            return
        }
        guard let toLocation = appDelegate.toLocation else {
            return
        }
        guard let noOfPassenger = numberOfPassengers.text else {
            return
        }
        guard let driverId = driverViewModel?.driverId else {
            return
        }
       /* guard let TotalDistance = driverViewModel?.totalDistance else {
            return
        }
        
        guard let TotalTime = driverViewModel?.totalTime else {
            return
        }*/
         var TotalDistance = "0"
        if let distance = driverViewModel?.totalDistance{
            TotalDistance = distance
        }
        
        var TotalTime = "0"
        if let time = driverViewModel?.totalTime{
            TotalTime = time
        }
        
        guard let RateId = driverViewModel?.rateID else {
            return
        }
        guard let totalAmount = driverViewModel?.totalPrice else {
            return
        }
        
        var toLat = ""
        var fromLat = ""
        var toLong = ""
        var fromLong = ""
        
        fromLat = String(describing: fromLocation.coordinate.latitude)
        fromLong = String(describing: fromLocation.coordinate.longitude)
        
        toLat = String(describing: toLocation.coordinate.latitude)
        toLong = String(describing: toLocation.coordinate.longitude)
        
        let parameters : [String:String] = ["pickup_lat" :fromLat,"pickup_long":fromLong,"passenger_no":noOfPassenger,"drop_lat":toLat,"drop_long":toLong,"driver_id":driverId,"approx_distance":TotalDistance,"approx_time":TotalTime,"device_token":"","ratecard_id":RateId,"drop_area":dropLocation,"pickup_area":pickUpLocation,"amount":totalAmount]
        ConfirmBookingManager.bookATaxiAPI(parameters: parameters as [String : AnyObject], vc: self) { (success) in
            self.navigationController?.popViewController(animated: true)
        }

    
    }
    
}
