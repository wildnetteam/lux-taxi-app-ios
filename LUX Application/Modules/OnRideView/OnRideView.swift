//
// WaitingView.swift
//
//
//

import UIKit
import HCSStarRatingView

protocol OnRideViewDelegate : class {
    func cancelRideButtontapped()
    func shareButtontapped()
    func callButtontapped()
    func payNowButtontapped()
}
class OnRideView: UIView {
    weak var delegate : OnRideViewDelegate?
    
    @IBOutlet weak var ratingViewHeight: NSLayoutConstraint!
    @IBOutlet weak var ratingViewWidth: NSLayoutConstraint!
    @IBOutlet weak var carDetailBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var userImageTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var phoneNumber: UILabel!
    @IBOutlet weak var carDetailsLabel: UILabel!
    @IBOutlet weak var passengersLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var ratingView: HCSStarRatingView!
    @IBOutlet weak var cancelbutton: UIButton!
    @IBOutlet weak var cancelRideLabel: UILabel!
    @IBOutlet weak var driverDistance: UILabel!
    @IBOutlet weak var cancelRideImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var payNowButton: UIButton!
    @IBOutlet weak var shareDetailsLabel: UILabel!
    @IBOutlet weak var callLabel: UILabel!
    class func instanceFromNib() -> OnRideView
    {
        let view = UINib(nibName: "OnRideView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! OnRideView

        return view
    }
    
    
    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        self.delegate?.cancelRideButtontapped()
    }
    @IBAction func shareDetailsButtonTapped(_ sender: UIButton) {
        self.delegate?.shareButtontapped()
    }
    @IBAction func callButtonTapped(_ sender: UIButton) {
        self.delegate?.callButtontapped()
    }
    
    @IBAction func payNowButtonTapped(_ sender: UIButton) {
        self.delegate?.payNowButtontapped()
    }
    
    @objc func setData(notification: Notification){
        if(appDelegate.distanceRemaining != nil){
            if(appDelegate.distanceRemaining == 0)
            {
              self.driverDistance.text = "Driver is arriving shortly"
            }
            else
            {
               self.driverDistance.text = "Driver is " +  String(appDelegate.distanceRemaining!) + " miles away"
            }
           
        }
    }

    func setData(model:WaitingViewModel,status:String?)
    {
        NotificationCenter.default.addObserver(self, selector: #selector(self.setData(notification:)), name: Notification.Name("updateDistance"), object: nil)

        if UIScreen.main.sizeType == .iPhone5 {
            ratingViewHeight.constant = 10
            ratingViewWidth.constant = 60
           
            carDetailBottomConstraint.constant = 5
            shareDetailsLabel.font = UIFont(name: "GothamRounded-Book", size: 12)
            cancelRideLabel.font = UIFont(name: "GothamRounded-Book", size: 12)
            callLabel.font = UIFont(name: "GothamRounded-Book", size: 12)
        }
        self.layoutIfNeeded()
         userImageTopConstraint.constant = -(self.userImage.frame.size.width/2)
        Utility.roundView(view: self.userImage, radius: ceil(self.userImage.frame.size.width/2))
      


        if let bookingStatus =  status{
            if bookingStatus == K.STATUS.ONPICKUP {
                self.driverDistance.text = "Driver has arrived"
                self.cancelbutton.isUserInteractionEnabled = true
                self.cancelRideLabel.textColor = UIColor.white
                self.cancelRideImage.image = #imageLiteral(resourceName: "cancelride")
                self.payNowButton.isHidden = true
                self.buttonHeightConstraint.constant = 0
                self.layoutIfNeeded()
            }
            
            else if bookingStatus == K.STATUS.ACCEPTED{
                self.cancelRideImage.image = #imageLiteral(resourceName: "cancelride")
                self.driverDistance.text = "Driver is arriving shortly"
                self.cancelbutton.isUserInteractionEnabled = true
                self.payNowButton.isHidden = true
                self.cancelRideLabel.textColor = UIColor.white
                self.buttonHeightConstraint.constant = 0
                self.layoutIfNeeded()
                
            }
            else if bookingStatus == K.STATUS.ONTRIP{
                self.driverDistance.text = ""
                self.cancelRideImage.image = #imageLiteral(resourceName: "CancelDisabled")
                self.payNowButton.isHidden = true
                self.cancelbutton.isUserInteractionEnabled = false
                self.cancelRideLabel.textColor = K.COLOR.disabledColor
                self.buttonHeightConstraint.constant = 0
                self.layoutIfNeeded()
            }
        }
        carDetailsLabel.text = model.carDetails
        priceLabel.text = model.totalPrice
        if let image = model.image
        {
            if let url = URL(string : image)
            {
                self.userImage.setImageWith(url, placeholderImage: #imageLiteral(resourceName: "avatar-placeholder"))
            }
        }
        if let rate = model.rating{
            ratingView.value = rate
        }
        userName.text = model.driverName
        phoneNumber.text = model.phone
        passengersLabel.text = model.person
    }
}
