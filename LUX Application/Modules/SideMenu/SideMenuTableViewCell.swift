//
//  SideMenuTableViewCell.swift
//  LUX Application
//
//  Created by Yogita on 08/02/18.
//  Copyright © 2018 WildnetTechnologies. All rights reserved.
//

import UIKit

class SideMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var sideImageView: UIImageView!
    @IBOutlet weak var sideTitleLabel: UILabel!
    
    let imageArr = [#imageLiteral(resourceName: "bookyourtrip"),#imageLiteral(resourceName: "trips"),#imageLiteral(resourceName: "tc")]
    let titleArr = ["Book your trip", "Ride History", "Legal Information","Contact us"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(index:Int)
    {
        self.sideImageView.image = imageArr[index]
        self.sideTitleLabel.text = titleArr[index]
    }

}
