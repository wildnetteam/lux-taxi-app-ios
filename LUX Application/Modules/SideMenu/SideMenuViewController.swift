//
//  SideMenuViewController.swift
//  LUX Application
//
//  Created by Yogita on 30/01/18.
//  Copyright © 2018 WildnetTechnologies. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController {

    @IBOutlet weak var imageViewProfile: UIImageView!
    var model : UserViewModel?
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setData()
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadData(notification:)), name: Notification.Name("ReloadTable"), object: nil)

        Utility.roundView(view: self.imageViewProfile, radius: 40)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }

    @IBAction func logoutButtonTapped(_ sender: UIButton) {
       logOut()
    }
    
    
    @IBAction func userImageTapped(_ sender: UIButton) {
      self.swiftySideMenu.toggleSideMenu()
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "UserProfileViewController")
        if (!(((self.swiftySideMenu.centerViewController) as! UINavigationController).viewControllers.last is UserProfileViewController))
        {
          ((self.swiftySideMenu.centerViewController) as! UINavigationController).pushViewController(vc!, animated: true)
        }
       
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func reloadData(notification: Notification){
        setData()
    }
    
    func setData()
    {
        model = UserViewModel.init(user: UserDefaultManager())
        if let model = model
        {
            self.userNameLabel.text = model.name
            if let image = model.image
            {
                if let url = URL(string : image)
                {
               self.imageViewProfile.setImageWith(url, placeholderImage: #imageLiteral(resourceName: "avatar-placeholder"))
                }
            }
            
        }
        
        
    }
    
    func logOut()
    {
        let alertController : UIAlertController = UIAlertController(title: K.MESSAGE.k_app_name, message: K.MESSAGE.k_logout, preferredStyle: UIAlertControllerStyle.alert)
        let noAction = UIAlertAction(title: "No", style: UIAlertActionStyle.default) {
            UIAlertAction in
            
        }
        let yesAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.logoutUser()
        }
        alertController.addAction(noAction)
        alertController.addAction(yesAction)

        self.present(alertController, animated: true, completion: nil)
    }
    
    func logoutUser(){
        HomeAPIManager.logOutAPI(parameters: [:], vc: self) { (success) in
            if(success == true){
                UserDefaultManager.removedata()
                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let loginVC : LoginViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                appDelegate.navigationController.pushViewController(loginVC, animated: true)
                self.swiftySideMenu.centerViewController = appDelegate.navigationController
                self.swiftySideMenu.toggleSideMenu()
            }
        }
    }
    
}

extension SideMenuViewController:UITableViewDelegate,UITableViewDataSource
{
 
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            self.swiftySideMenu.toggleSideMenu()
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController")
            if (!(((self.swiftySideMenu.centerViewController) as! UINavigationController).viewControllers.last is HomeViewController))
            {
                ((self.swiftySideMenu.centerViewController) as! UINavigationController).pushViewController(vc!, animated: true)
            }
            break
        case 1:
            self.swiftySideMenu.toggleSideMenu()
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyTripsViewController")
            if (!(((self.swiftySideMenu.centerViewController) as! UINavigationController).viewControllers.last is MyTripsViewController))
            {
                ((self.swiftySideMenu.centerViewController) as! UINavigationController).pushViewController(vc!, animated: true)
            }
            break
        case 2:
            self.swiftySideMenu.toggleSideMenu()
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "LegalViewController")
            if (!(((self.swiftySideMenu.centerViewController) as! UINavigationController).viewControllers.last is LegalViewController))
            {
                ((self.swiftySideMenu.centerViewController) as! UINavigationController).pushViewController(vc!, animated: true)
            } 
            break
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTableViewCell")
            as! SideMenuTableViewCell
        cell.selectionStyle = .none
        cell.setData(index: indexPath.row)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}
