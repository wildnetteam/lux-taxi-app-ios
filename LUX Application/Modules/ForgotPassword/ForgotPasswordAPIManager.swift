//
//  ForgotPasswordAPIManager.swift
//  LUX Application
//
//  Created by Yogita on 05/02/18.
//  Copyright © 2018 WildnetTechnologies. All rights reserved.
//

import Foundation
class ForgotPasswordAPIManager{
    class func forgotPasswordAPI(parameters : [String:AnyObject],vc:UIViewController,completion:@escaping status){
        if(!(Reachability.isConnectedToNetwork())){
            Utility.showAlertWithMessage(vc: vc, message: K.MESSAGE.k_network_error)
            return
        }
        Utility.showIndicator()
        WebServiceHelper.sharedInstance.callGetApiWithDetail(input: parameters as AnyObject, completeUrl: K.URL.BaseUrl + K.URL.forgotPassword){(success, data) in
            Utility.hideIndicator()
            if success == true
            {
                guard let response = data else {
                    return
                }
                guard let dic = response as? NSDictionary else {
                    return
                }
                guard let status = dic["status"] else {
                    return
                }
                if ((status as! String) == "success")
                {
                    completion(true)
                }
                else
                {
                    guard let msg = dic["message"] else {
                        return
                    }
                    Utility.snackbar(message: msg as! String,vc:vc)
                    completion(false)
                }
                }
            else
            {
                completion(false)
            }
            
        }
    }
}

