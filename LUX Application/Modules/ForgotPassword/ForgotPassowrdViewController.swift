//
//  ForgotPassowrdViewController.swift
//  LUX Application
//
//  Created by Yogita on 30/01/18.
//  Copyright © 2018 WildnetTechnologies. All rights reserved.
//

import UIKit

class ForgotPassowrdViewController: UIViewController {

    @IBOutlet weak var textFieldEmail: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Utility.enableSwipe(enable: false,swiftySideMenu:swiftySideMenu)
    }
    
    //MARK:------Validation--------
    
    func validateForgotPasswordFields() ->Bool
    {
        let success = true
        if ( Utility.isEmpty(self.textFieldEmail.text!) )
        {
            Utility.snackbar(message:  K.MESSAGE.k_email_Empty,vc:self)
            return false;
        }
        else if (!Utility.validateEmail( self.textFieldEmail.text! ))
        {
            Utility.snackbar(message: K.MESSAGE.k_email_Invalid,vc:self)
            return false;
        }
        
        return success;
        
    }  
    
    
    //MARK: IBAction
    
    @IBAction func sendLinkButtonTapped(_ sender: UIButton) {
        if Reachability.isConnectedToNetwork(){
            if validateForgotPasswordFields(){
                let parameters : [String:String] = ["email":self.textFieldEmail.text!]
                ForgotPasswordAPIManager.forgotPasswordAPI(parameters: parameters as [String : AnyObject], vc: self, completion: { (success) in
                   if(success == true )
                   {
                    Utility.showAlert(vc: self, message: K.MESSAGE.k_forgotPassLink)
                    }
                })
            }
        }
        
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        Utility.popViewController(fromVC: self)
    }
    
    
}
