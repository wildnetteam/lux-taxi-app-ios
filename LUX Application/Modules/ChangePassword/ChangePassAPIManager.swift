//
//  ChangePassAPIManager.swift
//  LUX Application
//
//  Created by Yogita on 05/02/18.
//  Copyright © 2018 WildnetTechnologies. All rights reserved.
//

import Foundation
class ChangePassAPIManager{
    typealias changePassResponse = (_ isSuccess: Bool) -> Void
    class func changePasswordAPI(parameters : [String:AnyObject],vc:UIViewController,completion:@escaping changePassResponse){
        if(!(Reachability.isConnectedToNetwork())){
            Utility.showAlertWithMessage(vc: vc, message: K.MESSAGE.k_network_error)
            return
        }
        
        Utility.showIndicator()
        WebServiceHelper.sharedInstance.postFormDataWithHeader(input: parameters as AnyObject, image: nil, completeUrl: K.URL.BaseUrl + K.URL.ChangePassword) { (success, data) in
            Utility.hideIndicator()
            if success == true
            {
                guard let response = data else {
                    return
                }
                guard let dic = response as? NSDictionary else {
                    return
                }
                guard let status = dic["status"] else {
                    return
                }
                if ((status as! String) == "success")
                {
                        completion(true)
                }
                else
                {
                    
                    guard let msg = dic["message"] else {
                        return
                    }
                    
                    guard let code = dic["code"] else {
                        return
                    }
                    
                    if (code as? Int) == 420{
                        Utility.logOut(vc: vc, message: msg as! String)
                        return
                    }
                    Utility.snackbar(message: msg as! String,vc:vc)
                    completion(false)
                }
            }
            else
            {
                completion(false)
            }
        }
        }
    
    
    
    }



