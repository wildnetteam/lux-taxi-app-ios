//
//  ChangePasswordViewController.swift
//  LUX Application
//
//  Created by Yogita on 27/02/18.
//  Copyright © 2018 WildnetTechnologies. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController {

    @IBOutlet weak var oldPasswordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:------Validation--------
    
    func validateChangePassFields() ->Bool
    {
        let success = true
        
        if ( Utility.isEmpty(self.oldPasswordTextField.text!) )
        {
            Utility.snackbar(message: K.MESSAGE.k_old_pass_Empty,vc:self)
            return false;
        }
        if ( !Utility.validateName(self.newPasswordTextField.text!))
        {
            Utility.snackbar(message: K.MESSAGE.k_new_Pass_Empty,vc:self)
            return false;
        }
        else if ( Utility.isEmpty(self.confirmPasswordTextField.text!) )
        {
            Utility.snackbar(message:  K.MESSAGE.k_confirm_pass_Empty,vc:self)
            return false;
        }
        else if (!Utility.validatePassword(self.newPasswordTextField.text!) )
        {
            Utility.snackbar(message: K.MESSAGE.k_pass_length,vc:self)
            return false;
        }
        else if (self.newPasswordTextField.text != self.confirmPasswordTextField.text){
            Utility.snackbar(message: K.MESSAGE.k_pass_Match,vc:self)
            return false;
        }
        return success;
        
    }
    
    
    @IBAction func saveButtonTapped(_ sender: UIButton) {
        if(validateChangePassFields()){
           let params : [String:String] = [
            "oldpassword": self.oldPasswordTextField.text!,
                "newpassword": self.newPasswordTextField.text!,
                "confirmpassword": self.confirmPasswordTextField.text!,
            ]
            ChangePassAPIManager.changePasswordAPI(parameters: params as [String : AnyObject], vc: self, completion: { (success) in
                if success == true{
                    let alertController : UIAlertController = UIAlertController(title: K.MESSAGE.k_app_name, message: K.MESSAGE.k_password_updated, preferredStyle: UIAlertControllerStyle.alert)
                    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                        UIAlertAction in
                        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let loginVC : LoginViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                        appDelegate.navigationController.pushViewController(loginVC, animated: true)
                        self.swiftySideMenu.centerViewController = appDelegate.navigationController
                    }
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                   
                }
            })
            
        }
        
    }
    
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        Utility.popViewController(fromVC: self)
    }
    
}
