//
//  SignUpViewController.swift
//  LUX Application
//
//  Created by Yogita on 30/01/18.
//  Copyright © 2018 WildnetTechnologies. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController,UIGestureRecognizerDelegate {
    
    @IBOutlet weak var TextFieldName: UITextField!
    @IBOutlet weak var TextFieldEmail: UITextField!
    @IBOutlet weak var TextFieldPassword: UITextField!
    @IBOutlet weak var TextFieldConfirmPassword: UITextField!
    @IBOutlet weak var TextFieldMobile: UITextField!
    @IBOutlet weak var TextFieldDOB: UITextField!
    @IBOutlet weak var imageViewProfile: UIImageView!
    @IBOutlet weak var textViewAddress: UITextView!
    @IBOutlet weak var textFieldCountry: UITextField!
    @IBOutlet weak var textFieldCity: UITextField!
    @IBOutlet weak var DobBg: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var datePickerView: UIView!
    @IBOutlet weak var textFieldZipCode: UITextField!
    @IBOutlet weak var textFieldStates: UITextField!
    @IBOutlet weak var datePickerBottomLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var checkBoxImage: UIImageView!
    @IBOutlet weak var statePicker: UIPickerView!
    @IBOutlet weak var pickerLabel: UILabel!
    
    var selectedState = 0
    var selectedCountry = 0
    
    var imagePicker = UIImagePickerController()
    var model : SignUpViewModel?
    var date = ""
    var isImage = false
    var isDatePickerOpen = false
    var isStatePickerOpen = false
    var isCountryPickerOpen = false
    var isTermsAccepted = false
    
    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Utility.hideIndicator()
        self.statePicker.delegate = self
        self.statePicker.dataSource = self
        self.textViewAddress.tintColor = UIColor.black
        self.setMinimumDate()
        setData()
        self.textViewAddress.text = "Enter your address.."
        self.textViewAddress.textColor = UIColor.lightGray
        self.imagePicker.delegate = self
        self.datePickerBottomLayoutConstraint.constant = -self.datePickerView.frame.size.height
        self.view.layoutIfNeeded()
        Utility.roundView(view: self.imageViewProfile, radius: 50)
       
        // Do any additional setup after loading the view.
    }
    
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        UIApplication.shared.statusBarStyle = .default
        Utility.enableSwipe(enable: false,swiftySideMenu:swiftySideMenu)
    }
    
    
    @IBAction func countryButtonTapped(_ sender: UIButton) {
    self.pickerLabel.text = "Select Country"
        self.isDatePickerOpen = false
        self.isCountryPickerOpen = true
        self.isStatePickerOpen = false
        self.datePicker.isHidden = true
        self.statePicker.isHidden = false
        self.view.endEditing(true)
        self.DobBg.isHidden = false
        self.statePicker.reloadAllComponents()
        self.datePickerBottomLayoutConstraint.constant = -self.datePickerView.frame.size.height
        self.view.layoutIfNeeded()
        
        UIView.animate(withDuration: Double(0.5), animations: {
            self.datePickerBottomLayoutConstraint.constant = 0
            self.view.layoutIfNeeded()
        })
    }
    
    
    @IBAction func stateButtonTapped(_ sender: UIButton) {
        self.pickerLabel.text = "Select State"
        self.isDatePickerOpen = false
        self.isCountryPickerOpen = false
        self.isStatePickerOpen = true
        self.datePicker.isHidden = true
        self.statePicker.isHidden = false
        self.view.endEditing(true)
        self.DobBg.isHidden = false
        self.statePicker.reloadAllComponents()
        self.datePickerBottomLayoutConstraint.constant = -self.datePickerView.frame.size.height
        self.view.layoutIfNeeded()
        
        UIView.animate(withDuration: Double(0.5), animations: {
            self.datePickerBottomLayoutConstraint.constant = 0
            self.view.layoutIfNeeded()
        })
    }
    
    
    //MARK:------Validation--------
    
    func validateSignUpFields() ->Bool
    {
        let success = true
        
        if ( Utility.isEmpty(self.TextFieldName.text!) )
        {
            Utility.snackbar(message: K.MESSAGE.k_name_Empty,vc:self)
            return false;
        }
        if ( !Utility.validateName(self.TextFieldName.text!))
        {
            Utility.snackbar(message: K.MESSAGE.k_name_Invalid,vc:self)
            return false;
        }
        if (Utility.checkSpecialCharaters(string: self.TextFieldName.text!))
        {
            Utility.snackbar(message: K.MESSAGE.k_name_Invalid,vc:self)
            return false;
        }
        else if ( Utility.isEmpty(self.TextFieldEmail.text!) )
        {
            Utility.snackbar(message:  K.MESSAGE.k_email_Empty,vc:self)
            return false;
        }
        else if (!Utility.validateEmail( self.TextFieldEmail.text! ))
        {
            Utility.snackbar(message: K.MESSAGE.k_email_Invalid,vc:self)
            return false;
        }
        else if ( Utility.isEmpty(self.TextFieldPassword.text!) )
        {
            Utility.snackbar(message: K.MESSAGE.k_pass_Empty,vc:self)
            return false;
        }
        else if (!Utility.validatePassword(self.TextFieldPassword.text!) )
        {
            Utility.snackbar(message: K.MESSAGE.k_pass_length,vc:self)
            return false;
        }
        else if (Utility.isEmpty(self.TextFieldConfirmPassword.text!) )
        {
            Utility.snackbar(message: K.MESSAGE.k_pass_2_Empty,vc:self)
            return false;
        }
        else if ((self.TextFieldConfirmPassword.text! != self.TextFieldPassword.text!) )
        {
            Utility.snackbar(message: K.MESSAGE.k_pass_Match,vc:self)
            return false;
        }
        else if (Utility.isEmpty(self.TextFieldMobile.text!) )
        {
            Utility.snackbar(message: K.MESSAGE.k_mob_Empty,vc:self)
            return false;
        }
        else if (!Utility.validateMob( self.TextFieldMobile.text! ))
        {
            Utility.snackbar(message: K.MESSAGE.k_mob_Invalid,vc:self)
            return false;
        }
        else if (Utility.isEmpty(self.TextFieldDOB.text!) )
        {
            Utility.snackbar(message: K.MESSAGE.k_dob_Empty,vc:self)
            return false;
        }
        else if (self.textViewAddress.text.characters.count == 0 || self.textViewAddress.text == "Enter your address.." )
        {
            Utility.snackbar(message: K.MESSAGE.k_address_Empty,vc:self)
            return false;
        }
        else if (self.textViewAddress.text.characters.count > 50 )
        {
            Utility.snackbar(message: K.MESSAGE.k_address_Valid,vc:self)
            return false;
        }
        else if (Utility.isEmpty(self.textFieldStates.text!) )
        {
            Utility.snackbar(message: K.MESSAGE.k_state_Empty,vc:self)
            return false;
        }
        else if (Utility.isEmpty(self.textFieldCity.text!) )
        {
            Utility.snackbar(message: K.MESSAGE.k_city_Empty,vc:self)
            return false;
        }
        else if (!(((self.textFieldCity.text?.characters.count)! > 2) && ((self.textFieldCity.text?.characters.count)! <= 50)))
        {
            Utility.snackbar(message: K.MESSAGE.k_city_Invalid,vc:self)
            return false;
        }
        else if (Utility.isEmpty(self.textFieldZipCode.text!) )
        {
            Utility.snackbar(message: K.MESSAGE.k_zipcode_Empty,vc:self)
            return false;
        }
        else if (self.textFieldZipCode.text?.characters.count != 5)
        {
            Utility.snackbar(message: K.MESSAGE.k_zipcode_Invalid,vc:self)
            return false;
        }
        else if(!isTermsAccepted){
            Utility.snackbar(message: K.MESSAGE.k_terms_accept,vc:self)
            return false;
        }
        
        return success;
        
    }
    
    //MARK: IBAction

    
    @IBAction func checkBoxButtonTapped(_ sender: UIButton) {
        if(isTermsAccepted){
            self.checkBoxImage.image = #imageLiteral(resourceName: "checkbox")
            isTermsAccepted = false
        }
        else
        {
           self.checkBoxImage.image = #imageLiteral(resourceName: "checked")
           isTermsAccepted = true
        }
        
    }
    
    @IBAction func termsAndConditionsTapped(_ sender: UIButton) {
        Utility.pushViewController(fromVC: self, toVC: LegalDetailViewController(), identifier: "LegalDetailViewController")
    }
    
    @IBAction func doneButtonTapped(_ sender: UIButton) {
        if(isDatePickerOpen){
            self.datePickerBottomLayoutConstraint.constant = 0
            self.view.layoutIfNeeded()
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd yyyy"
            let convertedDate = dateFormatter.string(from: self.datePicker.date as Date)
            self.TextFieldDOB.text = convertedDate
            dateFormatter.dateFormat = "yyyy-MM-dd"
            self.date = dateFormatter.string(from: self.datePicker.date as Date)
            UIView.animate(withDuration: Double(0.5), animations: {
                self.datePickerBottomLayoutConstraint.constant = -self.datePickerView.frame.size.height
                self.view.layoutIfNeeded()
            })
            { (true) in
                self.DobBg.isHidden = true
            }
        }
        if(isStatePickerOpen){
            self.textFieldStates.text = self.localStates()[selectedState]
            self.datePickerBottomLayoutConstraint.constant = 0
            self.view.layoutIfNeeded()
            UIView.animate(withDuration: Double(0.5), animations: {
                self.datePickerBottomLayoutConstraint.constant = -self.datePickerView.frame.size.height
                self.view.layoutIfNeeded()
            })
            { (true) in
                self.DobBg.isHidden = true
            }
        }
        if(isCountryPickerOpen){
            self.textFieldCountry.text = "United States"
            self.datePickerBottomLayoutConstraint.constant = 0
            self.view.layoutIfNeeded()
            UIView.animate(withDuration: Double(0.5), animations: {
                self.datePickerBottomLayoutConstraint.constant = -self.datePickerView.frame.size.height
                self.view.layoutIfNeeded()
            })
            { (true) in
                self.DobBg.isHidden = true
            }
        }
        
    }

    
    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        self.datePickerBottomLayoutConstraint.constant = 0
        self.view.layoutIfNeeded()
        
        UIView.animate(withDuration: Double(0.5), animations: {
            self.datePickerBottomLayoutConstraint.constant = -self.datePickerView.frame.size.height
            self.view.layoutIfNeeded()
        })
        { (true) in
            self.DobBg.isHidden = true
        }
    }
    
    @IBAction func signUpButtonTapped(_ sender: UIButton) {
        if validateSignUpFields()
        {
            let alertController : UIAlertController = UIAlertController(title: K.MESSAGE.k_confirm_title, message: "Please confirm your email address and submit the details.\n\n" + self.TextFieldEmail.text!, preferredStyle: UIAlertControllerStyle.alert)
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default) {
                UIAlertAction in
            }
            let submitAction = UIAlertAction(title: "Submit", style: UIAlertActionStyle.default) {
                UIAlertAction in
                self.sendDataForSignUp()
            }
            alertController.addAction(cancelAction)
            alertController.addAction(submitAction)
            self.present(alertController, animated: true, completion: nil)
           
        }
    }
    
    @IBAction func uploadImageButtonTapped(_ sender: UIButton) {
        let actionSheetController: UIAlertController = UIAlertController(title: "Choose an option", message: "", preferredStyle: .alert)
        
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            //Just dismiss the action sheet
        }
        actionSheetController.addAction(cancelAction)
        //Create and add first option action
        let takePictureAction: UIAlertAction = UIAlertAction(title: "Take Picture", style: .default) { action -> Void in
            self.openCamera()
        }
        actionSheetController.addAction(takePictureAction)
        //Create and add a second option action
        let choosePictureAction: UIAlertAction = UIAlertAction(title: "Choose Existing", style: .default) { action -> Void in
            self.openGallery()
        }
        actionSheetController.addAction(choosePictureAction)
        
        //Present the AlertController
        self.present(actionSheetController, animated: true, completion: nil)
        
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        Utility.popViewController(fromVC: self)
    }
    
    
    @IBAction func dateOfBirthButtonTapped(_ sender: UIButton) {
        self.pickerLabel.text = "Date of Birth"
        self.isDatePickerOpen = true
        self.isCountryPickerOpen = false
        self.isStatePickerOpen = false
        self.datePicker.isHidden = false
        self.statePicker.isHidden = true
        self.view.endEditing(true)
        self.DobBg.isHidden = false
        self.datePickerBottomLayoutConstraint.constant = -self.datePickerView.frame.size.height
        self.view.layoutIfNeeded()
        
        UIView.animate(withDuration: Double(0.5), animations: {
            self.datePickerBottomLayoutConstraint.constant = 0
            self.view.layoutIfNeeded()
        })
    }
   
    //MARK: sendDataForSignUp
    
    func sendDataForSignUp()
    {
        var fbId = ""
        var googleID = ""
        var userimage  = ""
        var user_image : UIImage?
        user_image = self.imageViewProfile.image!
        
        if let facebook = model?.facebookId
        {
            fbId = facebook
        }
        if let google = model?.googleId
        {
           googleID = google
        }
        if isImage{
          userimage = ""
          user_image = self.imageViewProfile.image!
        }
        else
        {
            if let image  = model?.image{
                userimage = image
                user_image = nil
            }
        }
        
        let parameters: [String:String] = ["name":self.TextFieldName.text!,"username":self.TextFieldEmail.text!,"email":self.TextFieldEmail.text!,"mobile":self.TextFieldMobile.text!,"password":self.TextFieldPassword.text!,"isdevice":"1","device_token":"","gender":"","dob":self.date,"facebook_id": fbId,"gmail_id":googleID,"social_image" : userimage,"address":self.textViewAddress.text,"state":self.textFieldStates.text!,"country":self.textFieldCountry.text!,"zipcode":self.textFieldZipCode.text!,"city":self.textFieldCity.text!]
        SignUpAPIManager.getSignUpData(parameters: parameters as [String : AnyObject],image: user_image, vc:self, completion: { (success) in
            if(success == true)
            {
                Utility.pushViewController(fromVC: self, toVC: HomeViewController(), identifier: "HomeViewController")
            }
            
            
        })
    }
    
}

extension SignUpViewController:UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate{
    
    func openGallery()
    {
        self.imagePicker.allowsEditing = true
        self.imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        present(self.imagePicker, animated: true, completion: nil)
    }
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            self.imagePicker.allowsEditing = true
            self.imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            self.imagePicker.cameraCaptureMode = .photo
            present(self.imagePicker, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerEditedImage] as! UIImage
         isImage = true
         self.imageViewProfile.image = chosenImage
        dismiss(animated: true, completion: nil)
        
    }
    
    func setData()
    {
        if let name =  model?.name
        {
            self.TextFieldName.text = name
        }
        if let email = model?.email
        {
            self.TextFieldEmail.text = email
        }
        if let image  = model?.image
        {
            self.imageViewProfile.setImageWith(URL(string: image)!, placeholderImage: #imageLiteral(resourceName: "Profile Pic"))
        }
    }
    func setMinimumDate()
    {
        var components = DateComponents()
        components.year = -100
        let minDate = Calendar.current.date(byAdding: components, to: Date())
        
        components.year = -13
        let maxDate = Calendar.current.date(byAdding: components, to: Date())
        
        datePicker.minimumDate = minDate
        datePicker.maximumDate = maxDate
    }
    
   
    fileprivate func localStates() -> [String] {
        if let path = Bundle.main.path(forResource: "State", ofType: "json") {
            do {
                let jsonData = try Data(contentsOf: URL(fileURLWithPath: path), options: .dataReadingMapped)
                let jsonResult = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as! [[String:String]]
                
                var countryNames = [String]()
                for country in jsonResult {
                    countryNames.append(country["name"]!)
                }
                
                return countryNames
            } catch {
                print("Error parsing jSON: \(error)")
                return []
            }
        }
        return []
    }
    
}

extension SignUpViewController:UITextFieldDelegate,UITextViewDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag  = textField.tag + 1
        // Try to find next responder
        var nextResponder : UIResponder?
        nextResponder = ((textField.superview?.superview?.viewWithTag(nextTag)))
        if((nextResponder) != nil){
            // Found next responder, so set it.
            nextResponder?.becomeFirstResponder()
        }
        else{
            textField.resignFirstResponder()
        }
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        if(textField == TextFieldMobile){
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= 16
        }
        return true
        
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Enter your address.."
            textView.textColor = UIColor.lightGray
        }
    }
}
extension SignUpViewController : UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(self.isStatePickerOpen){
            return self.localStates().count
        }
        if(self.isCountryPickerOpen){
            return 1
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(self.isStatePickerOpen){
            return self.localStates()[row]
        }
        if(self.isCountryPickerOpen){
            return "United States"
        }
        return  ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(self.isStatePickerOpen){
            self.selectedState = row
        }
        if(self.isCountryPickerOpen){
           self.selectedCountry = row
        }
    }
    
    
    
}
