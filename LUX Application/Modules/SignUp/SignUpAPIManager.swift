//
//  SignUpAPIManager.swift
//  LUX Application
//
//  Created by Yogita on 05/02/18.
//  Copyright © 2018 WildnetTechnologies. All rights reserved.
//

import Foundation

class SignUpAPIManager{
    class func getSignUpData(parameters : [String:AnyObject], image:UIImage?,vc:UIViewController,completion:@escaping status){
        if(!(Reachability.isConnectedToNetwork())){
            Utility.showAlertWithMessage(vc: vc, message: K.MESSAGE.k_network_error)
            return
        }
        Utility.showIndicator()
        WebServiceHelper.sharedInstance.postFormData(input: parameters as AnyObject, image: image, completeUrl: K.URL.BaseUrl + K.URL.SignUp) { (success, data) in
            Utility.hideIndicator()
            if success == true
            {
                guard let response = data else {
                    return
                }
                guard let dic = response as? NSDictionary else {
                    return
                }
                
                guard let status = dic["status"] else {
                    return
                }
                
                if ((status as! String) == "success")
                {
                    guard let userDetail = dic["userdetail"] else {
                        return
                    }
                    guard let userDetailArr = userDetail as? NSArray else {
                        return
                    }
                   _ =  UserModel.init(result: userDetailArr[0] as! Dictionary<String, Any>)
                   completion(true)
                }
                else
                {
                    guard let msg = dic["message"] else {
                        return
                    }
                    guard let code = dic["code"] else {
                        return
                    }
                    
                    if (code as? Int) == 420{
                        Utility.snackbar(message: msg as! String,vc: vc)
                        return
                    }
                    Utility.snackbar(message: msg as! String,vc:vc)
                    completion(false)
                }
                 UserDefaultManager.setToken(token: dic["token"] as? String)
                guard let userImage = dic["userimagepath"] else {
                    return
                }
                
                UserDefaultManager.setImagePath(imagePath: userImage as? String)
                }
            
           else
            {
                completion(false)
            }
            
        }
    }
}

