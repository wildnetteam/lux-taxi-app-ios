//
//  UserViewModel.swift
//  LUX Application
//

import UIKit

class SignUpViewModel: NSObject {
    private var signUpModel: SignUpModel
    
    var name: String? {
        return signUpModel.name
    }
    var email: String? {
        return signUpModel.email
    }
    var image: String? {
        return signUpModel.image
    }
    var facebookId: String? {
        return signUpModel.facebookId
    }
    var signUpType: String? {
        return signUpModel.signUpType
    }
    var googleId: String? {
        return signUpModel.googleId
    }
    
    init(user: SignUpModel) {
        self.signUpModel = user
    }
    
}
