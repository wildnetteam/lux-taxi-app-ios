//
// SignUpModel.swift
//  LUX Application
//

import UIKit

class SignUpModel: NSObject {
    var id : String?
    var email : String?
    var name : String?
    var image : String?
    var signUpType : String?
    var googleId : String?
    var facebookId : String?
    
init(id:String,email:String,name:String,image:String,signUpType:String,googleId:String,facebookId:String) {
        self.id = id
        self.email = email
        self.name = name
        self.image = image
        self.signUpType = signUpType
        self.googleId = googleId
        self.facebookId = facebookId    
    }
    
    
}

