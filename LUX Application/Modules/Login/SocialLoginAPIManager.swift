//
//  SocialLoginAPIManager.swift
//  LUX Application
//
//  Created by Yogita on 07/02/18.
//  Copyright © 2018 WildnetTechnologies. All rights reserved.
//

import Foundation

class SocialLoginAPIManager{
    class func verifySocialLoginData(parameters : [String:AnyObject],vc:UIViewController,completion:@escaping status){
        Utility.showIndicator()
        WebServiceHelper.sharedInstance.postData(input: parameters as AnyObject, completeUrl: K.URL.BaseUrl + K.URL.SocialLogin) { (success, data) in

            Utility.hideIndicator()
            if success == true
            {
                guard let response = data else {
                    return
                }
                guard let dic = response as? NSDictionary else {
                    return
                }
                guard let status = dic["status"] else {
                    return
                }
               
                if ((status as! String) == "success")
                {
                    guard let userDetail = dic["userdetail"] else {
                        return
                    }
                    guard let userDetaildic = userDetail as? Dictionary<String, Any> else {
                        return
                    }
                     UserDefaultManager.setImagePath(imagePath: dic["userimagepath"] as? String)
                    UserDefaultManager.setToken(token: dic["token"] as? String)
                    _ =  UserModel.init(result: userDetaildic)
                    completion(true)
                   
                }
                else
                {
                    guard let msg = dic["message"] else {
                        return
                    }
                    guard let code = dic["code"] else {
                        return
                    }
                    
                    if (code as? Int) == 420{
                       Utility.snackbar(message: msg as! String,vc: vc)
                        return
                    }
                    if (code as? Int) == 406{
                        Utility.snackbar(message: msg as! String,vc: vc)
                        return
                    }
                  //  Utility.snackbar(message: msg as! String, vc: vc)
                    completion(false)
                }
            }
            else
            {
                completion(false)
            }
            
        }
    }
}
