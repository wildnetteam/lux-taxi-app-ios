//
//  LoginViewController.swift
//  LUX Application
//
//  Created by Yogita on 30/01/18.
//  Copyright © 2018 WildnetTechnologies. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
import CoreLocation


class LoginViewController: UIViewController {
    
    @IBOutlet var bottomConstraint: NSLayoutConstraint!
    @IBOutlet var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var textFieldName: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var signUpButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(Utility.isIPhoneX()){
          self.topConstraint.constant = 50
            self.bottomConstraint.constant = -40
            self.view.layoutIfNeeded()
        }
        
        self.navigationController?.navigationBar.isHidden = true
    self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false

        Utility.textFieldBorderColor(viewEmail, color: UIColor.clear)
        Utility.textFieldBorderColor(viewPassword, color: UIColor.clear)
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        UIApplication.shared.statusBarStyle = .lightContent
       // setupLocationManager()
        fetchUserLocation()
        Utility.enableSwipe(enable: false,swiftySideMenu:swiftySideMenu)
    }
    
    //MARK:------Validation--------
    
    func validateLoginFields() ->Bool
    {
        let success = true
        if ( Utility.isEmpty(self.textFieldName.text!) )
        {
            Utility.snackbar(message:  K.MESSAGE.k_email_Empty,vc:self)
            return false;
        }
        else if (!Utility.validateEmail( self.textFieldName.text! ))
        {
            Utility.snackbar(message: K.MESSAGE.k_email_Invalid,vc:self)
            return false;
        }
        else if (Utility.isEmpty(self.textFieldPassword.text!) )
        {
            Utility.snackbar(message: K.MESSAGE.k_pass_Empty,vc:self)
            return false;
        }
        
        return success;
        
    }
    
    
    //MARK: IBAction
    
    @IBAction func forgotPassButtonTapped(_ sender: UIButton) {
        Utility.pushViewController(fromVC: self, toVC: ForgotPassowrdViewController(), identifier: "ForgotPassowrdViewController")
    }
    
    @IBAction func facebookButtonTapped(_ sender: UIButton) {
        if Reachability.isConnectedToNetwork()
        {
            let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
            fbLoginManager.logOut()
            fbLoginManager.loginBehavior = .web
            fbLoginManager.logIn(withReadPermissions: ["email"], from: self,handler: { (result, error) -> Void in
                if (error == nil){
                    let fbloginresult : FBSDKLoginManagerLoginResult = result!
                    if(FBSDKAccessToken.current() != nil) {
                        if(fbloginresult.grantedPermissions.contains("email"))
                        {
                            self.getFBUserData()
                        }
                    }
                }
            })
        }
        else
        {
            Utility.showAlertWithMessage(vc: self, message: K.MESSAGE.k_network_error)
        }
    }
    
    
    @IBAction func googleButtonTapped(_ sender: UIButton) {
        GIDSignIn.sharedInstance().signOut()
        GIDSignIn.sharedInstance().delegate=self
        GIDSignIn.sharedInstance().uiDelegate=self
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func signUpButtonTapped(_ sender: UIButton) {
        Utility.pushViewController(fromVC: self, toVC: SignUpViewController(), identifier: "SignUpViewController")
    }
    
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        if Reachability.isConnectedToNetwork()
        {
            if validateLoginFields()
            {
                let parameters : [String:String] =  ["email":self.textFieldName.text!, "password":self.textFieldPassword.text!,"device_token":""]
                LoginAPIManager.getLoginData(parameters: parameters as [String : AnyObject],vc: self, completion: { (success) in
                    if(success == true)
                    {
                        Utility.pushViewController(fromVC: self, toVC: HomeViewController(), identifier: "HomeViewController")
                    }
                })
            }
        }
        else
        {
            Utility.showAlertWithMessage(vc: self, message: K.MESSAGE.k_network_error)
        }
    }
    
    
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    print(result!)
                    let response : NSDictionary!
                    response = result as! NSDictionary
                    let dic = response["picture"] as! NSDictionary
                    let email = response["email"]
                    _=dic["data"] as! NSDictionary
                    let id : String = response!["id"] as! String
                    let model =  SignUpViewModel.init(user: SignUpModel.init(id: id, email: email as! String, name: response["name"] as! String, image: "https://graph.facebook.com/" + id + "/picture?type=large" , signUpType: "facebook_id", googleId: "", facebookId: id ))
                    
                    self.validateSocialLogin(facebookId: id, loginType:"facebook_id", googleId:"",model:model)
                    
                }
            })
        }
    }

    func validateSocialLogin(facebookId : String, loginType : String, googleId:String,model:SignUpViewModel)
    {
        if Reachability.isConnectedToNetwork()
        {
            let parameters : [String:String] =  ["facebook_id":facebookId, "gmail_id":googleId,"social_type":loginType,"device_token":""]
            SocialLoginAPIManager.verifySocialLoginData(parameters: parameters as [String : AnyObject],vc:self, completion: { (success) in
                if(success == true)
                {
                   Utility.pushViewController(fromVC: self, toVC: HomeViewController(), identifier: "HomeViewController")
                }
                else
                {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
                    vc.model = model
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }
            })
        }
        else
        {
            Utility.showAlertWithMessage(vc: self, message: K.MESSAGE.k_network_error)
        }
    }
    
    
    
}

extension LoginViewController:GIDSignInUIDelegate,GIDSignInDelegate
{
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            //let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let email = user.profile.email
            var pic : URL?
            if(user.profile.hasImage)
            {
                let dimension = round(200)
                pic = user.profile.imageURL(withDimension: UInt(dimension))
            }
            let picUrl = pic?.absoluteString
            
            let model =  SignUpViewModel.init(user: SignUpModel.init(id: userId!, email: email! , name: fullName as! String, image: picUrl!, signUpType: "facebook_id", googleId: userId!, facebookId: "" ))
             self.validateSocialLogin(facebookId: "", loginType:"gmail_id", googleId:userId!,model:model )
        } else {
            print("\(error.localizedDescription)")
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        
    }
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
    }
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    
}

extension LoginViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag  = textField.tag + 1
        // Try to find next responder
        var nextResponder : UIResponder?
        nextResponder = ((textField.superview?.superview?.viewWithTag(nextTag)))
        if((nextResponder) != nil){
            // Found next responder, so set it.
            nextResponder?.becomeFirstResponder()
        }
        else{
            textField.resignFirstResponder()
        }
        return false
    }
}

extension LoginViewController : CLLocationManagerDelegate{
    
    func fetchUserLocation()
    {
        if CLLocationManager.locationServicesEnabled() && CLLocationManager.authorizationStatus() != CLAuthorizationStatus.denied  && CLLocationManager.authorizationStatus() != CLAuthorizationStatus.restricted{
            LocationHandler.sharedInstance.updateLocation { (location, error, true) in
                
                if error != nil{
                    if error?.domain == "Restricted"
                    {
                        Utility.showAlertWithMessage(vc: self, message:K.MESSAGE.locationError )
                    }
                }
                else
                {
                    appDelegate.userLocation = location
                   // appDelegate.fromLocation = location
                    if (location?.coordinate) != nil
                    {
                        LocationHandler.sharedInstance.reverseGeoCoding(location: location!, completion: { (address) in
                            print(address)
                            appDelegate.userAddress = address
                        })
                    }
                }
            }
        }
        else{
            
            Utility.showAlertWithMessage(vc: self, message:K.MESSAGE.locationError )
            
        }
        
    }
    
    
}



