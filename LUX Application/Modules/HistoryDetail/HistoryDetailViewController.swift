//
//  HistoryDetailViewController.swift
//  LUX Application
//
//  Created by Yogita on 26/02/18.
//  Copyright © 2018 WildnetTechnologies. All rights reserved.
//

import UIKit
import HCSStarRatingView

class HistoryDetailViewController: UIViewController {

    @IBOutlet weak var toLocationLabel: UILabel!
    @IBOutlet weak var fromLocationLabel: UILabel!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var tripdate: UILabel!
    @IBOutlet weak var userimageView: UIImageView!
    @IBOutlet weak var toLocation: UITextField!
    @IBOutlet weak var fromLocation: UITextField!
    @IBOutlet weak var services: UILabel!
    @IBOutlet weak var numberOfPassengers: UITextField!
    @IBOutlet weak var totalDistance: UITextField!
    @IBOutlet weak var tripPrice: UILabel!
    @IBOutlet weak var carDescription: UILabel!
    @IBOutlet weak var driverName: UILabel!
    @IBOutlet weak var rating: HCSStarRatingView!
    @IBOutlet weak var mailInvoiceButton: UIButton!
    
    var model : MyTripsViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Utility.roundView(view: userimageView, radius: userimageView.frame.size.width/2)
        setData()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
    }
    
    func setData(){
        if(model?.status == K.STATUS.EXPIRED){
            totalPriceLabel.isHidden = true
            self.tripPrice.isHidden = true
            self.mailInvoiceButton.isHidden = true
        }
        else if(model?.status == K.STATUS.CANCELED){
            self.mailInvoiceButton.isHidden = true
            if let cancelPaymentStatus = model?.cancelledStatus{
                if let cancellationFees = model?.cancelFee{
                    if(cancelPaymentStatus == "1" && cancellationFees > "0"){
                         totalPriceLabel.isHidden = false
                        self.tripPrice.text = model?.cancelledFees
                        self.tripPrice.isHidden  = false
                    }
                    else
                    {
                         self.totalPriceLabel.isHidden = true
                        self.tripPrice.isHidden  = true
                    }
                }
            }
        }
        else
        {
           self.tripPrice.isHidden = false
           tripPrice.text = model?.amount
        }
       tripdate.text = model?.book_create_date_time
        if let image = model?.image
        {
            if let url = URL(string : image)
            {
                self.userimageView.setImageWith(url, placeholderImage: #imageLiteral(resourceName: "avatar-placeholder"))
            }
        }
        if let rate = model?.rating{
            if let n = NumberFormatter().number(from: rate) {
                rating.value = CGFloat(truncating: n)
            }
        }
        driverName.text = model?.name
       fromLocationLabel.text = model?.pickup_area //fromLocation.text = model?.pickup_area
        toLocationLabel.text = model?.drop_area
      //  toLocation.text = model?.drop_area
        services.text = model?.amenities
        numberOfPassengers.text = model?.person
        carDescription.text = model?.carDetails
        totalDistance.text = model?.distance
        
    }
    
    func mailInvoiceAPI(){
        guard let bookingId = model?.booking_id else {
            return
        }
        let params = ["booking_id" : bookingId]
        HomeAPIManager.mailInvoiceAPI(parameters: params as [String : AnyObject], vc: self) { (success) in
            if(success){
                Utility.showAlertWithMessage(vc: self, message: K.MESSAGE.k_mailInvoice)
            }
        }
        
    }
    
    

    @IBAction func mailInvoiceButtonTapped(_ sender: UIButton) {
        mailInvoiceAPI()
    }
    
    @IBAction func backButtontapped(_ sender: UIButton) {
        Utility.popViewController(fromVC: self)
    }
}
