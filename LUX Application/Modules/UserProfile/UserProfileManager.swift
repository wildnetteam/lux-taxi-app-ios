//
//  SignUpAPIManager.swift
//  LUX Application
//
//  Created by Yogita on 05/02/18.
//  Copyright © 2018 WildnetTechnologies. All rights reserved.
//

import Foundation

class UserProfileManager{
    class func getUserProfileData(parameters : [String:AnyObject],vc:UIViewController,completion:@escaping status){
        Utility.showIndicator()
        WebServiceHelper.sharedInstance.callGetApiWithHeader(input: nil, completeUrl: K.URL.BaseUrl + K.URL.GetUserDetails) { (success, data) in
           Utility.hideIndicator()
            if success == true
            {
                guard let response = data else {
                    return
                }
                guard let dic = response as? NSDictionary else {
                    return
                }
                
                guard let status = dic["status"] else {
                    return
                }
                
                if ((status as! String) == "success")
                {
                    guard let userDetail = dic["data"] else {
                        return
                    }
                    guard let userDetailArr = userDetail as? NSDictionary else {
                        return
                    }
                   _ =  UserModel.init(result: userDetailArr as! Dictionary<String, Any>)
                   completion(true)
                }
                else
                {
                    guard let msg = dic["message"] else {
                        return
                    }
                    Utility.snackbar(message: msg as! String,vc:vc)
                    completion(false)
                }
                guard let userImage = dic["userimagepath"] else {
                    return
                }
                UserDefaultManager.setImagePath(imagePath: userImage as? String)
                }
            
           else
            {
                completion(false)
            }
            
        }
    }
    
    class func postUserProfileData(parameters : [String:AnyObject],image:UIImage?,vc:UIViewController,completion:@escaping status){
        Utility.showIndicator()
        WebServiceHelper.sharedInstance.postFormDataWithHeader(input: parameters as AnyObject, image: image, completeUrl: K.URL.BaseUrl + K.URL.EditUserDetails) { (success, data) in
               Utility.hideIndicator()
            if success == true
            {
                guard let response = data else {
                    return
                }
                guard let dic = response as? NSDictionary else {
                    return
                }
                
                guard let status = dic["status"] else {
                    return
                }
                
                if ((status as! String) == "success")
                {
                    guard let userDetail = dic["data"] else {
                        return
                    }
                    guard let userDetailArr = userDetail as? NSDictionary else {
                        return
                    }
                    _ =  UserModel.init(result: userDetailArr as! Dictionary<String, Any>)
                    completion(true)
                }
                else
                {
                    guard let msg = dic["message"] else {
                        return
                    }
                    Utility.snackbar(message: msg as! String,vc:vc)
                    completion(false)
                }
                guard let userImage = dic["userimagepath"] else {
                    return
                }
                UserDefaultManager.setImagePath(imagePath: userImage as? String)
            }
                
            else
            {
                completion(false)
            }
            
        }
    }
    
}

