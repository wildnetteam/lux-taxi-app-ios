//
//  UserViewModel.swift
//  LUX Application
//

import UIKit

class UserProfileViewModel: NSObject {
    private var signUpModel: UserProfileModel
    
    var name: String? {
        return signUpModel.name
    }
    var email: String? {
        return signUpModel.email
    }
    var image: String? {
        return signUpModel.image
    }
    var facebookId: String? {
        return signUpModel.facebookId
    }
    var signUpType: String? {
        return signUpModel.signUpType
    }
    var googleId: String? {
        return signUpModel.googleId
    }
    
    init(user: UserProfileModel) {
        self.signUpModel = user
    }
    
}
