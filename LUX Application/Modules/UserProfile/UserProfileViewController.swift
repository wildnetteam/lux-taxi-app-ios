//
//  SignUpViewController.swift
//  LUX Application
//
//  Created by Yogita on 30/01/18.
//  Copyright © 2018 WildnetTechnologies. All rights reserved.
//

import UIKit

class UserProfileViewController: UIViewController {
    
    @IBOutlet weak var TextFieldEmail: UITextField!
    @IBOutlet weak var TextFieldPassword: UITextField!
    @IBOutlet weak var TextFieldMobile: UITextField!
    @IBOutlet weak var TextFieldDOB: UITextField!
    @IBOutlet weak var imageViewProfile: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var dobButton: UIButton!
    @IBOutlet weak var imageButton: UIButton!
    @IBOutlet weak var textFieldCountry: UITextField!
    @IBOutlet weak var textFieldState: UITextField!
    @IBOutlet weak var textFieldCity: UITextField!
    @IBOutlet weak var textFieldZipCode: UITextField!
    @IBOutlet weak var changePasswordButton: UIButton!
    @IBOutlet weak var textviewAddress: UITextView!
    @IBOutlet weak var DobBg: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var datePickerView: UIView!
    @IBOutlet weak var stateButton: UIButton!
    @IBOutlet weak var countryButton: UIButton!
    @IBOutlet weak var datePickerBottomLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var statePicker: UIPickerView!
    @IBOutlet weak var pickerLabel: UILabel!
    
    var imagePicker = UIImagePickerController()
    var date = ""
    var isImage = false
    var model : UserViewModel?
    var selectedState = 0
    var selectedCountry = 0
    var isDatePickerOpen = false
    var isStatePickerOpen = false
    var isCountryPickerOpen = false
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.statePicker.delegate = self
        self.statePicker.dataSource = self
        self.textviewAddress.tintColor = UIColor.black
        enableEditing(isEnable: false)
        self.setMinimumDate()
        getDataForUserProfile()
        setData()
        self.imagePicker.delegate = self
        self.datePickerBottomLayoutConstraint.constant = -self.datePickerView.frame.size.height
        self.view.layoutIfNeeded()
        Utility.roundView(view: self.imageViewProfile, radius: 50)
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Utility.enableSwipe(enable: false,swiftySideMenu:swiftySideMenu)
    }
    
    //MARK:------Validation--------
    
    func validateSignUpFields() ->Bool
    {
        let success = true
        
        if ( Utility.isEmpty(self.TextFieldEmail.text!) )
        {
            Utility.snackbar(message:  K.MESSAGE.k_email_Empty,vc:self)
            return false;
        }
        else if (!Utility.validateEmail( self.TextFieldEmail.text! ))
        {
            Utility.snackbar(message: K.MESSAGE.k_email_Invalid,vc:self)
            return false;
        }
        else if ( Utility.isEmpty(self.TextFieldPassword.text!) )
        {
            Utility.snackbar(message: K.MESSAGE.k_pass_Empty,vc:self)
            return false;
        }
        else if (!Utility.validatePassword(self.TextFieldPassword.text!) )
        {
            Utility.snackbar(message: K.MESSAGE.k_pass_length,vc:self)
            return false;
        }
        else if ( Utility.isEmpty(self.TextFieldMobile.text!) )
        {
            Utility.snackbar(message: K.MESSAGE.k_mob_Empty,vc:self)
            return false;
        }
        else if (!Utility.validateMob( self.TextFieldMobile.text! ))
        {
            Utility.snackbar(message: K.MESSAGE.k_mob_Invalid,vc:self)
            return false;
        }
        else if (Utility.isEmpty(self.TextFieldDOB.text!) )
        {
            Utility.snackbar(message: K.MESSAGE.k_dob_Empty,vc:self)
            return false;
        }
        else if (Utility.isEmpty(self.textviewAddress.text!) )
        {
            Utility.snackbar(message: K.MESSAGE.k_address_Empty,vc:self)
            return false;
        }
        else if (self.textviewAddress.text.characters.count > 50 )
        {
            Utility.snackbar(message: K.MESSAGE.k_address_Valid,vc:self)
            return false;
        }
        else if (Utility.isEmpty(self.textFieldState.text!) )
        {
            Utility.snackbar(message: K.MESSAGE.k_state_Empty,vc:self)
            return false;
        }
        else if (Utility.isEmpty(self.textFieldCity.text!) )
        {
            Utility.snackbar(message: K.MESSAGE.k_city_Empty,vc:self)
            return false;
        }
        else if (!(((self.textFieldCity.text?.characters.count)! > 2) && ((self.textFieldCity.text?.characters.count)! <= 50)))
        {
            Utility.snackbar(message: K.MESSAGE.k_city_Invalid,vc:self)
            return false;
        }
        else if (Utility.isEmpty(self.textFieldZipCode.text!) )
        {
            Utility.snackbar(message: K.MESSAGE.k_zipcode_Empty,vc:self)
            return false;
        }
        else if (self.textFieldZipCode.text?.characters.count != 5)
        {
            Utility.snackbar(message: K.MESSAGE.k_zipcode_Invalid,vc:self)
            return false;
        }
        return success;
        
    }
    
    
    //MARK: IBAction
    
    @IBAction func menuButtonTapped(_ sender: UIButton) {
        self.swiftySideMenu?.toggleSideMenu()
        
    }
    
    @IBAction func editButtonTapped(_ sender: UIButton) {
        if sender.tag == 100
        {
            self.enableEditing(isEnable: true)
            sender.tag = 200
        }
        else
        {
            self.editUserDetails()
        }
        
    }
    
    @IBAction func doneButtonTapped(_ sender: UIButton) {
        if(self.isDatePickerOpen){
            self.datePickerBottomLayoutConstraint.constant = 0
            self.view.layoutIfNeeded()
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd yyyy"
            let convertedDate = dateFormatter.string(from: self.datePicker.date as Date)
            self.TextFieldDOB.text = convertedDate
            dateFormatter.dateFormat = "yyyy-MM-dd"
            self.date = dateFormatter.string(from: self.datePicker.date as Date)
            UIView.animate(withDuration: Double(0.5), animations: {
                self.datePickerBottomLayoutConstraint.constant = -self.datePickerView.frame.size.height
                self.view.layoutIfNeeded()
            })
            { (true) in
                self.DobBg.isHidden = true
            }
        }
        if(isStatePickerOpen){
            self.textFieldState.text = self.localStates()[selectedState]
            self.datePickerBottomLayoutConstraint.constant = 0
            self.view.layoutIfNeeded()
            UIView.animate(withDuration: Double(0.5), animations: {
                self.datePickerBottomLayoutConstraint.constant = -self.datePickerView.frame.size.height
                self.view.layoutIfNeeded()
            })
            { (true) in
                self.DobBg.isHidden = true
            }
        }
        if(isCountryPickerOpen){
            self.textFieldCountry.text = "United States"
            self.datePickerBottomLayoutConstraint.constant = 0
            self.view.layoutIfNeeded()
            UIView.animate(withDuration: Double(0.5), animations: {
                self.datePickerBottomLayoutConstraint.constant = -self.datePickerView.frame.size.height
                self.view.layoutIfNeeded()
            })
            { (true) in
                self.DobBg.isHidden = true
            }
    }
    }
    
    
    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        self.datePickerBottomLayoutConstraint.constant = 0
        self.view.layoutIfNeeded()
        
        UIView.animate(withDuration: Double(0.5), animations: {
            self.datePickerBottomLayoutConstraint.constant = -self.datePickerView.frame.size.height
            self.view.layoutIfNeeded()
        })
        { (true) in
            self.DobBg.isHidden = true
        }
    }
    
    @IBAction func signUpButtonTapped(_ sender: UIButton) {
        if validateSignUpFields()
        {
            //  sendDataForSignUp()
        }
    }
    
    @IBAction func uploadImageButtonTapped(_ sender: UIButton) {
        let actionSheetController: UIAlertController = UIAlertController(title: "Choose an option", message: "", preferredStyle: .alert)
        
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            //Just dismiss the action sheet
        }
        actionSheetController.addAction(cancelAction)
        //Create and add first option action
        let takePictureAction: UIAlertAction = UIAlertAction(title: "Take Picture", style: .default) { action -> Void in
            self.openCamera()
        }
        actionSheetController.addAction(takePictureAction)
        //Create and add a second option action
        let choosePictureAction: UIAlertAction = UIAlertAction(title: "Choose Existing", style: .default) { action -> Void in
            self.openGallery()
        }
        actionSheetController.addAction(choosePictureAction)
        
        //Present the AlertController
        self.present(actionSheetController, animated: true, completion: nil)
    }
    

    @IBAction func dateOfBirthButtonTapped(_ sender: UIButton) {
        self.pickerLabel.text = "Date of Birth"
        self.isDatePickerOpen = true
        self.isCountryPickerOpen = false
        self.isStatePickerOpen = false
        self.datePicker.isHidden = false
        self.statePicker.isHidden = true
        self.view.endEditing(true)
        self.DobBg.isHidden = false
        self.datePickerBottomLayoutConstraint.constant = -self.datePickerView.frame.size.height
        self.view.layoutIfNeeded()
        
        UIView.animate(withDuration: Double(0.5), animations: {
            self.datePickerBottomLayoutConstraint.constant = 0
            self.view.layoutIfNeeded()
        })
    }
    
    @IBAction func countryButtonTapped(_ sender: UIButton) {
        self.pickerLabel.text = "Select Country"
        self.isDatePickerOpen = false
        self.isCountryPickerOpen = true
        self.isStatePickerOpen = false
        self.datePicker.isHidden = true
        self.statePicker.isHidden = false
        self.view.endEditing(true)
        self.DobBg.isHidden = false
        self.statePicker.reloadAllComponents()
        self.datePickerBottomLayoutConstraint.constant = -self.datePickerView.frame.size.height
        self.view.layoutIfNeeded()
        
        UIView.animate(withDuration: Double(0.5), animations: {
            self.datePickerBottomLayoutConstraint.constant = 0
            self.view.layoutIfNeeded()
        })
        
    }
    
    
    @IBAction func stateButtonTapped(_ sender: UIButton) {
        self.pickerLabel.text = "Select State"
        self.isDatePickerOpen = false
        self.isCountryPickerOpen = false
        self.isStatePickerOpen = true
        self.datePicker.isHidden = true
        self.statePicker.isHidden = false
        self.view.endEditing(true)
        self.DobBg.isHidden = false
        self.statePicker.reloadAllComponents()
        self.datePickerBottomLayoutConstraint.constant = -self.datePickerView.frame.size.height
        self.view.layoutIfNeeded()
        
        UIView.animate(withDuration: Double(0.5), animations: {
            self.datePickerBottomLayoutConstraint.constant = 0
            self.view.layoutIfNeeded()
        })
    }
    
    
    //MARK: getUserProfile
    
    @IBAction func changePasswordButtontapped(_ sender: UIButton) {
       Utility.pushViewController(fromVC: self, toVC: ChangePasswordViewController(), identifier: "ChangePasswordViewController")
        
    }
    func getDataForUserProfile()
    {
        let parameters: [String:String] = [:]
        UserProfileManager.getUserProfileData(parameters: parameters as [String : AnyObject], vc:self, completion: { (success) in
            if(success == true)
            {
               
                NotificationCenter.default.post(name: Notification.Name("ReloadTable"), object: nil)
                self.setData()
            }
        })
    }
    
    func editUserDetails()
    {
        var user_image : UIImage?
        user_image = self.imageViewProfile.image!
        
        if isImage{
            user_image = self.imageViewProfile.image!
        }
        else
        {
            user_image = nil
        }
        /*
         "email": 'Valid email required',
         "mobile": 'required',
         "image": 'if image changed other wise key value will be blank',
         */
        if(validateSignUpFields()){
            let parameters: [String:String] = ["email":self.TextFieldEmail.text!,"mobile":self.TextFieldMobile.text!,"dob":self.date,"address":self.textviewAddress.text,"state":self.textFieldState.text!,"country":self.textFieldCountry.text!,"zipcode":self.textFieldZipCode.text!,"city":self.textFieldCity.text!]
            UserProfileManager.postUserProfileData(parameters: parameters as [String : AnyObject], image: user_image, vc: self, completion: { (success) in
                if(success == true)
                {
                     Utility.snackbar(message: "Your profile details have been updated successfully." as! String,vc:self)
                    self.setData()
                    self.editButton.tag = 100
                    self.enableEditing(isEnable: false)
                }
            })
        }
        
        
    }
    
    
    func convertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let convertedDate = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "MMM dd yyyy"
        if((convertedDate) != nil)
        {
            return  dateFormatter.string(from: convertedDate!)
        }
        else
        {
            return ""
        }
        
    }
    
    func setData()
    {
        model = UserViewModel.init(user: UserDefaultManager())
        if let model = model
        {
            self.userNameLabel.text = model.name
            self.TextFieldEmail.text = model.email
            self.TextFieldMobile.text = model.mobile
            self.TextFieldDOB.text = model.dob
            self.textviewAddress.text = model.address
            if let date = model.dob
            {
                self.date = date
            }
            if let dob = model.dob
            {
                    self.TextFieldDOB.text = convertDateFormater(dob)
            }
            if let city = model.city
            {
                self.textFieldCity.text = city
            }
            if let country = model.country
            {
                self.textFieldCountry.text = country
            }
            if let state = model.state
            {
                self.textFieldState.text = state
            }
            if let zipcode = model.zipcode
            {
                self.textFieldZipCode.text = zipcode
            }
            if let image = model.image
            {
                if let url = URL(string : image)
                {
                    self.imageViewProfile.setImageWith(url, placeholderImage: #imageLiteral(resourceName: "avatar-placeholder"))
                }
            }
            NotificationCenter.default.post(name: Notification.Name("ReloadTable"), object: nil)

        }
    }
    
    func enableEditing(isEnable:Bool)
    {
        self.textviewAddress.isEditable = isEnable
        self.TextFieldMobile.isEnabled = isEnable
        self.changePasswordButton.isUserInteractionEnabled = isEnable
        self.dobButton.isUserInteractionEnabled = isEnable
        self.stateButton.isUserInteractionEnabled = isEnable
        self.countryButton.isUserInteractionEnabled = isEnable
        self.TextFieldEmail.isEnabled = isEnable
        self.textFieldCity.isEnabled = isEnable
        self.textFieldZipCode.isEnabled = isEnable
        self.imageButton.isUserInteractionEnabled = isEnable
        if(!isEnable)
        {
            self.editButton.setImage(#imageLiteral(resourceName: "edit"), for: .normal)
            self.editButton.setTitle(nil, for: .normal)
        }
        else
        {
            self.editButton.setImage(nil, for: .normal)
            self.editButton.setTitle("Save", for: .normal)
        }
    }
    
}

extension UserProfileViewController:UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate{
    
    func openGallery()
    {
        self.imagePicker.allowsEditing = true
        self.imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        present(self.imagePicker, animated: true, completion: nil)
    }
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            self.imagePicker.allowsEditing = true
            self.imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            self.imagePicker.cameraCaptureMode = .photo
            present(self.imagePicker, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerEditedImage] as! UIImage
        isImage = true
        self.imageViewProfile.image = chosenImage
        dismiss(animated: true, completion: nil)
        
    }
    
    
    func setMinimumDate()
    {
        var components = DateComponents()
        components.year = -100
        let minDate = Calendar.current.date(byAdding: components, to: Date())
        
        components.year = -13
        let maxDate = Calendar.current.date(byAdding: components, to: Date())
        
        datePicker.minimumDate = minDate
        datePicker.maximumDate = maxDate
    }
    
}

extension UserProfileViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag  = textField.tag + 1
        // Try to find next responder
        var nextResponder : UIResponder?
        nextResponder = ((textField.superview?.superview?.viewWithTag(nextTag)))
        if((nextResponder) != nil){
            // Found next responder, so set it.
            nextResponder?.becomeFirstResponder()
        }
        else{
            textField.resignFirstResponder()
        }
        return false
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        if(textField == TextFieldMobile){
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= 16
        }
        return true
       
    }
}

extension UserProfileViewController : UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(self.isStatePickerOpen){
            return self.localStates().count
        }
        if(self.isCountryPickerOpen){
            return 1
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(self.isStatePickerOpen){
            return self.localStates()[row]
        }
        if(self.isCountryPickerOpen){
            return "United States"
        }
        return  ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(self.isStatePickerOpen){
            self.selectedState = row
        }
        if(self.isCountryPickerOpen){
            self.selectedCountry = row
        }
    }
    
    fileprivate func localStates() -> [String] {
        if let path = Bundle.main.path(forResource: "State", ofType: "json") {
            do {
                let jsonData = try Data(contentsOf: URL(fileURLWithPath: path), options: .dataReadingMapped)
                let jsonResult = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as! [[String:String]]
                
                var countryNames = [String]()
                for country in jsonResult {
                    countryNames.append(country["name"]!)
                }
                
                return countryNames
            } catch {
                print("Error parsing jSON: \(error)")
                return []
            }
        }
        return []
    }
    
}
