//
// UserModel.swift
//  LUX Application
//

import UIKit

class UserModel: NSObject {
    var id : String?
    var email : String?
    var name : String?
    var status : Int?
    var loginType : String?
    var dob : String?
    var image:String?
    var signUpType : String?
    var twitterId : String?
    var facebookId : String?
    var mobile : String?
    var userStatus : String?
    var address : String?
    var country : String?
    var state : String?
    var city : String?
    var zipcode : String?
    
    init(result : Dictionary<String,Any>) {
        self.id = result["id"] as? String
        self.status = result["status"] as? Int
        self.dob = result["dob"] as? String
        self.image = result["image"] as? String
        self.signUpType = result["sign_up_type"] as? String
        self.twitterId = result["gmail_id"] as? String
        self.name = result["name"] as? String
        self.loginType = result["loginType"] as? String
        self.facebookId = result["facebook_id"] as? String
        self.email = result["email"] as? String
        self.address = result["address"] as? String
        
        self.country = result["country"] as? String
        self.state = result["state"] as? String
        self.city = result["city"] as? String
        self.zipcode = result["zipcode"] as? String
        
        if let mob = result["mobile"]
        {
            self.mobile = mob as? String
        }
        else if let mob = result["phone"]
        {
            self.mobile = mob as? String
        }
        self.userStatus = result["user_status"] as? String
        UserDefaultManager.setCurrentDob(dob: dob)
        UserDefaultManager.setCurrentAddress(address: address)
        UserDefaultManager.setCurrentMobile(mob: mobile)
        UserDefaultManager.setCurrentEmail(email:email)
        UserDefaultManager.setIsLogin(isLogin: true)
        UserDefaultManager.setCurrentName(isLogin: name)
        UserDefaultManager.setCurrentImage(image: image)
        
        UserDefaultManager.setCurrentCity(city: city)
        UserDefaultManager.setCurrentState(state: state)
        UserDefaultManager.setCurrentCountry(state: country)
        UserDefaultManager.setCurrentZipcode(state: zipcode)
      
        if let id = self.id
        {
            UserDefaultManager.setUserId(id:id )

        }
        if let id = twitterId
        {
            if id.count > 0
            {
                UserDefaultManager.setSocialId(id: id)
            }
            else if let id = facebookId
            {
                if id.count > 0
                {
                    UserDefaultManager.setSocialId(id: id)
                }
            }
        }
        NotificationCenter.default.post(name: Notification.Name("ReloadTable"), object: nil)

    }
    
}
