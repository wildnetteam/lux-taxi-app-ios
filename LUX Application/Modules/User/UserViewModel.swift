//
//  UserViewModel.swift
//  LUX Application
//

import UIKit

class UserViewModel: NSObject {
        private var userModel: UserDefaultManager
        
        var name: String? {
            return UserDefaultManager.getCurrentName()
        }
        var email: String? {
            return UserDefaultManager.getCurrentEmail()
        }
        var mobile: String? {
            return UserDefaultManager.getCurrentMobile()
        }
        var dob: String? {
            return UserDefaultManager.getCurrentDob()
        }
    
    var city: String? {
        return UserDefaultManager.getCurrentCity()
    }
    var country: String? {
        return UserDefaultManager.getCurrentCountry()
    }
    var state: String? {
        return UserDefaultManager.getCurrentState()
    }
    var zipcode: String? {
        return UserDefaultManager.getCurrentZipcode()
    }
    
        var address: String? {
            return UserDefaultManager.getCurrentAddress()
        }
        var image: String? {
            if let img =  UserDefaultManager.getCurrentImage()
            {
                if img.range(of:"http") != nil {
                 return UserDefaultManager.getCurrentImage()
                }
                else
                {
                    if let imagePath = UserDefaultManager.getImagePath()
                    {
                        if let image = UserDefaultManager.getCurrentImage()
                        {
                            return imagePath + image
                        }
                    }
                }
            }
            
            return ""
        }
        var socialID: String? {
            return UserDefaultManager.getSocialId()
        }

        init(user: UserDefaultManager) {
            self.userModel = user
        }
  
}
