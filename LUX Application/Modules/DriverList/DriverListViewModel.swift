//
//  DriverListViewModel.swift
//  LUX Application
//

import UIKit

class DriverListViewModel: NSObject {
    private var driverModel: DriverListModel
    var image : String?
    {
        guard let imgPath = driverModel.imagePath else{
            return ""
        }
        guard let img = driverModel.driverImage else{
            return ""
        }
       return imgPath + img
    }
    
    var distance : String?
    {
        return driverModel.distance
    }
    
    var amenities : String?
    {
        return driverModel.amenities
    }
    var driverName : String?{
        return driverModel.driverName
    }
    
    var driverId : String?{
        return driverModel.driverId
    }
    var pricePerMile : String?{
        return driverModel.pricePerMile

    }
    var basePrice : String?{
        return driverModel.basePrice
    }
    var seats : String?{
        return driverModel.seats
    }
    var totalPrice : String?{
        return driverModel.totalPrice
    }
    var totalDistance : String?{
        return driverModel.totalDistance
        
    }
    var rateID : String?{
        return driverModel.rateCardId
        
    }
    var totalTime : String?{
        return driverModel.totalTime
    }
    var rating : String?{
        return driverModel.rating
    }
    var carDetails : String?{
        guard let carName = driverModel.carName else{
            return ""
        }
        guard let carNumb = driverModel.carNumber else{
            return ""
        }
        return carNumb + " | " + carName
    }
   
    init(model: DriverListModel) {
        self.driverModel = model
    }
    
}
