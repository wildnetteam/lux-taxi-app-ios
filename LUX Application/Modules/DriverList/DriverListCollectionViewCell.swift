//
//  DriverListCollectionViewCell.swift
//  LUX Application
//
//  Created by Yogita on 16/02/18.
//  Copyright © 2018 WildnetTechnologies. All rights reserved.
//

import UIKit
import HCSStarRatingView

class DriverListCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet var distanceLabel: UILabel!
    @IBOutlet weak var diverName: UILabel!
    @IBOutlet weak var ratingView: HCSStarRatingView!
    @IBOutlet weak var carDetails: UILabel!
    @IBOutlet weak var numberOfseats: UILabel!
    @IBOutlet weak var pricePerMile: UILabel!
    @IBOutlet weak var servicesLabel: UILabel!
    @IBOutlet weak var bookButton: UIButton!
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var ratingViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var ratingViewWidth: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
       
        if UIScreen.main.sizeType == .iPhone5 {
            ratingViewHeight.constant = 10
            ratingViewWidth.constant = 60
        }
    }
    
    
    func setdata(model:DriverListViewModel){
        self.layoutIfNeeded()
         Utility.roundView(view: self.profileImageView, radius: self.profileImageView.frame.size.width/2)
        if let image = model.image
        {
            if let url = URL(string : image)
            {
                self.profileImageView.setImageWith(url, placeholderImage: #imageLiteral(resourceName: "avatar-placeholder"))
            }
        }
        if let distance = model.distance{
            self.distanceLabel.text = distance + " miles away"
        }
        self.diverName.text = model.driverName
        if let rate = model.rating{
            if let n = NumberFormatter().number(from: rate) {
                ratingView.value = CGFloat(truncating: n)
            }
        }
        self.carDetails.text = model.carDetails
        if let seats = model.seats{
            self.numberOfseats.text = String(seats)
        }
        if let price = model.totalPrice{
            self.pricePerMile.text = "$"+price
        }
        self.servicesLabel.text = model.amenities
    }

}
