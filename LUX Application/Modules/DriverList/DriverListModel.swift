//
// DriverListModel.swift
//  LUX Application
//

import UIKit

class DriverListModel: NSObject {
    var imagePath : String?
    var driverImage : String?
    var amenities : String?
    var driverName : String?
    var pricePerMile : String?
    var basePrice : String?
    var seats : String?
    var rating : String?
    var carName : String?
    var carNumber : String?
    var totalPrice : String?
    var totalDistance :String?
    var distance : String?
    var totalTime : String?
    var rateCardId : String?
    var driverId :String?
    /*
     {
     bookingdata =     {
     data =         (
     {
     amount = 254;
     "approx_time" = "31 mins";
     "assigned_for" = 1;
     distance = "7.7 mi";
     id = 48;
     status = 1;
     "status_code" = pending;
     "user_id" = 23;
     }
     );
     status = success;
     };
     code = 409;
     driverdata =     {
     data =         {
     "car_name" = 1;
     "car_type" = 1;
     cardata =             {
     amenities = "test,testst";
     "car_image" = "Type-Hatchback2.png";
     "car_name" = "Car 1";
     "car_no" = 123456;
     carimagepath = "http://site4demo.com/luxapp-admin/car_image/";
     "seating_capacity" = 7;
     };
     "driver_id" = 1;
     image = "boy-5122.png";
     name = "Driver Name 1";
     ratecard =             {
     baseprice = "100.00";
     permile = "20.00";
     "ratecard_id" = 1;
     "ratecard_name" = "Rate Card 1";
     };
     };
     driverimagepath = "http://site4demo.com/luxapp-admin/driverimages/";
     status = success;
     };
     message = "Waiting for driver to accept the request.";
     status = success;
     userdata =     {
     data =         (
     {
     id = 23;
     image = "user9313-file.png";
     mobile = 1236547893;
     name = "Yogita Bachani";
     }
     );
     status = success;
     userimagepath = "http://site4demo.com/luxapp-admin/user_image/";
     };
     }
     */
    
    
    
    
    class func initModel(res:[String : Any]) -> [DriverListModel]
    {
        var driverList = [DriverListModel]()
        if let data = res["data"]
        {
            for dic in data as! [[String:Any]]
            {
                let model = DriverListModel()
                model.imagePath = res["driverimagepath"] as? String
                model.totalDistance = res["approx_distance"] as? String
                model.totalTime = res["approx_time"] as? String

                if let cardata = dic["cardata"]{
                    if let carDic = cardata as? NSDictionary
                    {
                        model.amenities = carDic["amenities"] as? String
                        model.carName = carDic["car_name"] as? String
                        model.carNumber = carDic["car_no"] as? String
                        model.seats = carDic["seating_capacity"] as? String
                    }
                }
                if let ratedata = dic["ratecard"]{
                    if let rateDic = ratedata as? NSDictionary
                    {
                        model.rateCardId = rateDic["ratecard_id"] as? String
                        model.pricePerMile = rateDic["permile"] as? String
                        model.basePrice = rateDic["baseprice"] as? String
                        model.totalPrice = rateDic["approx_price"] as? String
                    }
                }
                
                model.distance = dic["distance"] as? String
                model.rating = dic["rating"] as? String
                model.driverId = dic["driver_id"] as? String
                model.driverName = dic["name"] as? String
                model.driverImage = dic["image"] as? String
                driverList.append(model)
            }
        }
        return driverList
        
    }
    
}




/*
 {
 "status": "success",
 "message": "Driver data exist.",
 "data": [
 {
 "image": "boy-5122.png",
 "name": "Driver Name 1",
 "rating": 0,
 "cardata": {
 "car_name": "Car 1",
 "car_image": "Type-Hatchback2.png",
 "car_no": "123456",
 "seating_capacity": "7",
 "amenities": "test,testst",
 "carimagepath": "http://site4demo.com/luxapp-admin/car_image/"
 },
 "ratecard": {
 "ratecard_name": "Rate Card 1",
 "baseprice": "100.00",
 "permile": "20.00",
 "approx_price": "504.00"
 }
 }
 ],
 "code": 409,
 "approx_distance": "20.2 mi",
 "approx_time": "1 hour 15 mins",
 "driverimagepath": "http://site4demo.com/luxapp-admin/driverimages/",
 "appuserstatus": 1
 }
 
 */


