//
//  LegalTableViewCell.swift
//  LUX Application
//
//  Created by Yogita on 13/03/18.
//  Copyright © 2018 WildnetTechnologies. All rights reserved.
//

import UIKit

class LegalTableViewCell: UITableViewCell {

    @IBOutlet weak var tiitleLabel: UILabel!
    let titleArr = ["Terms and Conditions","Privacy Policy","Location Information"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setData(index:Int)
    {
        self.tiitleLabel.text = titleArr[index]
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
