//
//  LegalViewController.swift
//  LUX Application
//
//  Created by Yogita on 13/03/18.
//  Copyright © 2018 WildnetTechnologies. All rights reserved.
//

import UIKit

class LegalViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
     let titleArr = ["Terms and Conditions","Privacy Policy","Location Information"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func menuButtonTapped(_ sender: UIButton) {
        self.swiftySideMenu.toggleSideMenu()
    }
    
    func checkURL(index : Int) -> String{
        switch index {
        case 0:
            return K.URL.urlTc
        case 1:
            return K.URL.urlPrivacy
        case 2:
            return K.URL.urlLocationInfo
        default:
            break
        }
        return K.URL.urlTc
    }
}

extension LegalViewController:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LegalDetailViewController") as! LegalDetailViewController
        vc.titleText = titleArr[indexPath.row]
        vc.url = checkURL(index: indexPath.row)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LegalTableViewCell")
            as! LegalTableViewCell
        cell.selectionStyle = .none
        cell.setData(index: indexPath.row)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}
