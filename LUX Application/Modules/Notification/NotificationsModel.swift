//
// HomeModel.swift
//  LUX Application
//

import UIKit

class NotificationsModel: NSObject {
    var driverimagepath : String?
    var image:String?
    var name:String?
    var notificationMsg:String?

    class func initModel(res:[String : Any]) -> [NotificationsModel]
    {
         var notificationData = [NotificationsModel]()
        if let data = res["data"]{
            if let bookingDataArray = data as? [[String:Any]]{
                for dic in bookingDataArray {
                   let model = NotificationsModel()
                     model.notificationMsg = dic["msg"] as? String
                    if let driverData = dic["driverdata"] as? [String:Any]{
                         model.driverimagepath = driverData["driverimagepath"] as? String
                        if let driverdata = driverData["data"] as? [String:Any]{
                            model.image = driverdata["image"] as? String
                            model.name = driverdata["name"] as? String
                        }
                    }
                   notificationData.append(model)
            }
        }
        }
        return notificationData
        
    }
    
    
    
    
    
    
}

