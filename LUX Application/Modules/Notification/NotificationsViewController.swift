//
//  MyTripsViewController.swift
//  LUX Application
//
//  Created by Yogita on 26/02/18.
//  Copyright © 2018 WildnetTechnologies. All rights reserved.
//

import UIKit

class NotificationsViewController: UIViewController {

    @IBOutlet weak var myTripsTableView: UITableView!
    var notificationModel : [NotificationsViewModel] = []
    var offset = "0"
    var isDataLoading = false
        
    @IBOutlet weak var noTripsLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadData(notification:)), name: Notification.Name(K.KEY.refreshNotification), object: nil)
        myTripsTableView.register(UINib.init(nibName: "NotificationsTableViewCell", bundle: nil), forCellReuseIdentifier: "NotificationsTableViewCell")
        getNotifications()
        // Do any additional setup after loading the view.
    }

    @objc func reloadData(notification: Notification){
        self.notificationModel = []
        self.myTripsTableView.reloadData()
        getNotifications()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    func getNotifications(){
        NotificationsAPIManager.getNotificationsAPI(parameters: [:], vc: self) { (success, response) in
            if success == true{
                if response.count > 0{
                    self.noTripsLabel.isHidden = true
                    for model in response{
                self.notificationModel.append(NotificationsViewModel.init(model: model))
//                        if let offset = model.offset{
//                            self.offset =  String(offset)
//                        }
                    }
                    
                    self.myTripsTableView.reloadData()
                }
            }
           
        }
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
       Utility.popViewController(fromVC: self)
    }
    

}
extension NotificationsViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return self.notificationModel.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : NotificationsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "NotificationsTableViewCell", for: indexPath) as! NotificationsTableViewCell
        cell.selectionStyle = .none
      cell.setData(model: notificationModel[indexPath.row])
        return cell
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
  /*  func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: 0) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
            // print("this is the last cell")
            let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            self.myTripsTableView.tableFooterView = spinner
            self.myTripsTableView.tableFooterView?.isHidden = false
        }*/
    }
    
    //ScrollViewDelegates
    
   /* func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        print("scrollViewWillBeginDragging")
        isDataLoading = false
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("scrollViewDidEndDecelerating")
    }
    
    //Pagination
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        print("scrollViewDidEndDragging")
        if ((myTripsTableView.contentOffset.y + myTripsTableView.frame.size.height) >= myTripsTableView.contentSize.height)
        {
            if !isDataLoading{
                isDataLoading = true
                getTrips()
            }
        }
        
        
    }
    
}*/

