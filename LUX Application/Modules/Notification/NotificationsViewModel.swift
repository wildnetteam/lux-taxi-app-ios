//
//  NotificationsViewModel.swift
//  LUX Application
//

import UIKit

class NotificationsViewModel: NSObject {
    private var tripsModel: NotificationsModel

    var name:String?
    {
        return tripsModel.name
    }
    var notificationMsg:String?{
        return tripsModel.notificationMsg
    }
    var image : String?
    {
        guard let imgPath = tripsModel.driverimagepath else{
            return ""
        }
        guard let img = tripsModel.image else{
            return ""
        }
        return imgPath + img
    }
    
    
    init(model: NotificationsModel) {
        self.tripsModel = model
    }
}
