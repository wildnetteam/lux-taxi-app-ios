//
//  NotificationsTableViewCell.swift
//  LUX Application
//
//  Created by Yogita on 21/03/18.
//  Copyright © 2018 WildnetTechnologies. All rights reserved.
//

import UIKit

class NotificationsTableViewCell: UITableViewCell {

    @IBOutlet weak var driverImage: UIImageView!
    @IBOutlet weak var driverName: UILabel!
    @IBOutlet weak var notificationMsg: UILabel!
    @IBOutlet weak var bgView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        Utility.addShadow(view: bgView)
         Utility.roundView(view: driverImage, radius: driverImage.frame.size.width/2)
    }
    
    
    func setData(model:NotificationsViewModel){
        self.driverName.text = model.name
        self.notificationMsg.text = model.notificationMsg
        if let image = model.image
        {
            if let url = URL(string : image)
            {
                self.driverImage.setImageWith(url, placeholderImage: #imageLiteral(resourceName: "avatar-placeholder"))
            }
        }
  
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
