//
// WaitingModel.swift
//  LUX Application
//

import UIKit

class WaitingModel: NSObject {
    var imagePath : String?
    var driverImage : String?
    var amenities : String?
    var driverName : String?
    var pricePerMile : String?
    var basePrice : String?
    var seats : String?
    var rating : String?
    var carName : String?
    var carNumber : String?
    var totalPrice : String?
    var totalDistance :String?
    var totalTime : String?
    var rateCardId : String?
    var driverId :String?
    var status : String?
    var bookingId : String?
    var drop_area : String?
    var pickup_area : String?
    var phone : String?
    var passengers : String?
    var pickup_lat : String?
    var pickup_long : String?
    var drop_lat : String?
    var drop_long : String?
    var bookingDateTime : String?
    var paymentStatus: String?
    var actualTime : String?
    var actualDistance : String?
    var finalamount : String?
    var userRating : String?
    var cancellationfee : String?
    var cancelledStatus : String?
  
    /*
     {
     bookingdata =     {
     data =         (
     {
     amount = 254;
     "approx_time" = "31 mins";
     "assigned_for" = 1;
     distance = "7.7 mi";
     id = 48;
     status = 1;
     "status_code" = pending;
     "user_id" = 23;
     }
     );
     status = success;
     };
     code = 409;
     driverdata =     {
     data =         {
     "car_name" = 1;
     "car_type" = 1;
     cardata =             {
     amenities = "test,testst";
     "car_image" = "Type-Hatchback2.png";
     "car_name" = "Car 1";
     "car_no" = 123456;
     carimagepath = "http://site4demo.com/luxapp-admin/car_image/";
     "seating_capacity" = 7;
     };
     "driver_id" = 1;
     image = "boy-5122.png";
     name = "Driver Name 1";
     ratecard =             {
     baseprice = "100.00";
     permile = "20.00";
     "ratecard_id" = 1;
     "ratecard_name" = "Rate Card 1";
     };
     };
     driverimagepath = "http://site4demo.com/luxapp-admin/driverimages/";
     status = success;
     };
     message = "Waiting for driver to accept the request.";
     status = success;
     userdata =     {
     data =         (
     {
     id = 23;
     image = "user9313-file.png";
     mobile = 1236547893;
     name = "Yogita Bachani";
     }
     );
     status = success;
     userimagepath = "http://site4demo.com/luxapp-admin/user_image/";
     };
     }
     */
    /*
     "pickup_lat": "28.5975414110518",
     "pickup_long": "77.3464704886137",
     "drop_lat": "28.5355161",
     "drop_long": "77.3910265",
 */
    
    class func initModel(res:[String : Any]) -> WaitingModel
    {
        let model = WaitingModel()
        if let bookingData = res["bookingdata"]{
            if let data  = (bookingData as! [String : Any])["data"]{
                if (data as! NSArray).count > 0{
                    let dic = (data as! NSArray)[0] as! NSDictionary
                    model.passengers = dic["person"] as? String
                    model.bookingDateTime = dic["book_create_date_time"] as? String
                    model.totalPrice = dic["amount"] as? String
                    model.totalDistance = dic["distance"] as? String
                    model.totalTime = dic["approx_time"] as? String
                    model.status = dic["status"] as? String
                    model.bookingId = dic["id"] as? String
                    model.drop_area = dic["drop_area"] as? String
                    model.pickup_area = dic["pickup_area"] as? String
                    model.pickup_lat = dic["pickup_lat"] as? String
                    model.pickup_long = dic["pickup_long"] as? String
                    model.drop_lat = dic["drop_lat"] as? String
                    model.drop_long = dic["drop_long"] as? String
                    
                    model.cancellationfee = dic["cancelled_fee"] as? String
                    model.cancelledStatus = dic["cancelled_status"] as? String
                    
                    model.paymentStatus = dic["payment_status"] as? String
                    //"cancelled_fee": "100"//cancelled_status
                    model.actualTime = dic["actually_time"] as? String
                    model.actualDistance = dic["actually_distance"] as? String
                    model.finalamount = dic["final_amount"] as? String
                    model.userRating = dic["user_rating"] as? String
                    
                
                }
            }
        }
        if let driverData = res["driverdata"]{
            if let dic = (driverData as! NSDictionary)["data"] as? NSDictionary
            {
                model.imagePath = (driverData as! NSDictionary)["driverimagepath"] as? String
                let res = dic as! NSDictionary
                
                    model.totalDistance = res["approx_distance"] as? String
                    model.totalTime = res["approx_time"] as? String
                    
                    if let cardata = res["cardata"]{
                        if let carDic = cardata as? NSDictionary
                        {
                            model.amenities = carDic["amenities"] as? String
                            model.carName = carDic["car_name"] as? String
                            model.carNumber = carDic["car_no"] as? String
                            model.seats = carDic["seating_capacity"] as? String
                        }
                    }
                    if let ratedata = res["ratecard"]{
                        if let rateDic = ratedata as? NSDictionary
                        {
                            model.rateCardId = rateDic["ratecard_id"] as? String
                            model.pricePerMile = rateDic["permile"] as? String
                            model.basePrice = rateDic["baseprice"] as? String
                         //   model.totalPrice = rateDic["approx_price"] as? String
                        }
                    }
                    
                    model.rating = res["rating"] as? String
                    model.driverId = res["driver_id"] as? String
                    model.driverName = res["name"] as? String
                    model.driverImage = res["image"] as? String
                    model.phone = res["phone"] as? String
            }
        }
       
        return model
        
    }
    
}
/*
 {
 "status": "success",
 "message": "Driver data exist.",
 "data": [
 {
 "image": "boy-5122.png",
 "name": "Driver Name 1",
 "rating": 0,
 "cardata": {
 "car_name": "Car 1",
 "car_image": "Type-Hatchback2.png",
 "car_no": "123456",
 "seating_capacity": "7",
 "amenities": "test,testst",
 "carimagepath": "http://site4demo.com/luxapp-admin/car_image/"
 },
 "ratecard": {
 "ratecard_name": "Rate Card 1",
 "baseprice": "100.00",
 "permile": "20.00",
 "approx_price": "504.00"
 }
 }
 ],
 "code": 409,
 "approx_distance": "20.2 mi",
 "approx_time": "1 hour 15 mins",
 "driverimagepath": "http://site4demo.com/luxapp-admin/driverimages/",
 "appuserstatus": 1
 }
 
 */


