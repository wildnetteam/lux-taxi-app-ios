//
// WaitingViewModel.swift
//  LUX Application
//

import UIKit

class WaitingViewModel: NSObject {
    private var driverModel: WaitingModel
    
    var paymentStatus: String?{
       return driverModel.paymentStatus
    }
    var userrating: String?{
        return driverModel.userRating
    }
    var actualTime : String?{
        return driverModel.actualTime
    }
    var actualDistance : String?{
        return driverModel.actualDistance
    }
    var finalamount : String?{
        if let price = driverModel.finalamount{
            if let priceInDouble = Double(price){
                return "$" + String(format: "%.2f", priceInDouble)
            }
        }
        
        return driverModel.finalamount
    }
    
    var image : String?
    {
        guard let imgPath = driverModel.imagePath else{
            return ""
        }
        guard let img = driverModel.driverImage else{
            return ""
        }
       return imgPath + img
    }
    
    var person : String?{
        return driverModel.passengers
    }
    var bookingDateTime : String?{
        return driverModel.bookingDateTime
    }
    var amenities : String?
    {
        return driverModel.amenities
    }
    var phone : String?
    {
        return driverModel.phone
    }
    var driverName : String?{
        return driverModel.driverName
    }
    
    var driverId : String?{
        return driverModel.driverId
    }
    var pricePerMile : String?{
        return driverModel.pricePerMile

    }
    var basePrice : String?{
        return driverModel.basePrice
    }
    var seats : String?{
        return driverModel.seats
    }
    var totalPrice : String?{
        if let price = driverModel.totalPrice{
            return "$" + price
        }
        return ""
    }
    var totalDistance : String?{
        return driverModel.totalDistance
        
    }
    var rateID : String?{
        return driverModel.rateCardId
        
    }
    
    var status : String?{
       return driverModel.status
    }
    var bookingId : String?{
       return driverModel.bookingId
    }
    var totalTime : String?{
        return driverModel.totalTime
    }
    var rating : CGFloat?{
        if let rate = driverModel.rating{
            if let n = NumberFormatter().number(from: rate) {
                return CGFloat(truncating: n)
            }
        }
        return 0.0
    }
    var carDetails : String?{
        guard let carName = driverModel.carName else{
            return ""
        }
        guard let carNumb = driverModel.carNumber else{
            return ""
        }
        return carNumb + " | " + carName
    }
    
    var carName : String?{
        guard let carName = driverModel.carName else{
            return ""
        }
        return carName
    }
    
    var cancellationfee : String?{
        if let price = driverModel.cancellationfee{
            return "$" + price
        }
        return ""
    }
    var cancelledStatus : String?{
        return driverModel.cancelledStatus
    }
    
    var carNumb : String?{
        guard let carNumb = driverModel.carNumber else{
            return ""
        }
        return carNumb
    }
    
    var drop_area : String?{
        return driverModel.drop_area
    }
    var pickup_area : String?{
        return driverModel.pickup_area
    }
    
    var pickup_lat : String?{
        return driverModel.pickup_lat

    }
    var pickup_long : String?{
        return driverModel.pickup_long
        
    }
    var drop_lat : String?{
        return driverModel.drop_lat
        
    }
    var drop_long : String?{
        return driverModel.drop_long
        
    }

    init(model: WaitingModel) {
        self.driverModel = model
    }
    
}
