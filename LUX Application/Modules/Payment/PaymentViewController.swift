//
//  RatingViewController.swift
//  LUX Application
//
//  Created by Yogita on 27/02/18.
//  Copyright © 2018 WildnetTechnologies. All rights reserved.
//

import UIKit
import HCSStarRatingView
import Stripe

class PaymentViewController: UIViewController,UITextViewDelegate {
    
    @IBOutlet weak var driverName: UILabel!
    @IBOutlet weak var carDetails: UILabel!
    @IBOutlet weak var pickUpAdd: UILabel!
    @IBOutlet weak var dropAddress: UILabel!
    @IBOutlet weak var ratingView: HCSStarRatingView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    var model : WaitingViewModel?
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var driverImage: UIImageView!
    var actionStatus = "0"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Utility.roundView(view: driverImage, radius: driverImage.frame.size.width/2)
         NotificationCenter.default.addObserver(self, selector: #selector(self.paymentCallback(notification:)), name: Notification.Name("PaymentCallBack"), object: nil)
        setData()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {

    }
    
    @objc func paymentCallback(notification: Notification){
    self.dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setData(){
        self.driverName.text = model?.driverName
        self.carDetails.text = model?.carDetails
        self.pickUpAdd.text = model?.pickup_area
        self.dropAddress.text = model?.drop_area
        if let status = model?.status{
            if(status == K.STATUS.COMPLETED){
                self.priceLabel.text = model?.finalamount
            }
            else{
               self.priceLabel.text = model?.cancellationfee
            }
        }
        if let image = model?.image
        {
            if let url = URL(string : image)
            {
                self.driverImage.setImageWith(url, placeholderImage: #imageLiteral(resourceName: "avatar-placeholder"))
            }
        }
    }
    
    
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
   
    @IBAction func submitButtonTapped(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PaymentinfoViewController") as! PaymentinfoViewController
        vc.actionStatus = self.actionStatus
        vc.model = self.model
        self.present(vc, animated: true, completion: nil)
        
      /*  let addCardViewController = STPAddCardViewController()
        addCardViewController.delegate = self
       self.present(addCardViewController, animated: true, completion: nil)*/
        
      // let controller = CheckoutViewController(delegate: self)
        //self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
