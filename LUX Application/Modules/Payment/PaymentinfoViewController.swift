
//
//  PaymentinfoViewController.swift
//  Woof
//


import UIKit
import Stripe

class PaymentinfoViewController: UIViewController {
    @IBOutlet weak var cardNumberTextField: UITextField!
    @IBOutlet weak var expiryTextField: UITextField!
    @IBOutlet weak var cvvTextField: UITextField!
    @IBOutlet weak var checkBoxButton: UIButton!
    @IBOutlet weak var cardView: UIView!
    var isCheckBoxButtonTapped = false
    var model : WaitingViewModel?
    var actionStatus = "0"

    
    //MARK: Controller Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
       

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.view.endEditing(true)
        
    }
    
   
    
    // MARK: UIbutton actions
    
    @IBAction func paymentBtnTapped(_ sender: UIButton) {
        if(self.validatePaymentFields()){
            generateCradParam()
        }
       
    }
    
    func getExpirydate(expireDate:String) -> (String,String){
        if(expireDate.contains("/")){
            let dateComponents = expireDate.components(separatedBy: "/")
            if(dateComponents.count == 2){
                let month = dateComponents[0]
                let year = dateComponents[1]
                return (month,year)
            }
            else{
                return("","")
            }
        }
         return("","")
    }
    
    func generateCradParam()
    {
        let params = STPCardParams()
        let components = cardNumberTextField?.text?.components(separatedBy: CharacterSet(charactersIn: "0123456789").inverted)
        let decimalString = components?.joined(separator:"")
        params.number = decimalString
        params.name = ""
        params.expMonth = UInt(bitPattern:Int(getExpirydate(expireDate: expiryTextField.text!).0)!)
        params.expYear = UInt(bitPattern:Int(getExpirydate(expireDate: expiryTextField.text!).1)!)
        params.cvc = cvvTextField?.text
        params.currency = "usd"

        if(!(Reachability.isConnectedToNetwork())){
            Utility.showAlertWithMessage(vc: self, message: K.MESSAGE.k_network_error)
            return
        }
        
        Utility.showIndicator()
        
        STPAPIClient.shared().createToken(withCard: params) { (token: STPToken?, error: Error?) in
            guard let token = token, error == nil else {
                 Utility.hideIndicator()
                DispatchQueue.main.async {
                    Utility.showAlertWithMessage(vc: self, message: ((error?.localizedDescription)!))
                }
                return
            }
            self.paymentAPI(token: token.tokenId)
    }
    }
        func paymentAPI(token:String){
            guard let bookingId = model?.bookingId else {
                return
            }
            let appdelegate = UIApplication.shared.delegate as? AppDelegate
            let isLive = appdelegate?.isLive
            let params = ["transaction_id":token,"booking_id":bookingId,"action_type":actionStatus,"need_to_save":"0","is_live":isLive!] as [String : Any]
            PaymentAPIManager.paymentAPI(parameters: params as [String : AnyObject], vc: self) { (success) in
                if success == true{
                    DispatchQueue.main.async {
                        let alertController : UIAlertController = UIAlertController(title: K.MESSAGE.k_app_name, message: K.MESSAGE.k_paymentDone, preferredStyle: UIAlertControllerStyle.alert)
                        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) {
                            UIAlertAction in
                            self.dismiss(animated: true, completion: {
                                NotificationCenter.default.post(name: Notification.Name("PaymentCallBack"), object: nil)
                            })
                        }
                        alertController.addAction(okAction)
                        self.present(alertController, animated: true, completion: nil)
                    }
                    
                }
            }
        }
    
    
    func validatePaymentFields() ->Bool
    {
        let success = true
        
        if (Utility.isEmpty(self.cardNumberTextField.text!) )
        {
            Utility.snackbar(message: K.MESSAGE.k_cardEmpty,vc:self)
            return false;
        }
      
        else if ( Utility.isEmpty(self.expiryTextField.text!) )
        {
            Utility.snackbar(message:  K.MESSAGE.k_expiryEmpty,vc:self)
            return false;
        }
        else if(getExpirydate(expireDate: self.expiryTextField.text!) == ("","")){
            Utility.snackbar(message:  K.MESSAGE.k_expiryValid,vc:self)
            return false;
            }
        else if ( Utility.isEmpty(self.cvvTextField.text!) )
        {
            Utility.snackbar(message: K.MESSAGE.k_cvvEmpty,vc:self)
            return false;
        }
       
        return success;
        
    }

    @IBAction func backButtonTapped(_ sender: UIButton) {
     _ = self.dismiss(animated: true, completion: nil)
        
    }
    
    
    @IBAction func checkBoxBtnTapped(_ sender: UIButton) {
        if isCheckBoxButtonTapped == false {
            isCheckBoxButtonTapped = true
            self.checkBoxButton.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
        } else {
            isCheckBoxButtonTapped = false
            self.checkBoxButton.setImage(#imageLiteral(resourceName: "checkbox"), for: .normal)
            
        }
    }
    
    func donePicker(_ barBtnItem: UIBarButtonItem)
    {
        self.view.endEditing(true)
    }
    
    //MARK: Textfield Delegate
}

extension PaymentinfoViewController :UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if textField ==  cardNumberTextField
        {
            if newString.characters.count <= 19
            {
                let replacementStringIsLegal = string.rangeOfCharacter(from: CharacterSet(charactersIn: "0123456789").inverted) == nil
                
                if !replacementStringIsLegal
                {
                    return false
                }
                
                let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
                let components = newString.components(separatedBy: CharacterSet(charactersIn: "0123456789").inverted)
                
                let decimalString = components.joined(separator: "") as NSString
                let length = decimalString.length
                let hasLeadingOne = length > 0 && decimalString.character(at: 0) == (1 as unichar)
                
                if length == 0 || (length > 16 && !hasLeadingOne) || length > 19
                {
                    let newLength = (textField.text! as NSString).length + (string as NSString).length - range.length as Int
                    
                    return (newLength > 16) ? false : true
                }
                var index = 0 as Int
                let formattedString = NSMutableString()
                
                if hasLeadingOne
                {
                    formattedString.append("1 ")
                    index += 1
                }
                if length - index > 4
                {
                    let prefix = decimalString.substring(with: NSMakeRange(index, 4))
                    formattedString.appendFormat("%@-", prefix)
                    index += 4
                }
                
                if length - index > 4
                {
                    let prefix = decimalString.substring(with: NSMakeRange(index, 4))
                    formattedString.appendFormat("%@-", prefix)
                    index += 4
                }
                if length - index > 4
                {
                    let prefix = decimalString.substring(with: NSMakeRange(index, 4))
                    formattedString.appendFormat("%@-", prefix)
                    index += 4
                }
                
                let remainder = decimalString.substring(from: index)
                formattedString.append(remainder)
                textField.text = formattedString as String
                return false
            }
            else
            {
                return false
            }
            
            
        }
        else if textField == cvvTextField
        {
            if newString.characters.count <= 4
            {
                return true
            }
            else
            {
                return false
            }
        }
        else if textField == expiryTextField{
            if range.length > 0
            {
                return true
            }
            
            //Dont allow empty strings
            if string == " "
            {
                return false
            }
            
            //Check for max length including the spacers we added
            if range.location >= 5
            {
                return false
            }
            
            var originalText = textField.text
            let replacementText = string.replacingOccurrences(of: " ", with: "")
            
            //Verify entered text is a numeric value
            let digits = NSCharacterSet.decimalDigits
            for char in replacementText.unicodeScalars
            {
                if digits.hasMember(inPlane: UInt8(char.value))
                {
                    return false
                }
            }
            
            //Put / space after 2 digit
            if range.location == 2
            {
                originalText?.append("/")
                textField.text = originalText
            }
            
            return true
        }
       
       
        return true
    }
    
    func isDateChar(string:String)-> Bool{
        if (string.characters.count>0){
            let phoneRegEx = "[0-9-/ ]{1}"
            let phoneTest = NSPredicate(format:"SELF MATCHES %@", phoneRegEx)
            let result = phoneTest.evaluate(with: string)
            if(!result){
            }
            return result
        }else{
            
            return false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
   
    
    
    
}
