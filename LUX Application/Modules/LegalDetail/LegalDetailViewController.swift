//
//  LegalDetailViewController.swift
//  LUX Application
//
//  Created by Yogita on 13/03/18.
//  Copyright © 2018 WildnetTechnologies. All rights reserved.
//

import UIKit
import AFNetworking
import NVActivityIndicatorView

class LegalDetailViewController: UIViewController,UIWebViewDelegate {
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    let activityData = ActivityData()
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var webView: UIWebView!
    var titleText = "Terms and Conditions"
    var url = K.URL.urlTc
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadData(notification:)), name: Notification.Name(K.KEY.refreshLegal), object: nil)
        self.titleLabel.text = titleText
        webView.delegate = self
        webView.loadRequest(NSURLRequest(url: NSURL(string: url) as! URL) as URLRequest)
       
        // Do any additional setup after loading the view.
    }

    @objc func reloadData(notification: Notification){
     // webView.loadRequest(NSURLRequest(url: NSURL(string: url) as! URL) as URLRequest)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        Utility.popViewController(fromVC: self)
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        activityIndicator.isHidden = false


    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
activityIndicator.isHidden = true
        
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        activityIndicator.isHidden = true
    }
  

}
