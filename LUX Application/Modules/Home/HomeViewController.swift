//
//  HomeViewController.swift
//  LUX Application
//
//  Created by Yogita on 30/01/18.
//  Copyright © 2018 WildnetTechnologies. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import GoogleMaps
import Alamofire
import SwiftyJSON
import MessageUI
import NVActivityIndicatorView

class HomeViewController: UIViewController {
    
    var timer = Timer()
    var polylineTimer = Timer()
    var xPostion : CGFloat = 0
    var model : HomeViewModel?
    var driverViewModel : [DriverListViewModel] = [DriverListViewModel]()
    var imgArr : NSArray = []
    var linkArr : NSArray = []
    var time = 10
    var waitingView : WaitingView?
    var onRideView : OnRideView?
    var waitingViewModel : WaitingViewModel?
    var isNoBooking = true
    var ishistory = false
    var overlayAdded = false
    var isMarkersForPickupAdded = false
    var polyline = GMSPolyline()
    var polylineOne :GMSPolyline?
    var timeRemaining = ""
    var timeFormat = ""
    var didUserZoomed = false
    var i : Int?
    var path = GMSPath()
    var animationPath : GMSMutablePath?

    
    @IBOutlet weak var googleMap: GMSMapView!
    @IBOutlet weak var driverCollectionView: UICollectionView!
    @IBOutlet weak var textFieldFromLocation: UITextField!
    @IBOutlet weak var textFieldPassengers: UITextField!
    @IBOutlet weak var textFieldToLocation: UITextField!
    @IBOutlet var advertisementBottomConstraint: NSLayoutConstraint!
    @IBOutlet var bottomConstraint: NSLayoutConstraint!
    @IBOutlet var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var notificationButoon: MIBadgeButton!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var locationView: UIView!
    @IBOutlet weak var imageScrollView: UIScrollView!
    var isViewPresented = false
    var bounds = GMSCoordinateBounds()
    var isObserveLocationfromFirebase = false
    var driverLat : Double?
    var driverLong : Double?
    var driverOldLat : Double?
    var driverOldLong : Double?
    
    var isMarksMarked = false
    var currentStatus = "100"
    var carMarker : GMSMarker?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(Utility.isIPhoneX()){
            self.topConstraint.constant = 50
            self.advertisementBottomConstraint.constant = -30
            self.bottomConstraint.constant = -40
            self.view.layoutIfNeeded()
        }
        if (appDelegate.token != nil){
            appDelegate.updateDeviceToken(deviceToken: appDelegate.token!)
        }
        self.removeData()
        self.menuButton.setImage(#imageLiteral(resourceName: "3bar navigation copy"), for: .normal)
        setUpUIElements()
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadData(notification:)), name: Notification.Name(K.KEY.refreshBookingData), object: nil)
        NotificationCenter.default.post(name: Notification.Name("ReloadTable"), object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(self.expireRide(notification:)), name: Notification.Name("ExpireRide"), object: nil)
        // Do any additional setup after loading the view.
    }
    
    @objc func reloadData(notification: Notification){
        if (isViewPresented){
            self.viewWillAppear(true)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.isViewPresented = true
        self.currentStatus = "100"
        Utility.enableSwipe(enable: false, swiftySideMenu: self.swiftySideMenu)
        fetchUserLocation()
        setUpData()
        if appDelegate.getDrivers{
            getDrivers()
        }
        else{
             self.getCommondata()
        }
        Utility.enableSwipe(enable: true,swiftySideMenu:swiftySideMenu)
        UIApplication.shared.statusBarStyle = .default
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        isViewPresented = false
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func menuButtonTapped(_ sender: UIButton) {
        if(sender.imageView?.image == #imageLiteral(resourceName: "Back Arrow Copy")){
            self.removeData()
            self.imageScrollView.isHidden = false
            self.driverCollectionView.isHidden = true
            self.driverViewModel = []
            self.menuButton.setImage(#imageLiteral(resourceName: "3bar navigation copy"), for: .normal)
        }
        else{
            self.swiftySideMenu?.toggleSideMenu()
        }
    }
    
    func removeData()
    {
        appDelegate.fromAddress = nil
        appDelegate.toAddress = nil
        appDelegate.fromLocation = nil
        appDelegate.toLocation = nil
        appDelegate.numberOfPassenger = nil
        appDelegate.getDrivers = false
        setUpData()
    }
    
    func setUpData()
    {
        if  let toAddress = appDelegate.fromAddress
        {
            textFieldFromLocation.text = toAddress
        }
        else if let userAddress = appDelegate.userAddress
        {
            textFieldFromLocation.text = userAddress
           
        }
        textFieldToLocation.text = appDelegate.toAddress
        textFieldPassengers.text = appDelegate.numberOfPassenger
    }
    
    func stopTimerTest() {
        if appDelegate.expireTimer != nil {
            appDelegate.expireTimer?.invalidate()
            appDelegate.expireTimer = nil
        }
    }
    
    func checkStatus(){
        if(currentStatus != waitingViewModel?.status){
            //self.polyline = nil
            self.polyline.map = nil
            self.polylineOne = nil
            self.polylineOne?.map = nil
            didUserZoomed = false
            if let status = waitingViewModel?.status{
                if let bookingId = waitingViewModel?.bookingId{
                    FireBaseManager.defaultManager.removeObservers(bookingId: bookingId)
                }
                self.isObserveLocationfromFirebase = false
                self.currentStatus = status
                if self.currentStatus == K.STATUS.PENDING{
                    removeData()
                    stopTimerTest()
                    self.locationView.isHidden = true
                    self.imageScrollView.isHidden = true
                    showWaitingView()
                    if let lat = waitingViewModel?.pickup_lat{
                        if let long = waitingViewModel?.pickup_long{
                            if let dropLat = waitingViewModel?.drop_lat{
                                if let dropLong = waitingViewModel?.drop_long{
                                    overlayAdded = false
                                    googleMap.clear()
                                    addOverlayToMapView(source: CLLocation(latitude: Double(lat)!, longitude: Double(long)!), destination: CLLocation(latitude: Double(dropLat)!, longitude: Double(dropLong)!), isPending: true)
                                }
                            }
                        }
                    }
                   // getRefreshBookingData() -- modified
                }
                else if self.currentStatus == K.STATUS.ACCEPTED || self.currentStatus == K.STATUS.ONTRIP || self.currentStatus == K.STATUS.ONPICKUP {
                    if(!isObserveLocationfromFirebase){
                        if let bookingId = waitingViewModel?.bookingId{
                                overlayAdded = false
                                self.googleMap.clear()
                                self.isMarkersForPickupAdded = false
                            FireBaseManager.defaultManager.getDriverLatLong(bookingId: bookingId, completionBlock: { (dic) in
                                if(!(self.driverLat == nil && self.driverLong == nil)){
                                    if(self.driverOldLat != nil && self.driverOldLong != nil){
                                        if( self.driverOldLat != self.driverLat){
                                            print("DriverOldLat changed")
                                            self.driverOldLat = self.driverLat
                                        }
                                        if( self.driverOldLong != self.driverLong){
                                            print("DriverOldLong changed")
                                            self.driverOldLong = self.driverLong
                                        }
                                    }
                                    else{
                                         self.driverOldLat = self.driverLat
                                         self.driverOldLong = self.driverLong

                                    }
                                }
                                
                                if let lat = (dic.object(forKey: "latitude") as? NSNumber){
                                   self.driverLat = Double(lat)
                                }
                                if let long = (dic.object(forKey: "longitude") as? NSNumber){
                                    self.driverLong = Double(long)
                                }
                              //  self.driverLat = (dic.object(forKey: "latitude") as? NSNumber)
                              //  self.driverLong = (dic.object(forKey: "longitude") as? NSNumber)
                                
                                
                                self.isObserveLocationfromFirebase = true
                                if let lat = self.waitingViewModel?.pickup_lat{
                                    if let long = self.waitingViewModel?.pickup_long{
                                        if let dropLat = self.waitingViewModel?.drop_lat{
                                            if let dropLong = self.waitingViewModel?.drop_long{
                                                if self.currentStatus == K.STATUS.ACCEPTED || self.currentStatus == K.STATUS.ONPICKUP {
                                                    if let driverLat = self.driverLat{
                                                        if let driverLong = self.driverLong{
                                                             appDelegate.distanceRemaining = nil
                                                            self.updateOverlayToMapView(source: CLLocation(latitude: Double(driverLat), longitude: Double(driverLong)), destination: CLLocation(latitude: Double(lat)!, longitude: Double(long)!), isPending: true);
                                                        }
                                                    }
                                                }
                                                 else if self.currentStatus == K.STATUS.ONTRIP{
                                                    if let driverLat = self.driverLat{
                                                        if let driverLong = self.driverLong{
                                                             appDelegate.distanceRemaining = nil
                                                            self.updateOverlayToMapView(source: CLLocation(latitude: Double(driverLat), longitude: Double(driverLong)), destination: CLLocation(latitude: Double(dropLat)!, longitude: Double(dropLong)!), isPending: true);
                                                        }
                                                    }
                                                    
                                                }
                                                
                                            }
                                        }
                                    }
                                }
                            })
                        }
                    }
                    showOnrideView()
                    locationView.isHidden = true
                    imageScrollView.isHidden = true
                 //   getRefreshBookingData() -- modified
                }
                else if self.currentStatus == K.STATUS.CANCELED{  // canceled
                    // removeData()
                   // self.getCommonRefreshedData() -- modified
                    overlayAdded = false
                    locationView.isHidden = false
                    imageScrollView.isHidden = false
                    for view in self.view.subviews{
                        if (view is WaitingView){
                            view.removeFromSuperview()
                        }
                    }
                    for view in self.view.subviews{
                        if (view is OnRideView){
                            view.removeFromSuperview()
                        }
                    }
                    self.model = nil
                    self.markDriverCurrentLocation()
                    if let cancelPaymentStatus = waitingViewModel?.cancelledStatus{
                        if let cancellationFees = waitingViewModel?.cancellationfee{
                            if(cancelPaymentStatus == "0" && cancellationFees.replacingOccurrences(of: "$", with: "") > "0"){
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
                                vc.actionStatus = "1"
                                vc.model = waitingViewModel
                                self.navigationController?.present(vc, animated: true, completion: nil)
                            }
                            else if(cancelPaymentStatus == "1")
                            {
                              
                            }
                            else if(cancelPaymentStatus == "0" && cancellationFees.replacingOccurrences(of: "$", with: "") == "0")
                            {
                              //  Utility.showAlertWithMessage(vc: self, message: K.MESSAGE.k_cancelled_booking)
                            }
                        }
                    }
                }
                else if self.currentStatus == K.STATUS.EXPIRED{  // expired
                   // self.getCommonRefreshedData()-- modified
                    overlayAdded = false
                    locationView.isHidden = false
                    imageScrollView.isHidden = false
                    for view in self.view.subviews{
                        if (view is WaitingView){
                            view.removeFromSuperview()
                        }
                    }
                    for view in self.view.subviews{
                        if (view is OnRideView){
                            view.removeFromSuperview()
                        }
                    }
                    self.model = nil
                    self.markDriverCurrentLocation()
                    Utility.showAlertWithMessage(vc: self, message: K.MESSAGE.k_expired_booking)
                }
                else if self.currentStatus == K.STATUS.COMPLETED {  // Completed
                   // self.getCommonRefreshedData() -- modified
                    overlayAdded = false
                    locationView.isHidden = false
                    imageScrollView.isHidden = false
                    for view in self.view.subviews{
                        if (view is WaitingView){
                            view.removeFromSuperview()
                        }
                    }
                    for view in self.view.subviews{
                        if (view is OnRideView){
                            view.removeFromSuperview()
                        }
                    }
                    self.markDriverCurrentLocation()
                    self.model = nil
                    if let payment =  waitingViewModel?.paymentStatus{
                        if payment == "0" {
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
                            vc.actionStatus = "0"
                            vc.model = waitingViewModel
                            self.navigationController?.present(vc, animated: true, completion: nil)
                        }
                        else if let rating =  waitingViewModel?.userrating{
                            if rating == "0"{
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "RatingViewController") as! RatingViewController
                                vc.model = waitingViewModel
                                self.navigationController?.present(vc, animated: true, completion: nil)
                            }
                            
                        }
                    }
                    
                    
                }
                
            }
        }
        else
        {
            //  getRefreshBookingData()  -- modified
        }
      
    }
    
    func getDrivers()
    {
        DispatchQueue.main.async {
            self.menuButton.setImage(#imageLiteral(resourceName: "Back Arrow Copy"), for: .normal)
            self.driverCollectionView.isHidden = false
        }
       
        /*guard let _ = appDelegate.fromAddress else {
            return
        }*/
        if(appDelegate.fromAddress == nil){
            appDelegate.fromAddress = appDelegate.userAddress
        }
        guard let _ = appDelegate.toAddress else {
            return
        }
       
        if(appDelegate.fromLocation == nil){
            appDelegate.fromLocation = appDelegate.userLocation
        }
        /*guard let fromLocation = appDelegate.fromLocation else {
            return
        }*/
        guard let toLocation = appDelegate.toLocation else {
            return
        }
        guard let noOfPassenger = textFieldPassengers.text else {
            return
        }
        
        var toLat = ""
        var fromLat = ""
        var toLong = ""
        var fromLong = ""
        
        if let fromLocation = appDelegate.fromLocation{
         fromLat = String(describing: fromLocation.coordinate.latitude)
        fromLong = String(describing: fromLocation.coordinate.longitude)
        }
       
        toLat = String(describing: toLocation.coordinate.latitude)
        toLong = String(describing: toLocation.coordinate.longitude)
        
        //*************************
        print("fromLat ",fromLat)
        print("fromLong ",fromLong)
        print("toLat ",toLat)
        print("toLong ",toLong)
        
        let parameters : [String:String] = ["pickup_lat" :fromLat,"pickup_long":fromLong,"passenger_no":noOfPassenger,"drop_lat":toLat,"drop_long":toLong]
        self.driverViewModel = []
        self.driverCollectionView.reloadData()
        Utility.showIndicator()
        HomeAPIManager.getDrivers(parameters: parameters as [String : AnyObject], vc: self) { (success, response) in
             Utility.hideIndicator()
            if(response.count > 0){
                self.imageScrollView.isHidden = true
            }
            for model in response
            {
                appDelegate.getDrivers = false; self.driverViewModel.append(DriverListViewModel.init(model: model))
            }
            self.driverCollectionView.reloadData()
        }
        
        
    }
    
    func initAdSlide()
    {
        if self.imgArr.count > 0
        {
            for index in 0...imgArr.count - 1{
                let adImageView = UIImageView()
                let imageTap = UIButton()
                
                if let url = URL(string : imgArr[index] as! String)
                {
                    adImageView.setImageWith(url)
                }
                adImageView.frame = CGRect(x:self.xPostion,y: 0,width: self.imageScrollView.frame.size.width,height:self.imageScrollView.frame.size.height)
                imageTap.frame = adImageView.frame
                imageTap.addTarget(self, action: #selector(self.handleImageTap(_:)), for: .touchUpInside)
                imageTap.tag = index
                self.imageScrollView.addSubview(adImageView)
                self.imageScrollView.addSubview(imageTap)
                self.xPostion += self.view.frame.width
                self.imageScrollView.contentSize = CGSize(width: CGFloat(index + 1) * self.imageScrollView.frame.size.width, height: self.imageScrollView.frame.size.height)
            }
            let scrollWidth = self.imageScrollView.contentSize.width + self.imageScrollView.frame.size.width
            self.imageScrollView.contentSize = CGSize(width: scrollWidth, height: self.imageScrollView.frame.size.height)
            self.timer.invalidate()
            self.timer = Timer.scheduledTimer(timeInterval: TimeInterval(time), target: self, selector: #selector(self.onSliderShow), userInfo: nil, repeats: true)
        }
        
    }
    
    @objc func onSliderShow(timer: Timer){
        if let imgArr = model?.imageArr
        {
            self.timer = timer
            let pageWidth = self.imageScrollView.frame.width
            let maxWidth = pageWidth * CGFloat(imgArr.count)
            let contentOffset = self.imageScrollView.contentOffset.x
            var slideToX = contentOffset + pageWidth
            
            if contentOffset + pageWidth == maxWidth{
                slideToX = 0
            }
            self.imageScrollView.scrollRectToVisible(CGRect(x:slideToX,y: 0,width: pageWidth,height: self.imageScrollView.frame.height), animated: true)
        }
    }
    
    @objc func handleImageTap(_ sender: UIButton){
        Utility.openUrl(url: self.linkArr[sender.tag] as! String)
    }
    
    func setUpUIElements()
    {
        self.locationView.isHidden = true
        self.imageScrollView.isHidden = true
        Utility.addShadow(view: locationView)
        self.driverCollectionView.isPagingEnabled = false
        self.driverCollectionView.register(UINib(nibName: "DriverListCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "DriverCell")
    }
    
    
    
    func checkCommonStatus()
    {
        if let status = model?.status{
            if status == K.STATUS.PENDING{
                self.locationView.isHidden = true
                self.imageScrollView.isHidden = true
                removeData()
                self.menuButton.setImage(#imageLiteral(resourceName: "3bar navigation copy"), for: .normal)
               self.getBookedDriverData() // -- modified
            }
            else if status == K.STATUS.ACCEPTED || status == K.STATUS.ONTRIP || status == K.STATUS.ONPICKUP {
                self.menuButton.setImage(#imageLiteral(resourceName: "3bar navigation copy"), for: .normal)
                locationView.isHidden = true
                imageScrollView.isHidden = true
               self.getBookedDriverData()  //-- modified
            }
            else if status == K.STATUS.CANCELED{  // canceled
                
                locationView.isHidden = false
                imageScrollView.isHidden = false
                for view in self.view.subviews{
                    if (view is WaitingView){
                        view.removeFromSuperview()
                    }
                }
                for view in self.view.subviews{
                    if (view is OnRideView){
                        view.removeFromSuperview()
                    }
                }
                fetchUserLocation()
                 self.getBookedDriverData()  //-- modified
            }
            else if status == K.STATUS.EXPIRED{  // expired
                locationView.isHidden = false
                imageScrollView.isHidden = false
                for view in self.view.subviews{
                    if (view is WaitingView){
                        view.removeFromSuperview()
                    }
                }
                for view in self.view.subviews{
                    if (view is OnRideView){
                        view.removeFromSuperview()
                    }
                }
                fetchUserLocation()
            }
            else if status == K.STATUS.COMPLETED{  // Completed
                self.locationView.isHidden = false
                self.imageScrollView.isHidden = false
                if let rating = model?.rating{
                    if rating == "0"{
                        for view in self.view.subviews{
                            if (view is WaitingView){
                                view.removeFromSuperview()
                            }
                        }
                        for view in self.view.subviews{
                            if (view is OnRideView){
                                view.removeFromSuperview()
                            }
                        }
                        getBookedDriverData()  //-- modified
                    }
                }
                fetchUserLocation()
            }
        }
    }
    
    /*func getRefreshBookingData(){
        guard let bookingId = waitingViewModel?.bookingId else {
            return
        }
        
        let parameters : [String:String] = ["booking_id":bookingId]
        HomeAPIManager.getRefreshedBookedDriverdata(parameters: parameters as [String : AnyObject], vc: self) { (success, response) in
            if success == true{
                self.waitingViewModel = WaitingViewModel.init(model: response)
                self.checkStatus()
            }
        }
    }*/
    
    func getBookedDriverData(){
        guard let bookingId = model?.booking_id else {
            return
        }
        let parameters : [String:String] = ["booking_id":bookingId]
        HomeAPIManager.getBookedDriverdata(parameters: parameters as [String : AnyObject], vc: self) { (success, response) in
            if success == true{
                self.waitingViewModel = WaitingViewModel.init(model: response)
                self.checkStatus()
            }
        }
    }
    
    func getCommonRefreshedData()
    {
        let parameters : [String:AnyObject] = [:]
        HomeAPIManager.getHomeCommonAPI(parameters: parameters, vc: self) { (success, response) in
            if success == true
            {
                self.model = HomeViewModel.init(model: response)
                self.setNotificationCount()
                self.locationView.isHidden = false
                self.imageScrollView.isHidden = false
                if let time  = self.model?.timeInterval
                {
                    self.time = time
                }
                if let imgArr = self.model?.imageArr
                {
                    if imgArr.count > 0
                    {
                        self.imgArr = imgArr
                    }
                }
                if let linkArr = self.model?.linkArr
                {
                    if linkArr.count > 0
                    {
                        self.linkArr = linkArr
                    }
                }
                self.initAdSlide()
            }
            
        }
    }
    
    func getCommondata()
    {
        driverLat = nil
        driverLong = nil
        driverOldLat = nil
        driverOldLong = nil
        currentStatus = "100"
        let parameters : [String:AnyObject] = [:]
        HomeAPIManager.getHomeCommonAPI(parameters: parameters, vc: self) { (success, response) in
            if success == true
            {
                self.model = HomeViewModel.init(model: response)
                self.setNotificationCount()
                self.locationView.isHidden = false
                self.imageScrollView.isHidden = false
                self.checkCommonStatus()
                if let time  = self.model?.timeInterval
                {
                    self.time = time
                }
                if let imgArr = self.model?.imageArr
                {
                    if imgArr.count > 0
                    {
                        self.imgArr = imgArr
                    }
                }
                if let linkArr = self.model?.linkArr
                {
                    if linkArr.count > 0
                    {
                        self.linkArr = linkArr
                    }
                }
                self.initAdSlide()
            }
            
        }
    }
    
    func addOverlayToMapView(source :CLLocation ,destination:CLLocation,isPending:Bool){
        if (overlayAdded){
            return
        }
        if let currentLocation = appDelegate.userLocation
        {
            googleMap.clear()
            markOriginDest(originLat: source.coordinate.latitude, originLong: source.coordinate.longitude, destinationLat: destination.coordinate.latitude, destinationLong: destination.coordinate.longitude, isPending: isPending)
            let mode = "driving"
            let directionURL = "https://maps.googleapis.com/maps/api/directions/json?origin=\(source.coordinate.latitude),\(source.coordinate.longitude)&destination=\(destination.coordinate.latitude),\(destination.coordinate.longitude)&mode=\(mode)&key=\(K.KEY.GoogleDirectionKey)"
            Alamofire.request(directionURL).responseJSON { response in
                print(response.request as Any)  // original URL request
                print(response.response as Any) // HTTP URL response
                print(response.data as Any)     // server data
                print(response.result as Any)   // result of response serialization
                
                do{
                    let json = try JSON(data: response.data!)
                    let routes = json["routes"].arrayValue
                    
                    // print route using Polyline
                    for route in routes
                    {
                        let routeOverviewPolyline = route["overview_polyline"].dictionary
                        let points = routeOverviewPolyline?["points"]?.stringValue
                        let path = GMSPath.init(fromEncodedPath: points!)
                        let polyline = GMSPolyline.init(path: path)
                        polyline.strokeWidth = 4
                        polyline.strokeColor = UIColor.black
                        polyline.map = self.googleMap
                        self.overlayAdded = true
                    }
                    
                }catch let error {
                    print("Error while parsign JSON on addOverlay \(error.localizedDescription)")
                }
            }
        }
    }
    
    func updateOverlayToMapView(source :CLLocation ,destination:CLLocation,isPending:Bool){
        if !isMarkersForPickupAdded{
            markOriginDestinationMarkers(originLat: source.coordinate.latitude, originLong: source.coordinate.longitude, destinationLat: destination.coordinate.latitude, destinationLong: destination.coordinate.longitude)
        }
        let mode = "driving"
        let directionURL = "https://maps.googleapis.com/maps/api/directions/json?origin=\(source.coordinate.latitude),\(source.coordinate.longitude)&destination=\(destination.coordinate.latitude),\(destination.coordinate.longitude)&mode=\(mode)&key=\(K.KEY.GoogleDirectionKey)"
        Alamofire.request(directionURL).responseJSON { response in
            do{
                let json = try JSON(data: response.data!)
                let routes = json["routes"].arrayValue
                for route in routes
                {
                    let legs = route["legs"].arrayValue
                    if legs.count > 0
                    {
                        let legsDic = legs[0].dictionaryValue
                        let distanceDic = (legsDic["distance"]?.dictionaryValue) ?? [:]
                        let distanceValue = distanceDic["value"]?.rawValue
                        let distanceInMiles = distanceValue as! Double * 0.000621371
                        appDelegate.distanceRemaining = Int(distanceInMiles)
                        print("Distance" , distanceValue ?? "")
                        let durationDic = (legsDic["duration"]?.dictionaryValue) ?? [:]
                        let durationValue = durationDic["value"]?.rawValue
                       
                        var duration = durationValue as! Int
                        if(duration < 60){
                            self.timeFormat = "sec"
                        }
                        if((durationValue as! Int) >= 60){
                            duration = Int(ceil(Double(duration / 60)))
                            self.timeFormat = "mins"
                        }
                        if((durationValue as! Int)/60 >= 60){
                            duration = Int(ceil(Double(duration / 60)))
                            self.timeFormat = "hrs"
                        }
                      //   print("Duration", duration ?? "")
                        self.timeRemaining = String(duration)
                        self.googleMap.selectedMarker = self.carMarker
                        if let marker = self.carMarker{
                            if(self.driverLat != nil){
                                if(self.driverLong != nil){
                                     self.moveCar(driverMarker: marker)
                                   // marker.position = CLLocationCoordinate2D(latitude: self.driverLat!, longitude: self.driverLong!)
                                }
                            }
                        }
                        if(self.currentStatus == K.STATUS.ACCEPTED){
                         NotificationCenter.default.post(name: Notification.Name("updateDistance"), object: nil)
                        }
                        }
                    let routeOverviewPolyline = route["overview_polyline"].dictionary
                    let points = routeOverviewPolyline?["points"]?.stringValue
                    self.path = GMSPath.init(fromEncodedPath: points!)!
                        self.i = nil
                        self.polyline.path = self.path
                        self.polyline.strokeColor = UIColor.black
                        self.polyline.strokeWidth = 3
                        self.polyline.map = self.googleMap
                        self.overlayAdded = true
                    // to animate polyline
                    //self.polylineTimer = Timer.scheduledTimer(timeInterval: 0.001, target: self, selector: #selector(self.animatePolylinePath), userInfo: nil, repeats: true)
                   /* self.polyline = GMSPolyline.init(path: path)
                    self.polyline?.strokeWidth = 4
                    self.polyline?.strokeColor = UIColor.black
                    self.polyline?.map = self.googleMap*/
                    
                }
                
            }catch let error {
                print("Error while parsign JSON on addOverlay \(error.localizedDescription)")
            }
        }
    }
    
    @objc func animatePolylinePath() {
        if ( i != nil) {
            if (path.count() > i!){
                animationPath?.add(path.coordinate(at: UInt(i!)))
                self.polyline.path = animationPath
                self.polyline.strokeColor = UIColor.black
                self.polyline.strokeWidth = 3
                self.polyline.map = self.googleMap
                i! = i! + 1
            }
            else
            {
            self.polylineTimer.invalidate()
            }
        }
        else {
            i = 0
            animationPath = GMSMutablePath()
           // self.polyline.map = nil
        }
    }
    
    func markOriginDest(originLat:Double,originLong:Double,destinationLat:Double,destinationLong:Double,isPending:Bool)
    {
        googleMap.delegate = self
        let originMarker = GMSMarker()
        let originmarkerImage = UIImage(named: "circle")!.withRenderingMode(.alwaysOriginal)
        let originmarkerView = UIImageView(image: originmarkerImage)
        originMarker.iconView = originmarkerView
        originMarker.position = CLLocationCoordinate2D(latitude: originLat, longitude: originLong)
        originMarker.map = googleMap
        
        let destinationMarker = GMSMarker()
        let destinationmarkerImage = UIImage(named: "to")!.withRenderingMode(.alwaysOriginal)
        let destinationmarkerView = UIImageView(image: destinationmarkerImage)
        destinationMarker.iconView = destinationmarkerView
        destinationMarker.position = CLLocationCoordinate2D(latitude: destinationLat, longitude: destinationLong)
        destinationMarker.map = googleMap

        if(!didUserZoomed){
            var bounds = GMSCoordinateBounds()
            bounds = bounds.includingCoordinate(destinationMarker.position)
            bounds = bounds.includingCoordinate(originMarker.position)
            googleMap.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 10))
            //let camera =  GMSCameraPosition.camera(withLatitude: CLLocationDegrees(originLat), longitude: CLLocationDegrees(originLat), zoom: 10.0)
           // googleMap.camera = camera
        }
       
    }
    
    func degreesToRadians(degrees: Double) -> Double { return degrees * .pi / 180.0 }
    func radiansToDegrees(radians: Double) -> Double { return radians * 180.0 / .pi }
    
    func getBearingBetweenTwoPoints1(point1 : CLLocation, point2 : CLLocation) -> Double {
        
        let lat1 = degreesToRadians(degrees: point1.coordinate.latitude)
        let lon1 = degreesToRadians(degrees: point1.coordinate.longitude)
        
        let lat2 = degreesToRadians(degrees: point2.coordinate.latitude)
        let lon2 = degreesToRadians(degrees: point2.coordinate.longitude)
        
        let dLon = lon2 - lon1
        
        let y = sin(dLon) * cos(lat2)
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon)
        let radiansBearing = atan2(y, x)
        
        return radiansToDegrees(radians: radiansBearing)
    }
    
    //Zoom map with cordinate
    func updateMapFrame(newLocation: CLLocation, zoom: Float) {
        let camera = GMSCameraPosition.camera(withTarget: newLocation.coordinate, zoom: zoom)
        self.googleMap.animate(to: camera)
    }
    
    func moveCar(driverMarker:GMSMarker){
        guard let driverOldLongitude = driverOldLong else{
            return
        }
        guard let driverOldLatitude = driverOldLat else{
            return
        }
        guard let driverNewLongitude = driverLong else{
            return
        }
        guard let driverNewLatitude = driverLat else{
            return
        }
        driverMarker.icon = #imageLiteral(resourceName: "limousine")
        let oldCoodinate = CLLocationCoordinate2D(latitude: driverOldLatitude, longitude: driverOldLongitude)
        let newCoodinate =  CLLocationCoordinate2D(latitude: driverNewLatitude, longitude: driverNewLongitude)
        driverMarker.position = CLLocationCoordinate2DMake(driverNewLatitude, driverNewLongitude)
        let angle = angleFromCoordinate(firstCordinate: oldCoodinate, toCoordinate: newCoodinate)
        driverMarker.rotation = CLLocationDegrees(angle * (180.0 / .pi))
        driverMarker.map = googleMap
    }
    
    func angleFromCoordinate(firstCordinate:CLLocationCoordinate2D, toCoordinate:CLLocationCoordinate2D)->Float{
        let deltaLongitude = Float(toCoordinate.longitude - firstCordinate.longitude)
        let deltaLatitude = Float(toCoordinate.latitude - firstCordinate.latitude)
        let angle: Float = (.pi * 0.5) - atan(deltaLatitude / deltaLongitude)
        if deltaLongitude > 0 {
            return angle
        } else if deltaLongitude < 0 {
            return angle + .pi
        } else if deltaLatitude < 0 {
            return .pi
        }
        return 0.0
    }
    
    func markOriginDestinationMarkers(originLat:Double,originLong:Double,destinationLat:Double,destinationLong:Double)
        {
        googleMap.delegate = self
        let originMarker = GMSMarker()
        let originmarkerImage = UIImage(named: "circle")!.withRenderingMode(.alwaysOriginal)
        let originmarkerView = UIImageView(image: originmarkerImage)
        originMarker.iconView = originmarkerView
        originMarker.position = CLLocationCoordinate2D(latitude: destinationLat, longitude: destinationLong)
        originMarker.map = googleMap
        
         self.carMarker = GMSMarker()
         let carmarkerImage = UIImage(named: "limousine")!.withRenderingMode(.alwaysOriginal)
         let carmarkerView = UIImageView(image: carmarkerImage)
            self.carMarker?.iconView = carmarkerView
         self.carMarker?.position = CLLocationCoordinate2D(latitude: originLat, longitude: originLong)
           self.carMarker?.map = googleMap
           googleMap.selectedMarker = self.carMarker
          //  self.moveCar(driverMarker: self.carMarker!)
            if(!didUserZoomed){
                var bounds = GMSCoordinateBounds()
                bounds = bounds.includingCoordinate(originMarker.position)
                bounds = bounds.includingCoordinate((self.carMarker?.position)!)
                googleMap.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 10))
            }
          
             self.isMarkersForPickupAdded = true
    }
    
    func setNotificationCount()
    {
        if let model = self.model
        {
            if let count = model.notificationCount
            {
                 notificationButoon.badgeString = String(count)
                 notificationButoon.badgeEdgeInsets = UIEdgeInsetsMake(15, -20, 0, 0)
                if(count == 0){
                    notificationButoon.badgeBackgroundColor = UIColor.clear
                    notificationButoon.badgeTextColor = UIColor.clear
                }
                else
                {
                    notificationButoon.badgeBackgroundColor = UIColor.red
                    notificationButoon.badgeTextColor = UIColor.white
                }
            }
            
            
        }
    }
    
    @IBAction func notificationButtonTapped(_ sender: MIBadgeButton) {
        Utility.pushViewController(fromVC: self, toVC: NotificationsViewController(), identifier: "NotificationsViewController")
    }
    
    @IBAction func fromButtonTapped(_ sender: UIButton) {
        navigateToSearchScreen(activeTextfield:1)
        
    }
    
    @IBAction func toButtonTapped(_ sender: UIButton) {
        navigateToSearchScreen(activeTextfield:2)
        
    }
    
    @IBAction func noOfPassengerButtonTapped(_ sender: UIButton) {
        navigateToSearchScreen(activeTextfield:3)
    }
    
    func navigateToSearchScreen(activeTextfield:Int)
    {
        if !(Reachability.isConnectedToNetwork()){
            Utility.showAlertWithMessage(vc: self, message: K.MESSAGE.k_network_error)
            return
        }
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        vc.activeText = activeTextfield
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func showWaitingView()
    {
        locationView.isHidden = true
        imageScrollView.isHidden = true
        driverCollectionView.isHidden = true
        
        for view in self.view.subviews{
            if (view is WaitingView){
                view.removeFromSuperview()
            }
        }
        for view in self.view.subviews{
            if (view is OnRideView){
                view.removeFromSuperview()
            }
        }
        
        if let view = self.view.viewWithTag(1000)
        {
            view.removeFromSuperview()
        }
        waitingView = WaitingView.instanceFromNib()
        waitingView?.delegate = self
        if let model = waitingViewModel{
            waitingView?.setData(model: model)
        }
        Utility.addShadow(view: waitingView!)
        if(Utility.isIPhone5()){
            waitingView?.frame = CGRect(x: 20.0, y: self.view.frame.size.height - self.view.frame.size.height/2.0, width: self.view.frame.size.width - 40, height: self.view.frame.size.height/2.1)
        }
        else{
            waitingView?.frame = CGRect(x: 20.0, y: self.view.frame.size.height - self.view.frame.size.height/2.1, width: self.view.frame.size.width - 40, height: self.view.frame.size.height/2.2)
        }
        waitingView?.tag = 1000
        self.view.addSubview(waitingView!)
    }
    
    func showOnrideView()
    {
        locationView.isHidden = true
        imageScrollView.isHidden = true
        driverCollectionView.isHidden = true
        
        for view in self.view.subviews{
            if (view is WaitingView){
                view.removeFromSuperview()
            }
        }
        for view in self.view.subviews{
            if (view is OnRideView){
                view.removeFromSuperview()
            }
        }
        
        if let view = self.view.viewWithTag(1111)
        {
            view.removeFromSuperview()
        }
        onRideView = OnRideView.instanceFromNib()
        onRideView?.delegate = self
        if let waitingModel = waitingViewModel{
            onRideView?.setData(model: waitingModel,status:waitingViewModel?.status)
        }
        Utility.addShadow(view: onRideView!)
        if let bookingStatus =  waitingViewModel?.status{
            if bookingStatus == "2" || bookingStatus == "6"{
                  if(Utility.isIPhone5()){
                    onRideView?.frame = CGRect(x: 20.0, y: self.view.frame.size.height - self.view.frame.size.height/2.4, width: self.view.frame.size.width - 40, height: self.view.frame.size.height/2.6)
                }
                  else{
                    onRideView?.frame = CGRect(x: 20.0, y: self.view.frame.size.height - self.view.frame.size.height/2.5, width: self.view.frame.size.width - 40, height: self.view.frame.size.height/2.7)
                }
            }
            else if bookingStatus == "4"{
              //  onRideView?.frame = CGRect(x: 20.0, y: self.view.frame.size.height - self.view.frame.size.height/2.1, width: self.view.frame.size.width - 40, height: self.view.frame.size.height/2.2)
                 if(Utility.isIPhone5()){
                    onRideView?.frame = CGRect(x: 20.0, y: self.view.frame.size.height - self.view.frame.size.height/2.4, width: self.view.frame.size.width - 40, height: self.view.frame.size.height/2.6)
                }
                 else{
                    onRideView?.frame = CGRect(x: 20.0, y: self.view.frame.size.height - self.view.frame.size.height/2.5, width: self.view.frame.size.width - 40, height: self.view.frame.size.height/2.7)
                }
            }
        }
        
        onRideView?.tag = 1111
        self.view.addSubview(onRideView!)
    }
   
    
    
    @objc func expireRide(notification: Notification){
        if let bookingId = waitingViewModel?.bookingId {
            if let status = waitingViewModel?.status{
                if status == K.STATUS.PENDING{
                    let parameters : [String:String] = ["booking_id":bookingId,"action_type":"2"]
                    HomeAPIManager.cancelBooking(parameters: parameters as [String : AnyObject], vc: self) { (success, response) in
                        self.locationView.isHidden = false
                        self.imageScrollView.isHidden = false
                       
                        self.model = nil
                        self.waitingViewModel = WaitingViewModel.init(model: response)
                        for view in self.view.subviews{
                            if (view is WaitingView){
                                view.removeFromSuperview()
                            }
                        }
                        self.markDriverCurrentLocation()
                        Utility.showAlertWithMessage(vc: self, message:K.MESSAGE.k_expired_booking )
                    }
                }
            }
            
        }
       
    }
    
}

extension HomeViewController :MFMessageComposeViewControllerDelegate{
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
    }
}

import Foundation
import MessageUI




extension HomeViewController:OnRideViewDelegate{
    func callButtontapped() {
        guard let mobile = self.waitingViewModel?.phone else {
            return
        }
        if let phoneCallURL:NSURL = NSURL(string: "tel://\(mobile)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL as URL)) {
                if #available(iOS 10.0, *) {
                    application.open(phoneCallURL as URL, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                }
            }
        }
    }
    
    func shareButtontapped() {
        //Hi, I am using the LUX application to travel from “Origin” to “Destination”. I am riding with “Driver Name” in a “Vehicle Name” whose number is ”Driver Number”.

        if (MFMessageComposeViewController.canSendText()) {
            // Obtain a configured MFMessageComposeViewController
            let messageComposeVC = MFMessageComposeViewController()
            messageComposeVC.messageComposeDelegate = self
            messageComposeVC.body = "Hi , I am using LUX Application to travel from " + (waitingViewModel?.pickup_area)! +  " to " + (waitingViewModel?.drop_area)! + ".I am riding with " + (waitingViewModel?.driverName)! +  " in a " + (waitingViewModel?.carName)! + " whose number is : " +  (waitingViewModel?.phone)! //+  " and Car Number is: " +  (waitingViewModel?.carNumb)!
            
            present(messageComposeVC, animated: true, completion: nil)
        } else {
            // Let the user know if his/her device isn't able to send text messages
            let errorAlert = UIAlertView(title: "Cannot Send Text Message", message: "Your device is not able to send text messages.", delegate: self, cancelButtonTitle: "OK")
            errorAlert.show()
        }
    }
    
    func payNowButtontapped() {
        
    }
    
    func cancelRideButtontapped(){
        var msg = ""
        if let status = waitingViewModel?.status{
            if status == K.STATUS.ONPICKUP{
                if let amount = waitingViewModel?.cancellationfee{
                    msg = K.MESSAGE.k_cancellationPayment + amount
                }
            }
           else{
                msg = K.MESSAGE.k_cancelRide
            }
            
        }
        let alertController : UIAlertController = UIAlertController(title: K.MESSAGE.k_app_name, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        let noAction = UIAlertAction(title: "No", style: UIAlertActionStyle.default) {
            UIAlertAction in
            
        }
        let yesAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) {
            UIAlertAction in
            guard let bookingId = self.waitingViewModel?.bookingId else {
                return
            }
            
            if let status = self.waitingViewModel?.status{
                if status == K.STATUS.ONPICKUP{
                    let action_type = "3"
                    
                    let parameters : [String:String] = ["booking_id":bookingId,"action_type":action_type]
                    HomeAPIManager.cancelBooking(parameters: parameters as [String : AnyObject], vc: self) { (success, response) in
                        if let updatedStatus = response.status{
                            if (updatedStatus == K.STATUS.CANCELED){
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
                                vc.actionStatus = "1"
                                vc.model = self.waitingViewModel
                                self.navigationController?.present(vc, animated: true, completion: nil)
                            }
                            else{
                                self.getCommondata()
                            }
                        }
                    }
                }
                else{
                    
                    var actionType = "0"
                    
                    if let status = self.waitingViewModel?.status{
                        if status == "2"
                        {
                            actionType = "1"
                        }
                        else if status == "2"
                        {
                            actionType = "1"
                        }
                        else if status == "6"
                        {
                            actionType = "2"
                        }
                    }
                    
                    let parameters : [String:String] = ["booking_id":bookingId,"action_type":actionType]
                    HomeAPIManager.cancelBooking(parameters: parameters as [String : AnyObject], vc: self) { (success, response) in
                        if let updatedStatus = response.status{
                            if (updatedStatus == K.STATUS.CANCELED){
                        self.locationView.isHidden = false
                        self.imageScrollView.isHidden = false
                        self.model = nil
                        self.waitingViewModel = WaitingViewModel.init(model: response)
                        for view in self.view.subviews{
                            if (view is OnRideView){
                                view.removeFromSuperview()
                            }
                        }
                        self.markDriverCurrentLocation()
                        Utility.showAlertWithMessage(vc: self, message:K.MESSAGE.k_cancelled_ride )
                            }
                        }
                        else{
                            self.getCommondata()

                        }
                    }
                    
                    
                   
                }
                
            }
          
            
        }
        alertController.addAction(yesAction)
        alertController.addAction(noAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    
    
}

extension HomeViewController:WaitingViewDelegate{
    func cancelButtontapped() {
        
        let alertController : UIAlertController = UIAlertController(title: K.MESSAGE.k_app_name, message: K.MESSAGE.k_cancel_booking, preferredStyle: UIAlertControllerStyle.alert)
        let noAction = UIAlertAction(title: "No", style: UIAlertActionStyle.default) {
            UIAlertAction in
        }
        let yesAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) {
            UIAlertAction in
            guard let bookingId = self.waitingViewModel?.bookingId else {
                return
            }
            
            let parameters : [String:String] = ["booking_id":bookingId,"action_type":"0"]
            HomeAPIManager.cancelBooking(parameters: parameters as [String : AnyObject], vc: self) { (success, response) in
                self.stopTimerTest()
                self.waitingViewModel = WaitingViewModel.init(model: response)
                self.model = nil
                self.locationView.isHidden = false
                self.imageScrollView.isHidden = false
                for view in self.view.subviews{
                    if (view is WaitingView){
                        view.removeFromSuperview()
                    }
                }
                self.markDriverCurrentLocation()
                Utility.showAlertWithMessage(vc: self, message: K.MESSAGE.k_cancelled_booking)
            }
            
        }
        alertController.addAction(yesAction)
        alertController.addAction(noAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
}

extension HomeViewController : CLLocationManagerDelegate{
    
    func fetchUserLocation()
    {
        if CLLocationManager.locationServicesEnabled() && CLLocationManager.authorizationStatus() != CLAuthorizationStatus.denied  && CLLocationManager.authorizationStatus() != CLAuthorizationStatus.restricted{
            LocationHandler.sharedInstance.updateLocation { (location, error, true) in
                
                if error != nil{
                    if error?.domain == "Restricted"
                    {
                        Utility.showAlertWithMessage(vc: self, message:K.MESSAGE.locationError )
                    }
                }
                else
                {
                    appDelegate.userLocation = location
                    self.markDriverCurrentLocation()
                    if (location?.coordinate) != nil
                    {
                        LocationHandler.sharedInstance.reverseGeoCoding(location: location!, completion: { (address) in
                          //  print(address)
                            appDelegate.userAddress = address
                            self.setUpData()
                        })
                    }
                }
            }
        }
        else{
            
            Utility.showAlertWithMessage(vc: self, message:K.MESSAGE.locationError )
        }
        
    }
}


extension HomeViewController:GMSMapViewDelegate{
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        didUserZoomed = true
    }
    
    func markDriverCurrentLocation()
    {
        if let status = waitingViewModel?.status{
            if(status == K.STATUS.ONPICKUP || status == K.STATUS.ACCEPTED || status == K.STATUS.ONTRIP || status == K.STATUS.PENDING)
            {
                return
            }
        }
        if let status = model?.status{
            if(status == K.STATUS.ONPICKUP || status == K.STATUS.ACCEPTED || status == K.STATUS.ONTRIP || status == K.STATUS.PENDING)
            {
                return
            }
        }
        
        guard let userLocation = appDelegate.userLocation else {
            return
        }
        var userLat = 0.0
        var userLong = 0.0
        userLat =  userLocation.coordinate.latitude
        userLong = userLocation.coordinate.longitude
        googleMap.delegate = self
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        let markerView = UIImageView(image: #imageLiteral(resourceName: "circle"))
        marker.iconView = markerView
        marker.position = CLLocationCoordinate2D(latitude: (CLLocationDegrees(userLat)), longitude: CLLocationDegrees(userLong))
        if(!didUserZoomed){
              let camera =  GMSCameraPosition.camera(withLatitude: CLLocationDegrees(userLat), longitude: CLLocationDegrees(userLong), zoom: 15.0)
            googleMap.camera = camera
        }
        googleMap.clear()
        marker.map = googleMap
        //googleMap.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 10))
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        if(marker == carMarker){
            let customInfoWindow = Bundle.main.loadNibNamed("MarkerInfoWindow", owner: self, options: nil)![0] as! MarkerInfoWindow
                customInfoWindow.layoutIfNeeded()
                customInfoWindow.timeLabel.text = self.timeRemaining
                customInfoWindow.minsLabel.text = self.timeFormat
            return customInfoWindow
        }
        else {
            return nil
    }
 
}
}

extension HomeViewController:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.driverViewModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : DriverListCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DriverCell", for: indexPath) as! DriverListCollectionViewCell
        cell.setdata(model: driverViewModel[indexPath.row])
        cell.bookButton.addTarget(self, action: #selector(self.bookButtonTapped(sender:)), for: .touchUpInside)
        cell.bookButton.tag = indexPath.row
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width / 1.5, height :self.driverCollectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0.0
    }
    
    @objc func bookButtonTapped(sender:UIButton)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RideDetailViewController") as! RideDetailViewController
        vc.driverViewModel = driverViewModel[sender.tag]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func getHeadingForDirection(fromCoordinate fromLoc: CLLocationCoordinate2D, toCoordinate toLoc: CLLocationCoordinate2D) -> Float {
        
        let fLat: Float = Float((fromLoc.latitude).degreesToRadians)
        let fLng: Float = Float((fromLoc.longitude).degreesToRadians)
        let tLat: Float = Float((toLoc.latitude).degreesToRadians)
        let tLng: Float = Float((toLoc.longitude).degreesToRadians)
        let degree: Float = (atan2(sin(tLng - fLng) * cos(tLat), cos(fLat) * sin(tLat) - sin(fLat) * cos(tLat) * cos(tLng - fLng))).radiansToDegrees
        if degree >= 0 {
            return degree
        }
        else {
            return 360 + degree
        }
    }

}


extension Int {
    var degreesToRadians: Double { return Double(self) * .pi / 180 }
}
extension FloatingPoint {
    var degreesToRadians: Self { return self * .pi / 180 }
    var radiansToDegrees: Self { return self * 180 / .pi }
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
