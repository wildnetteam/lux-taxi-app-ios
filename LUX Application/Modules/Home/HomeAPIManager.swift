//
//  HomeAPIManager.swift
//  LUX Application
//
//  Created by Yogita on 05/02/18.
//  Copyright © 2018 WildnetTechnologies. All rights reserved.
//

import Foundation
class HomeAPIManager{
    typealias response = (_ isSuccess: Bool,_ response : HomeModel) -> Void
    typealias driverResponse = (_ isSuccess: Bool,_ response : [DriverListModel]) -> Void
    typealias bookingData = (_ isSuccess: Bool,_ response : WaitingModel) -> Void

    class func getHomeCommonAPI(parameters : [String:AnyObject],vc:UIViewController,completion:@escaping response){
        if(!(Reachability.isConnectedToNetwork())){
            Utility.showAlertWithMessage(vc: vc, message: K.MESSAGE.k_network_error)
            return
        }
        Utility.showIndicator()
        WebServiceHelper.sharedInstance.callGetApiWithHeader(input: nil, completeUrl: K.URL.BaseUrl + K.URL.HomeCommon){(success, data) in
            Utility.hideIndicator()
            if success == true
            {
                guard let response = data else {
                    return
                }
                guard let dic = response as? NSDictionary else {
                    return
                }
                guard let status = dic["status"] else {
                    return
                }
                if ((status as! String) == "success")
                {
                    guard let notificationCount = dic["notificationcount"] else {
                        return
                    }
                    guard let imagePath = dic["imagepath"] else {
                        return
                    }
                    guard let res = dic["advertisement"] else {
                        return
                    }
                    guard let advertisement = res as? NSArray else {
                        return
                    }
                    guard let timeInterval = dic["timetnterval"] else {
                        return
                    }
                    guard let bookingdata = dic["bookingdata"] else {
                        return
                    }
                    let model = HomeModel.parsedata(advertiseArr: advertisement, notificationCount: notificationCount as! Int, imagepath: imagePath as! String, timeinterval: timeInterval as! Int,data: dic )
                    completion(true, model)
                }
                else
                {
                    guard let msg = dic["message"] else {
                        return
                    }
                    guard let code = dic["code"] else {
                        return
                    }
                    
                    if UserDefaultManager.getIsLogin()!{
                        if (code as? Int) == 420{
                            Utility.logOut(vc: vc, message: msg as! String)
                            return
                        }
                    }
                
                    Utility.snackbar(message: msg as! String,vc:vc)
                    let model = HomeModel.parsedata(advertiseArr: [], notificationCount:0, imagepath: "", timeinterval: 0,data: [:])
                    completion(false,model)
                }
                }
            else
            {
               let model = HomeModel.parsedata(advertiseArr: [], notificationCount:0, imagepath: "", timeinterval: 0,data: [:])
                completion(false,model)
            }
            
        }
    }
    
    
    
    class func getBookedDriverdata(parameters : [String:AnyObject],vc:UIViewController,completion:@escaping bookingData){
        if(!(Reachability.isConnectedToNetwork())){
            Utility.showAlertWithMessage(vc: vc, message: K.MESSAGE.k_network_error)
            return
        }
        Utility.showIndicator()
        WebServiceHelper.sharedInstance.postFormDataWithHeader(input: parameters as AnyObject, image: nil, completeUrl: K.URL.BaseUrl + K.URL.BookedDriverData) { (success, data) in
            Utility.hideIndicator()
            if success == true
            {
                guard let response = data else {
                    return
                }
                guard let dic = response as? NSDictionary else {
                    return
                }
                guard let status = dic["status"] else {
                    return
                }
                if ((status as! String) == "success")
                {
                    let model = WaitingModel.initModel(res: dic as! [String : Any])
                        completion(true, model)
                }
                else
                {
                    guard let msg = dic["message"] else {
                        return
                    }
                    guard let code = dic["code"] else {
                        return
                    }
                    if UserDefaultManager.getIsLogin()!{
                        if (code as? Int) == 420{
                            Utility.logOut(vc: vc, message: msg as! String)
                            return
                        }
                    }
                   
                    Utility.snackbar(message: msg as! String,vc:vc)
                    let model = WaitingModel.initModel(res: [:])
                    completion(true, model)
                }
            }
            else
            {
                let model = WaitingModel.initModel(res: [:])
                completion(false,model)
            }
        }
        }
    
    class func mailInvoiceAPI(parameters : [String:AnyObject],vc:UIViewController,completion:@escaping status){
        if(!(Reachability.isConnectedToNetwork())){
            Utility.showAlertWithMessage(vc: vc, message: K.MESSAGE.k_network_error)
            return
        }
        Utility.showIndicator()
        WebServiceHelper.sharedInstance.postFormDataWithHeader(input: parameters as AnyObject, image: nil, completeUrl: K.URL.BaseUrl + K.URL.MailInvoice) { (success, data) in
            Utility.hideIndicator()
            if success == true
            {
                guard let response = data else {
                    return
                }
                guard let dic = response as? NSDictionary else {
                    return
                }
                guard let status = dic["status"] else {
                    return
                }
                if ((status as! String) == "success")
                {
                   completion(true)
                }
                else
                {
                    guard let msg = dic["message"] else {
                        return
                    }
                    guard let code = dic["code"] else {
                        return
                    }
                    
                    if (code as? Int) == 420{
                        Utility.logOut(vc: vc, message: msg as! String)
                        return
                    }
                    Utility.snackbar(message: msg as! String,vc:vc)
                   
                    completion(false)
                }
            }
            else
            {
                completion(false)
            }
        }
    }
    
    
    class func updateUserLocation(parameters : [String:AnyObject],vc:UIViewController,completion:@escaping bookingData){
        if(!(Reachability.isConnectedToNetwork())){
            Utility.showAlertWithMessage(vc: vc, message: K.MESSAGE.k_network_error)
            return
        }
        WebServiceHelper.sharedInstance.postFormDataWithHeader(input: parameters as AnyObject, image: nil, completeUrl: K.URL.BaseUrl + K.URL.RiderLocation) { (success, data) in
            Utility.hideIndicator()
            if success == true
            {
                guard let response = data else {
                    return
                }
                guard let dic = response as? NSDictionary else {
                    return
                }
                guard let status = dic["status"] else {
                    return
                }
                if ((status as! String) == "success")
                {
                    let model = WaitingModel.initModel(res: dic as! [String : Any])
                    completion(true, model)
                }
                else
                {
                    guard let msg = dic["message"] else {
                        return
                    }
                    guard let code = dic["code"] else {
                        return
                    }
                    
                    if UserDefaultManager.getIsLogin()!{
                        if (code as? Int) == 420{
                            Utility.logOut(vc: vc, message: msg as! String)
                            return
                        }
                    }
                    Utility.snackbar(message: msg as! String,vc:vc)
                    let model = WaitingModel.initModel(res: [:])
                    completion(true, model)
                }
            }
            else
            {
                let model = WaitingModel.initModel(res: [:])
                completion(false,model)
            }
        }
    }
    
    class func getRefreshedBookedDriverdata(parameters : [String:AnyObject],vc:UIViewController,completion:@escaping bookingData){
        if(!(Reachability.isConnectedToNetwork())){
            Utility.showAlertWithMessage(vc: vc, message: K.MESSAGE.k_network_error)
            return
        }
        WebServiceHelper.sharedInstance.postFormDataWithHeader(input: parameters as AnyObject, image: nil, completeUrl: K.URL.BaseUrl + K.URL.BookedDriverData) { (success, data) in
            Utility.hideIndicator()
            if success == true
            {
                guard let response = data else {
                    return
                }
                guard let dic = response as? NSDictionary else {
                    return
                }
                guard let status = dic["status"] else {
                    return
                }
                if ((status as! String) == "success")
                {
                    let model = WaitingModel.initModel(res: dic as! [String : Any])
                    completion(true, model)
                }
                else
                {
                    guard let msg = dic["message"] else {
                        return
                    }
                    guard let code = dic["code"] else {
                        return
                    }
                    if UserDefaultManager.getIsLogin()!{
                        if (code as? Int) == 420{
                            Utility.logOut(vc: vc, message: msg as! String)
                            return
                        }
                    }
                    Utility.snackbar(message: msg as! String,vc:vc)
                    let model = WaitingModel.initModel(res: [:])
                    completion(true, model)
                }
            }
            else
            {
                let model = WaitingModel.initModel(res: [:])
                completion(false,model)
            }
        }
    }
    
    class func cancelBooking(parameters : [String:AnyObject],vc:UIViewController,completion:@escaping bookingData){
        if(!(Reachability.isConnectedToNetwork())){
            Utility.showAlertWithMessage(vc: vc, message: K.MESSAGE.k_network_error)
            return
        }
        Utility.showIndicator()
        WebServiceHelper.sharedInstance.postFormDataWithHeader(input: parameters as AnyObject, image: nil, completeUrl: K.URL.BaseUrl + K.URL.cancelBooking) { (success, data) in
            Utility.hideIndicator()
            if success == true
            {
                guard let response = data else {
                    return
                }
                guard let dic = response as? NSDictionary else {
                    return
                }
                guard let status = dic["status"] else {
                    return
                }
                if ((status as! String) == "success")
                {
                    let model = WaitingModel.initModel(res: dic as! [String : Any])
                    completion(true, model)
                }
                else
                {
                    
                    guard let msg = dic["message"] else {
                        return
                    }
                    guard let code = dic["code"] else {
                        return
                    }
                    
                    if (code as? Int) == 420{
                        Utility.logOut(vc: vc, message: msg as! String)
                        return
                    }
                    Utility.snackbar(message: msg as! String,vc:vc)
                    let model = WaitingModel.initModel(res: [:])
                    completion(true, model)
                }
            }
            else
            {
                let model = WaitingModel.initModel(res: [:])
                completion(false,model)
            }
        }
    }
    
    
    class func getDrivers(parameters : [String:AnyObject],vc:UIViewController,completion:@escaping driverResponse){
        if(!(Reachability.isConnectedToNetwork())){
            Utility.showAlertWithMessage(vc: vc, message: K.MESSAGE.k_network_error)
            return
        }
        WebServiceHelper.sharedInstance.postFormDataWithHeader(input: parameters as AnyObject, image: nil, completeUrl: K.URL.BaseUrl + K.URL.GetDriversList){(success, data) in
           
            if success == true
            {
                print("Driverdata**********")
                guard let response = data else {
                    return
                }
                guard let dic = response as? NSDictionary else {
                    return
                }
                guard let status = dic["status"] else {
                    return
                }
                if ((status as! String) == "success")
                {
                    let model = DriverListModel.initModel(res: dic as! [String : Any])
                    completion(true, model)
                }
                else
                {
                    guard let msg = dic["message"] else {
                        return
                    }
                    guard let code = dic["code"] else {
                        return
                    }
                    
                    if (code as? Int) == 420{
                        Utility.logOut(vc: vc, message: msg as! String)
                        return
                    }
                    Utility.snackbar(message: msg as! String,vc:vc)
                    let model = DriverListModel.initModel(res: [:])
                    completion(true, model)
                }
            }
            else
            {
                let model = DriverListModel.initModel(res: [:])
                completion(true, model)
            }
            
        }
    }
    
    
    class func logOutAPI(parameters : [String:AnyObject],vc:UIViewController,completion:@escaping status){
        if(!(Reachability.isConnectedToNetwork())){
            Utility.showAlertWithMessage(vc: vc, message: K.MESSAGE.k_network_error)
            return
        }
        Utility.showIndicator()
        WebServiceHelper.sharedInstance.callGetApiWithHeader(input: nil, completeUrl: K.URL.BaseUrl + K.URL.LogOut){(success, data) in
            Utility.hideIndicator()
            if success == true
            {
                guard let response = data else {
                    return
                }
                guard let dic = response as? NSDictionary else {
                    return
                }
                guard let status = dic["status"] else {
                    return
                }
                if ((status as! String) == "success")
                {
                    completion(true)
                }
                else
                {
                    guard let msg = dic["message"] else {
                        return
                    }
                    guard let code = dic["code"] else {
                        return
                    }
                    
                    if (code as? Int) == 420{
                        Utility.logOut(vc: vc, message: msg as! String)
                        return
                    }
                    Utility.snackbar(message: msg as! String,vc:vc)
                   
                    completion(false)
                }
            }
            else
            {
                completion(false)
            }
            
        }
    }
    
    
}



