//
// HomeModel.swift
//  LUX Application
//

import UIKit

class HomeModel: NSObject {
    var advertiseArr : NSArray?
    var notificationCount : Int?
    var imagepath : String?
    var timeinterval : Int?
    var booking_id : String?
    var status : String?
    var status_code :String?
    var rating : String?
    var paymentStatus: String?
    var actualTime : String?
    var actualDistance : String?
    var finalamount : String?
    
    class func  parsedata(advertiseArr:NSArray,notificationCount:Int,imagepath:String,timeinterval:Int,data:NSDictionary) -> HomeModel {
        let model = HomeModel()
        model.advertiseArr = advertiseArr
        model.notificationCount = notificationCount
        model.imagepath = imagepath
        model.timeinterval = timeinterval
    
        
        if let bookingdata = data["bookingdata"]
        {
            let bookingdataDic = bookingdata as! NSDictionary
            if let bookingdataArr = bookingdataDic["data"] as? NSArray
            {
                if bookingdataArr.count > 0
                {
                    if let bookingdataDic = bookingdataArr.object(at: 0) as? NSDictionary
                    {
                        model.booking_id = bookingdataDic["id"] as? String
                        model.status = bookingdataDic["status"] as? String
                        model.status_code = bookingdataDic["status_code"] as? String
                        model.rating = bookingdataDic["user_rating"] as? String
                         model.paymentStatus = bookingdataDic["payment_status"] as? String
                         model.actualTime = bookingdataDic["actually_time"] as? String
                        model.actualDistance = bookingdataDic["actually_distance"] as? String
                        model.finalamount = bookingdataDic["final_amount"] as? String

                        
                    }
                }
                
            }
            
        }
        return model
    }
    
    
    
    
    
}

