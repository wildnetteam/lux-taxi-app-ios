//
//  HomeViewModel.swift
//  LUX Application
//

import UIKit

class HomeViewModel: NSObject {
    private var homeModel: HomeModel
    var tempLinkArr : NSMutableArray = []
    var tempImageArr : NSMutableArray = []

    var booking_id : String?{
        return homeModel.booking_id
    }
    var rating : String?{
        return homeModel.rating
    }
    
    var paymentStatus: String?{
        return homeModel.paymentStatus
    }
    var actualTime : String?{
        return homeModel.actualTime
    }
    var actualDistance : String?{
            return homeModel.actualDistance
        }
    
    var finalamount : String?{
        return homeModel.actualDistance
    }
    
    var status : String?{
         return homeModel.status
    }
    var status_code :String?{
         return homeModel.status_code
    }
    var notificationCount: Int? {
        return homeModel.notificationCount
    }
    var imagePath: String? {
        return homeModel.imagepath
    }
    var timeInterval: Int? {
        return homeModel.timeinterval
    }
    var linkArr: NSMutableArray? {
        if let adArr = homeModel.advertiseArr
        {
            tempLinkArr = []
            if (adArr.count > 0)
            {
                for dic in adArr
                {
                    tempLinkArr.add((dic as! NSDictionary).value(forKey: "link") ?? "")
                }
               return tempLinkArr
            }
        }
        return []
    }
        
        var imageArr: NSMutableArray? {
            if let adArr = homeModel.advertiseArr
            {
                tempImageArr = []
                if (adArr.count > 0)
                {
                    for dic in adArr
                    {
                      tempImageArr.add(homeModel.imagepath! + ((((dic as! NSDictionary).value(forKey: "image")) as! String)))
                    }
                    return tempImageArr
                }
            }
    
       return []
    }
   
    
    init(model: HomeModel) {
        self.homeModel = model
    }
    
}
