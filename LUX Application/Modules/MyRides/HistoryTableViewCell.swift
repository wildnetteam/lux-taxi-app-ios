//
//  HistoryTableViewCell.swift
//  LUX Application
//
//  Created by Yogita on 26/02/18.
//  Copyright © 2018 WildnetTechnologies. All rights reserved.
//

import UIKit

class HistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var tripStatusHeight: NSLayoutConstraint!
    @IBOutlet weak var tripPrice: UILabel!
    @IBOutlet weak var tripStatusView: UIView!
    @IBOutlet weak var toLocation: UITextField!
    @IBOutlet weak var fromLocation: UITextField!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var carDetails: UILabel!
    @IBOutlet weak var tripDateAndTime: UILabel!
    @IBOutlet weak var tripStatusLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        Utility.addShadow(view: bgView)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(model:MyTripsViewModel){
        self.tripPrice.text = model.amount
        self.toLocation.text = model.drop_area
        self.fromLocation.text = model.pickup_area
        self.tripDateAndTime.text = model.book_create_date_time
        self.carDetails.text = model.carDetails
        
        
        if let status = model.status{
            self.tripDateAndTime.font = UIFont(name:"GothamRounded-Book", size: 13.0)
            tripStatusHeight.constant = 17
            switch status {
            case K.STATUS.PENDING:
               self.tripStatusView.backgroundColor = K.COLOR.ongoingColor
               self.tripStatusLabel.text = "WAITING"
               self.tripStatusLabel.textColor = K.COLOR.ongoingColor
               self.tripPrice.isHidden  = false
                break
            case K.STATUS.CANCELED:
                self.tripStatusView.backgroundColor = K.COLOR.cancelColor
                self.tripStatusLabel.text = "CANCELLED"
                self.tripStatusLabel.textColor = K.COLOR.cancelColor
                if let cancelPaymentStatus = model.cancelledStatus{
                    if let cancellationFees = model.cancelFee{
                        if(cancelPaymentStatus == "1" && cancellationFees > "0"){
                            self.tripPrice.text = model.cancelledFees
                            self.tripPrice.isHidden  = false
                        }
                        else
                        {
                            self.tripPrice.isHidden  = true
                        }
                    }
                }
                
               
                break
            case K.STATUS.EXPIRED:
                self.tripStatusView.backgroundColor = K.COLOR.cancelColor
                self.tripStatusLabel.text = "EXPIRED"
                self.tripStatusLabel.textColor = K.COLOR.cancelColor
                self.tripPrice.isHidden  = true
                break
            case K.STATUS.ONPICKUP:
                self.tripStatusView.backgroundColor = K.COLOR.ongoingColor
                self.tripStatusLabel.text = "ONGOING"
                self.tripStatusLabel.textColor = K.COLOR.ongoingColor
                self.tripPrice.isHidden  = false
                break
            case K.STATUS.ONTRIP:
                self.tripStatusLabel.text = "ONGOING"
                self.tripStatusLabel.textColor = K.COLOR.ongoingColor
                self.tripStatusView.backgroundColor = K.COLOR.ongoingColor
                self.tripPrice.isHidden  = false
                break
            case K.STATUS.ACCEPTED:
                self.tripStatusLabel.text = "ONGOING"
                self.tripStatusLabel.textColor = K.COLOR.ongoingColor
                self.tripStatusView.backgroundColor = K.COLOR.ongoingColor
                self.tripPrice.isHidden  = false
                break
            case K.STATUS.COMPLETED:
                self.tripStatusLabel.text = ""
               tripStatusHeight.constant = 0
                self.tripStatusView.backgroundColor = K.COLOR.completedColor
                self.tripPrice.isHidden  = false
                self.tripDateAndTime.font = UIFont(name:"GothamRounded-Book", size: 16.0)
                break
                self.layoutIfNeeded()
            default:
                break
            }
        }
        
    }
    
}
