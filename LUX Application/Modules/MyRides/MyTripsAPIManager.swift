//
//  HomeAPIManager.swift
//  LUX Application
//
//  Created by Yogita on 05/02/18.
//  Copyright © 2018 WildnetTechnologies. All rights reserved.
//

import Foundation
class MyTripsAPIManager{
    typealias myTripsResponse = (_ isSuccess: Bool,_ response : [MyTripsModel]) -> Void
    class func getTripsAPI(parameters : [String:AnyObject],vc:UIViewController,completion:@escaping myTripsResponse){
        if(!(Reachability.isConnectedToNetwork())){
            Utility.showAlertWithMessage(vc: vc, message: K.MESSAGE.k_network_error)
            return
        }
        Utility.showIndicator()
        WebServiceHelper.sharedInstance.callGetApiWithHeader(input: parameters as AnyObject, completeUrl: K.URL.BaseUrl + K.URL.getMyTrips){(success, data) in
            Utility.hideIndicator()
            if success == true
            {
                guard let response = data else {
                    return
                }
                guard let dic = response as? NSDictionary else {
                    return
                }
                guard let status = dic["status"] else {
                    return
                }
                if ((status as! String) == "success")
                {
                   
                    let model = MyTripsModel.initModel(res: dic as! [String : Any])
                    completion(true, model)
                }
                else
                {
                    guard let msg = dic["message"] else {
                        return
                    }
                    guard let code = dic["code"] else {
                        return
                    }
                    
                    if (code as? Int) == 420{
                        Utility.logOut(vc: vc, message: msg as! String)
                        return
                    }
                    Utility.snackbar(message: msg as! String,vc:vc)
                    let model = MyTripsModel.initModel(res: [:])
                    completion(false,model)
                }
                }
            else
            {
                let model = MyTripsModel.initModel(res: [:])
                completion(false,model)
            }
            
        }
    }
    
}



