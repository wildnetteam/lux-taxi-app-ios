//
//  HomeViewModel.swift
//  LUX Application
//

import UIKit

class MyTripsViewModel: NSObject {
    private var tripsModel: MyTripsModel

    var cancelledFees:String?{
        if let price = tripsModel.cancelledFees{
            if let priceInDouble = Double(price){
                return "$" + String(format: "%.2f", priceInDouble)
            }
        }
        return tripsModel.cancelledFees
    }
    
    var cancelFee : String?{
         return tripsModel.cancelledFees
    }
    
    var cancelledStatus : String?{
        return tripsModel.cancelledStatus
    }
    var booking_id : String?{
        return tripsModel.booking_id
    }
    var status : String?{
      return tripsModel.status
    }
    var status_code :String?{
        return tripsModel.status_code
    }
    var image : String?
    {
        guard let imgPath = tripsModel.driverimagepath else{
            return ""
        }
        guard let img = tripsModel.image else{
            return ""
        }
        return imgPath + img
    }
    var book_create_date_time :String?{
        if let bookDate = tripsModel.book_create_date_time
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatter.timeZone = TimeZone(identifier: "UTC")
            let date = dateFormatter.date(from: bookDate)
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat = "EEEE, dd MMM yyyy, hh:mm aa"
            dateFormatter1.timeZone = TimeZone.current
            dateFormatter1.locale = Locale.current
            let currentTimeZoneBookingTime = dateFormatter1.string(from: date!)
            return currentTimeZoneBookingTime
        } 
        else
        {
            return ""
        }
    }
    var carDetails:String?{
        guard let carName = tripsModel.car_name else{
            return ""
        }
        guard let carNumb = tripsModel.car_no else{
            return ""
        }
        return carNumb + " | " + carName
    }
    var amenities:String?{
        return tripsModel.amenities
    }
    var car_no:String?{
        return tripsModel.car_no
    }
    var name:String?{
        return tripsModel.name
    }
    var rating:String?{
        return tripsModel.rating
    }
    var drop_area:String?{
        return tripsModel.drop_area
    }
    
    var person:String?{
        return tripsModel.person
    }
    var offset : Int?{
        return tripsModel.offset
    }
    var distance:String?{
        return tripsModel.distance
    }
    var pickup_area:String?{
        return tripsModel.pickup_area
    }
    var amount:String?{
        if let price = tripsModel.amount{
            if let priceInDouble = Double(price){
               return "$" + String(format: "%.2f", priceInDouble)
            }
        }
        return tripsModel.amount
    }
    var bookingId : String?{
        return tripsModel.booking_id
    }
   
    
    init(model: MyTripsModel) {
        self.tripsModel = model
    }
}
