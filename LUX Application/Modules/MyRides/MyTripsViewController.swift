//
//  MyTripsViewController.swift
//  LUX Application
//
//  Created by Yogita on 26/02/18.
//  Copyright © 2018 WildnetTechnologies. All rights reserved.
//

import UIKit

class MyTripsViewController: UIViewController {

    @IBOutlet weak var myTripsTableView: UITableView!
    var tripsModel : [MyTripsViewModel] = []
    var offset = "0"
    var isDataLoading = false
        
    @IBOutlet weak var noTripsLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadData(notification:)), name: Notification.Name(K.KEY.refreshHistory), object: nil)
        myTripsTableView.register( UINib.init(nibName: "HistoryTableViewCell", bundle: nil), forCellReuseIdentifier: "HistoryTableViewCell")
        getTrips()
        // Do any additional setup after loading the view.
    }

    @objc func reloadData(notification: Notification){
        self.tripsModel = []
        self.myTripsTableView.reloadData()
        getTrips()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    func getTrips(){
        let params :[String:String] = ["offset":offset,"perpage":"10","list":"rider"]
        MyTripsAPIManager.getTripsAPI(parameters: params as [String : AnyObject], vc: self) { (success, response) in
            if success == true{
                if response.count > 0{
                    self.noTripsLabel.isHidden = true
                    for model in response{
                        self.tripsModel.append(MyTripsViewModel.init(model: model))
                        if let offset = model.offset{
                            self.offset =  String(offset)
                        }
                    }
                    
                    self.myTripsTableView.reloadData()
                }
                else
                {
                    self.isDataLoading = false
                }
            }
            else{
                 self.noTripsLabel.isHidden = false
            }
           
        }
    }
    
    @IBAction func menuButtonTapped(_ sender: UIButton) {
       self.swiftySideMenu.toggleSideMenu()
    }
    

}
extension MyTripsViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let status = tripsModel[indexPath.row].status{
            if status == K.STATUS.PENDING || status == K.STATUS.ACCEPTED || status == K.STATUS.ONTRIP || status == K.STATUS.ONPICKUP {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "HistoryDetailViewController") as! HistoryDetailViewController
                vc.model = tripsModel[indexPath.row]
                self.navigationController?.pushViewController(vc, animated: true)
            }
         
        }
       
       
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return self.tripsModel.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 230
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : HistoryTableViewCell = tableView.dequeueReusableCell(withIdentifier: "HistoryTableViewCell", for: indexPath) as! HistoryTableViewCell
        cell.selectionStyle = .none
        cell.setData(model: tripsModel[indexPath.row])
        return cell
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: 0) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
            // print("this is the last cell")
            let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            self.myTripsTableView.tableFooterView = spinner
            if(isDataLoading){
                self.myTripsTableView.tableFooterView?.isHidden = true
            }
            else
            {
               self.myTripsTableView.tableFooterView?.isHidden = true
            }
        }
    }
    
    //ScrollViewDelegates
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        print("scrollViewWillBeginDragging")
        isDataLoading = false
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("scrollViewDidEndDecelerating")
    }
    
    //Pagination
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        print("scrollViewDidEndDragging")
        if ((myTripsTableView.contentOffset.y + myTripsTableView.frame.size.height) >= myTripsTableView.contentSize.height)
        {
            if !isDataLoading{
                isDataLoading = true
                getTrips()
            }
        }
        
        
    }
    
}

