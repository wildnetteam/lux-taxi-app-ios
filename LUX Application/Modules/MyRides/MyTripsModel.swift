//
// HomeModel.swift
//  LUX Application
//

import UIKit
/*
 {
 amount = 350;
 "approx_time" = "39 mins";
 "assigned_for" = 1;
 "book_create_date_time" = "2018-02-23 18:28:44";
 distance = "12.5 mi";
 driverdata =                 {
 data =                     {
 "car_name" = 1;
 "car_type" = 1;
 cardata =                         {
 amenities = "test,testst";
 "car_image" = "Type-Hatchback2.png";
 "car_name" = "Car 1";
 "car_no" = 123456;
 carimagepath = "http://site4demo.com/luxapp-admin/car_image/";
 "seating_capacity" = 7;
 };
 "driver_id" = 1;
 image = "boy-5122.png";
 name = "Driver Name 1";
 phone = 9911223696;
 ratecard =                         {
 baseprice = "100.00";
 permile = "20.00";
 "ratecard_id" = 1;
 "ratecard_name" = "Rate Card 1";
 };
 rating = 3;
 };
 driverimagepath = "http://site4demo.com/luxapp-admin/driverimages/";
 status = success;
 };
 "drop_area" = "Barakhamba, New Delhi, Delhi, India";
 "drop_lat" = "28.6285887";
 "drop_long" = "77.2269242";
 "final_amount" = 0;
 id = 66;
 person = 5;
 "pickup_area" = "Lohia Road,Sector 63,Noida,Uttar Pradesh 201301,India,";
 "pickup_lat" = "28.6310985172434";
 "pickup_long" = "77.3811781313991";
 status = 5;
 "status_code" = completed;
 "taxi_id" = 1;
 "user_id" = 100;
 userdata =                 {
 data =                     (
 {
 id = 100;
 image = "user4513-file.png";
 mobile = 9876541230;
 name = Test;
 }
 );
 status = success;
 userimagepath = "http://site4demo.com/luxapp-admin/user_image/";
 };
 },
 */
class MyTripsModel: NSObject {
    var booking_id : String?
    var status : String?
    var status_code :String?
    var driverimagepath : String?
    var book_create_date_time :String?
    var car_name:String?
    var amenities:String?
    var car_no:String?
    var image:String?
    var name:String?
    var rating:String?
    var drop_area:String?
    var person:String?
    var distance:String?
    var pickup_area:String?
    var amount:String?
    var bookingId : String?
    var cancelledFees:String?
    var cancelledStatus : String?
    var offset : Int?
    /*
     "cancelled_fee": "40",
     "cancelled_status": "1",
 */

    class func initModel(res:[String : Any]) -> [MyTripsModel]
    {
        var tripData = [MyTripsModel]()
        if let bookingData = res["bookingdata"]{
            let bookingDataDic = bookingData as! NSDictionary
            if let data = bookingDataDic["data"]
            {
                for dic in data as! [[String:Any]]
                {
                    let model = MyTripsModel()
                    model.offset = res["offset"] as? Int
                    model.amount = dic["amount"] as? String 
                    model.distance = dic["distance"] as? String
                    model.book_create_date_time = dic["book_create_date_time"] as? String
                    model.drop_area = dic["drop_area"] as? String
                    model.cancelledFees = dic["cancelled_fee"] as? String
                    model.cancelledStatus = dic["cancelled_status"] as? String
                    model.pickup_area = dic["pickup_area"] as? String
                    model.status = dic["status"] as? String
                    model.status_code = dic["status_code"] as? String
                    model.booking_id = dic["id"] as? String
                    model.person = dic["person"] as? String

                    if let driverdata = dic["driverdata"]{
                        if let driverDic = driverdata as? NSDictionary
                        {
                            model.driverimagepath = driverDic["driverimagepath"] as? String
                            if let data = driverDic["data"]{
                                if let dataDic = data as? NSDictionary
                                {
                                    model.rating = dataDic["rating"] as? String
                                    model.name = dataDic["name"] as? String
                                    model.image = dataDic["image"] as? String
                                    
                                    if let cardata = dataDic["cardata"]{
                                        if let carDic = cardata as? NSDictionary
                                        {
                                            model.amenities = carDic["amenities"] as? String
                                            model.car_name = carDic["car_name"] as? String
                                            model.car_no = carDic["car_no"] as? String
                                        }
                                    }
                                }
                            }
                           
                        }
                    }
        
                    tripData.append(model)
                }
            }
        }
        
        
        return tripData
        
    }
    
    
    
    
    
    
}

