//
//  RatingViewController.swift
//  LUX Application
//
//  Created by Yogita on 27/02/18.
//  Copyright © 2018 WildnetTechnologies. All rights reserved.
//

import UIKit
import HCSStarRatingView

class RatingViewController: UIViewController,UITextViewDelegate,SSRadioButtonControllerDelegate {

    @IBOutlet weak var driverName: UILabel!
    @IBOutlet weak var carDetails: UILabel!
    @IBOutlet weak var pickUpAdd: UILabel!
    @IBOutlet weak var dropAddress: UILabel!
    @IBOutlet weak var ratingView: HCSStarRatingView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var driverImage: UIImageView!
    @IBOutlet weak var buttonPersonal: SSRadioButton!
    @IBOutlet weak var buttonBusiness: SSRadioButton!
    
    var isButtonSelected = false
    var selectedRideType = 0
    var model : WaitingViewModel?
    var radioButtonController: SSRadioButtonsController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Utility.roundView(view: driverImage, radius: driverImage.frame.size.width/2)
        radioButtonController = SSRadioButtonsController(buttons: buttonPersonal, buttonBusiness)
        radioButtonController!.delegate = self
      //  radioButtonController!.shouldLetDeSelect = true
        setData()
        self.textView.text = "Leave a comment.."
        self.textView.textColor = UIColor.gray
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setData(){
        self.driverName.text = model?.driverName
        self.carDetails.text = model?.carDetails
        self.pickUpAdd.text = model?.pickup_area
        self.dropAddress.text = model?.drop_area
        self.priceLabel.text = model?.finalamount
        if let image = model?.image
        {
            if let url = URL(string : image)
            {
                self.driverImage.setImageWith(url, placeholderImage: #imageLiteral(resourceName: "avatar-placeholder"))
            }
        }
    }
    
    func didSelectButton(selectedButton: UIButton?)
    {
        self.isButtonSelected = true
        if let selectedBtn = selectedButton{
            self.selectedRideType = selectedBtn.tag
        }
       // NSLog(" \(selectedButton)" )
    }
    
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
   
    @IBAction func submitButtonTapped(_ sender: UIButton) {
        var ratingText = self.textView.text
        if ratingView.value == 0{
            Utility.showAlertWithMessage(vc: self, message: K.MESSAGE.k_provide_rating)
            return
            
        }
        
        if textView.text.characters.count == 0 || textView.text == "Leave a comment.."{
          //Utility.showAlertWithMessage(vc: self, message: K.MESSAGE.k_leave_comment)
            ratingText = ""
           // return
        }
        
        if(selectedRideType == 0){
            Utility.showAlertWithMessage(vc: self, message: K.MESSAGE.k_provide_rideType)
            return
            
        }
        
        guard let bookingId = model?.bookingId else {
            return
        }
        
        guard let driverId = model?.driverId else {
            return
        }
        
        
        let params = ["rating":ratingView.value,"comment":ratingText,"booking_id":bookingId,"driver_id":driverId,"ride_type" : (selectedRideType == 200) ? "Business" : "Personal"] as [String : Any]
        print(params)
        RatingAPIManager.rateDriverAPI(parameters: params as [String : AnyObject], vc: self) { (success) in
            if success == true{
                self.dismiss(animated: true, completion: {
                    Utility.showAlertWithMessage(vc: self, message: K.MESSAGE.k_rating_done)
                })
            }
        }
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.gray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Leave a comment.."
            textView.textColor = UIColor.gray
        }
    }
    
}
