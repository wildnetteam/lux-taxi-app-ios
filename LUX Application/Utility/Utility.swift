//
//  Utility.swift
//  LUX Application
//
//  Created by Yogita on 02/02/18.
//  Copyright © 2018 WildnetTechnologies. All rights reserved.
//

import Foundation
import TTGSnackbar
import NVActivityIndicatorView

class Utility{
   static let activityData = ActivityData()

    class func pushViewController(fromVC:UIViewController, toVC: UIViewController, identifier: String)
    {
        let vc = fromVC.storyboard?.instantiateViewController(withIdentifier: identifier)
        if let vc = vc{
            fromVC.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    class func popViewController(fromVC:UIViewController) {
        _  = fromVC.navigationController?.popViewController(animated: true)
    }
    
    class func validateEmail( _ email : String ) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate( with: email )
    }
    
    class func validateMobile( _ mobile : String ) -> Bool
    {
        let mobRegEx = "[0-9]{6,14}$";
        let mobTest = NSPredicate(format:"SELF MATCHES %@", mobRegEx)
        return mobTest.evaluate( with: mobile )
    }
    
    class func addShadow(view:UIView)
    {
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.4
        view.layer.shadowOffset = CGSize(width:2,height:4)
        view.layer.shadowRadius = 10
    }
    
    class func addBottomShadow(view:UIView)
    {
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.4
        view.layer.shadowOffset = CGSize(width:0,height:4)
        view.layer.shadowRadius = 10
    }
    
    class func openUrl(url:String)
    {
        let url = URL(string: url)!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
            
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    class func validatePassword( _ password : String ) -> Bool
    {
        return password.characters.count >= 6 && password.characters.count <= 20
    }
    class func validateName( _ password : String ) -> Bool
    {
        return password.characters.count >= 2 && password.characters.count <= 35
    }
    
    class func validateMob( _ password : String ) -> Bool
    {
        return password.characters.count <= 16
    }
    
    class func isEmpty( _ string : String ) -> Bool
    {
        return string.characters.count == 0
    }
    
    
    class func textFieldBorderColor(_ view:Any, color:UIColor) -> Void {
        (view as AnyObject).layer.cornerRadius = 4.0
        (view as AnyObject).layer.masksToBounds = true
        (view as AnyObject).layer.borderColor = color.cgColor
        (view as AnyObject).layer.borderWidth = 1.0
    }
    
    
    class func addPaddingToTextField (textfield:UITextField) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height:textfield.frame.height))
        textfield.leftView = paddingView
        textfield.leftViewMode = UITextFieldViewMode.always
        
    }
    
    
    class func addRightView(textfield : UITextField, imagename : String)
    {
        let rightImage = UIImageView(frame: CGRect(x:0, y: 0, width: 30, height:textfield.frame.height))
        rightImage.image = UIImage(named : imagename)
        textfield.rightView = rightImage
        textfield.rightViewMode = UITextFieldViewMode.always
    }
    
    class func roundView(view : Any, radius : CGFloat)
    {
        (view as AnyObject).layer.cornerRadius = radius
        (view as AnyObject).layer.masksToBounds = true
    }
    
    class func snackbar (message : String,vc:UIViewController)
    {
        self.showAlertWithMessage(vc: vc, message: message)
    }
    
    class func checkSpecialCharaters(string:String) -> Bool{
        let characterset = CharacterSet(charactersIn: " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
        if string.rangeOfCharacter(from: characterset.inverted) != nil {
            return true // contains special characters
        }
        else
        {
            return false
        }
    }
    
    class func showSnackbar(message : String,vc:UIViewController?){
        let snackbar = TTGSnackbar()
        snackbar.message = message
        snackbar.duration = .forever
        snackbar.backgroundColor = UIColor.gray
        snackbar.messageTextAlign = .center
        snackbar.animationType = .slideFromTopBackToTop
        snackbar.show()
    }
    
    class func showSnackbarWithLessDuration(message : String,vc:UIViewController?){
        let snackbar = TTGSnackbar()
        snackbar.message = message
        snackbar.backgroundColor = UIColor.gray
        snackbar.messageTextAlign = .center
        snackbar.animationType = .slideFromTopBackToTop
        snackbar.show()
    }
    
    class func showIndicator()
    {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(self.activityData, nil)
    }
    
    class func hideIndicator(){
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
    }
    
    class func showAlert(vc:UIViewController,message:String)
    {
    let alertController : UIAlertController = UIAlertController(title: K.MESSAGE.k_app_name, message: message, preferredStyle: UIAlertControllerStyle.alert)
    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
    UIAlertAction in
     _ = vc.navigationController?.popViewController(animated: true)
    }
    alertController.addAction(okAction)
    vc.present(alertController, animated: true, completion: nil)
    }
    
    class func showAlertWithMessage(vc:UIViewController,message:String)
    {
        let alertController : UIAlertController = UIAlertController(title: K.MESSAGE.k_app_name, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            UIAlertAction in
        }
        alertController.addAction(okAction)
        vc.present(alertController, animated: true, completion: nil)
    }
    
    class func enableSwipe(enable:Bool,swiftySideMenu:SwiftySideMenuViewController){
        swiftySideMenu.enableLeftSwipeGesture = false
        swiftySideMenu.enableRightSwipeGesture = false
    }
    
    class func logOut(vc:UIViewController,message:String)
    {
        let alertController : UIAlertController = UIAlertController(title: K.MESSAGE.k_app_name, message: message, preferredStyle: UIAlertControllerStyle.alert)
       
        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) {
            UIAlertAction in
            UserDefaultManager.removedata()
            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let loginVC : LoginViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            appDelegate.navigationController.pushViewController(loginVC, animated: true)
            if(vc.swiftySideMenu.isLeftMenuOpened()){
                vc.swiftySideMenu.toggleSideMenu()
            }
            vc.swiftySideMenu.centerViewController = appDelegate.navigationController
        }
        alertController.addAction(okAction)
        
        vc.present(alertController, animated: true, completion: nil)
    }
    
    class func isIPhoneX()->Bool{
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                return false
            // print("iPhone 5 or 5S or 5C")
            case 1334:
                return false
            //print("iPhone 6/6S/7/8")
            case 1920, 2208:
                return false
            // print("iPhone 6+/6S+/7+/8+")
            case 2436:
                return true
            //  print("iPhone X")
            default:
                return false
                //print("unknown")
            }
        }
    return false
}
    
    class func isIPhone5()->Bool{
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                return true
            // print("iPhone 5 or 5S or 5C")
            case 1334:
                return false
            //print("iPhone 6/6S/7/8")
            case 1920, 2208:
                return false
            // print("iPhone 6+/6S+/7+/8+")
            case 2436:
                return false
            //  print("iPhone X")
            default:
                return false
                //print("unknown")
            }
        }
        return false
    }
}

extension UIScreen {
    
    enum SizeType: CGFloat {
        case Unknown = 0.0
        case iPhone4 = 960.0
        case iPhone5 = 1136.0
        case iPhone6 = 1334.0
        case iPhone6Plus = 1920.0
    }
    
    var sizeType: SizeType {
        let height = nativeBounds.height
        guard let sizeType = SizeType(rawValue: height) else { return .Unknown }
        return sizeType
    }
}
extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}




