  //
//  WebServiceHelper.swift
  //  GenericParseTask
  //
  //  Created by Yogita on 31/01/18.
  //  Copyright © 2018 WildnetTechnologies. All rights reserved.
//

import UIKit
import AFNetworking

class WebServiceHelper: NSObject {
    
    typealias completionBlock = (_ isSuccess: Bool,_ result:AnyObject?) -> Void
    private let sessionManager = AFHTTPSessionManager()
    private let manager = AFHTTPRequestSerializer()
    static let sharedInstance = WebServiceHelper()
   // private lazy var sessionManager : AFHTTPSessionManager = {
//        let securityPolicy = AFSecurityPolicy(pinningMode: .none)
//        securityPolicy.allowInvalidCertificates = true
//        securityPolicy.validatesDomainName = false
//
//        let manager = AFHTTPSessionManager()
//        manager.securityPolicy = securityPolicy
     //  return manager
 //
   // }()
    
    //*****************************************************************
    // MARK: AFNetworking GetMethod Call
    //********* ********************************************************
    
    func callGetApiWithDetail(input:AnyObject?,completeUrl:String , completion: @escaping (_ isSuccess: Bool,_ result:AnyObject?) -> Void) {
        //sessionManager.securityPolicy =
        sessionManager.requestSerializer = AFJSONRequestSerializer()
        sessionManager.responseSerializer = AFJSONResponseSerializer()
        sessionManager.requestSerializer.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        sessionManager.requestSerializer.setValue("KE3sFRnbY4", forHTTPHeaderField: "secretkey")
        sessionManager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/html","application/json","text/json","text/plain","application/x-www-form-urlencoded") as? Set<String>
        sessionManager.get(completeUrl, parameters: input, progress: nil, success: {(taks,response)-> Void in
            completion(true, response as AnyObject)
        }, failure: {(task,error)-> Void in
        completion(false, error as AnyObject)
        })
        }
    
    func callGetApiWithHeader(input:AnyObject?,completeUrl:String , completion: @escaping (_ isSuccess: Bool,_ result:AnyObject?) -> Void) {
        
        sessionManager.requestSerializer = AFJSONRequestSerializer()
        sessionManager.responseSerializer = AFJSONResponseSerializer()
        sessionManager.requestSerializer.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        sessionManager.requestSerializer.setValue(UserDefaultManager.getToken(), forHTTPHeaderField: "token")
        sessionManager.requestSerializer.setValue("KE3sFRnbY4", forHTTPHeaderField: "secretkey")
        sessionManager.requestSerializer.setValue(UserDefaultManager.getUserId(), forHTTPHeaderField: "uid")
        sessionManager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/html","application/json","text/json","text/plain","application/x-www-form-urlencoded") as? Set<String>
        sessionManager.get(completeUrl, parameters: input, progress: nil, success: {(taks,response)-> Void in
            completion(true, response as AnyObject)
        }, failure: {(task,error)-> Void in
            completion(false, error as AnyObject)
        })
    }
    
    
    
    //*****************************************************************
    // MARK: AFNetworking PostMethod Call
    //*****************************************************************
    
    func callPostApiWithDetail(input:AnyObject?,completeUrl:String , completion: @escaping (_ isSuccess: Bool,_ result:AnyObject?) -> Void){
        
       
            sessionManager.requestSerializer = AFHTTPRequestSerializer()
            sessionManager.responseSerializer = AFJSONResponseSerializer()
            sessionManager.responseSerializer.acceptableContentTypes = NSSet(object: "text/html") as? Set<String>
        sessionManager.requestSerializer.setValue("KE3sFRnbY4", forHTTPHeaderField: "secretkey")
        sessionManager.post(completeUrl, parameters: input, constructingBodyWith: { (data) in
            
        }, progress: { (progress) in
            
        }, success: { (dataTask, response) in
            completion(true, response as AnyObject)
        }) { (dataTask, error) in
            completion(false, error as AnyObject)
        }
    }
    
    func postFormData(input:AnyObject?,image:UIImage?,completeUrl:String , completion: @escaping (_ isSuccess: Bool,_ result:AnyObject?) -> Void){
        sessionManager.requestSerializer = AFHTTPRequestSerializer()
        sessionManager.responseSerializer = AFJSONResponseSerializer()
        sessionManager.responseSerializer.acceptableContentTypes = NSSet(object: "text/html") as? Set<String>
        sessionManager.requestSerializer.setValue("KE3sFRnbY4", forHTTPHeaderField: "secretkey")

        sessionManager.post(completeUrl, parameters: input, constructingBodyWith: { (data) in
            if let image = image
            {
                if let imageData = UIImageJPEGRepresentation(image, 0.6) {
                    data.appendPart(withFileData: imageData, name: "image", fileName: "file.png", mimeType: "image/png")
                }
            }
            
        }, progress: { (progress) in
            
        }, success: { (dataTask, response) in
            completion(true, response as AnyObject)
        }) { (dataTask, error) in
           completion(false, error as AnyObject)
        }
    }
    
    func postFormDataWithHeader(input:AnyObject?,image:UIImage?,completeUrl:String , completion: @escaping (_ isSuccess: Bool,_ result:AnyObject?) -> Void){
        sessionManager.requestSerializer = AFHTTPRequestSerializer()
        sessionManager.responseSerializer = AFJSONResponseSerializer()
        sessionManager.responseSerializer.acceptableContentTypes = NSSet(object: "text/html") as? Set<String>
        sessionManager.requestSerializer.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        sessionManager.requestSerializer.setValue(UserDefaultManager.getToken(), forHTTPHeaderField: "token")
        sessionManager.requestSerializer.setValue("KE3sFRnbY4", forHTTPHeaderField: "secretkey")
        sessionManager.requestSerializer.setValue(UserDefaultManager.getUserId(), forHTTPHeaderField: "uid")
        sessionManager.post(completeUrl, parameters: input, constructingBodyWith: { (data) in
            if let image = image
            {
                if let imageData = UIImageJPEGRepresentation(image, 0.6) {
                    data.appendPart(withFileData: imageData, name: "image", fileName: "file.png", mimeType: "image/png")
                }
            }
            
        }, progress: { (progress) in
            
        }, success: { (dataTask, response) in
            completion(true, response as AnyObject)
        }) { (dataTask, error) in
            completion(false, error as AnyObject)
        }
    }
    func postData(input:AnyObject?,completeUrl:String , completion: @escaping (_ isSuccess: Bool,_ result:AnyObject?) -> Void){
        sessionManager.requestSerializer = AFHTTPRequestSerializer()
        sessionManager.responseSerializer = AFJSONResponseSerializer()
        sessionManager.responseSerializer.acceptableContentTypes = NSSet(object: "text/html") as? Set<String>
        sessionManager.requestSerializer.setValue("KE3sFRnbY4", forHTTPHeaderField: "secretkey")
        sessionManager.post(completeUrl, parameters: input, constructingBodyWith: { (data) in
            
        }, progress: { (progress) in
            
        }, success: { (dataTask, response) in
            completion(true, response as AnyObject)
        }) { (dataTask, error) in
            completion(false, error as AnyObject)
        }
    }
}
