//
//  LocationErrorView.swift
//  Woof
//
//  Created by Pankaj Singh on 09/08/17.
//
//

import UIKit

class LocationErrorView: UIView {

    class func instanceFromNib() -> LocationErrorView
    {
        let view =   UINib(nibName: "LocationErrorView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! LocationErrorView
        return view
    }

}
