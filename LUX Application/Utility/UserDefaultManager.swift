//
//  UserDefaultManager.swift
//  LUX Application
//
//  Created by Yogita on 30/01/18.
//  Copyright © 2018 WildnetTechnologies. All rights reserved.
//

import UIKit

class UserDefaultManager: NSObject {
    static let userDefaults = UserDefaults.standard
    
    // MARK:- Setting and Getting  email
    class func setCurrentEmail(email:String?){
        userDefaults.set(email, forKey: K.KEY.Email)
        userDefaults.synchronize()
    }
    
    class func getCurrentEmail()->String?{
        if let userInfo = userDefaults.value(forKey: K.KEY.Email){
            return userInfo as? String
        }else{
            return nil
        }
    }
    
    // MARK:- Setting and Getting  mobile
    class func setCurrentMobile(mob:String?){
        userDefaults.set(mob, forKey: K.KEY.Mobile)
        userDefaults.synchronize()
    }
    
    class func getCurrentMobile()->String?{
        if let userInfo = userDefaults.value(forKey: K.KEY.Mobile){
            return userInfo as? String
        }else{
            return nil
        }
    }
    
    // MARK:- Setting and Getting  UserID
    class func setUserId(id:String?){
        userDefaults.set(id, forKey: K.KEY.Id)
        userDefaults.synchronize()
    }
    
    class func getUserId()->String?{
        if let userInfo = userDefaults.value(forKey: K.KEY.Id){
            return userInfo as? String
        }else{
            return nil
        }
    }
    
    // MARK:- Setting and Getting  DOB
    class func setCurrentDob(dob:String?){
        userDefaults.set(dob, forKey: K.KEY.Dob)
        userDefaults.synchronize()
    }
    
    class func getCurrentDob()->String?{
        if let userInfo = userDefaults.value(forKey: K.KEY.Dob){
            return userInfo as? String
        }else{
            return nil
        }
    }
    // MARK:- Setting and Getting  state
    class func setCurrentState(state:String?){
        userDefaults.set(state, forKey: K.KEY.State)
        userDefaults.synchronize()
    }
    
    class func getCurrentState()->String?{
        if let userInfo = userDefaults.value(forKey: K.KEY.State){
            return userInfo as? String
        }else{
            return nil
        }
    }
    
    // MARK:- Setting and Getting  Country
    class func setCurrentCountry(state:String?){
        userDefaults.set(state, forKey: K.KEY.Country)
        userDefaults.synchronize()
    }
    
    class func getCurrentCountry()->String?{
        if let userInfo = userDefaults.value(forKey: K.KEY.Country){
            return userInfo as? String
        }else{
            return nil
        }
    }
    
    // MARK:- Setting and Getting  Country
    class func setCurrentCity(city:String?){
        userDefaults.set(city, forKey: K.KEY.City)
        userDefaults.synchronize()
    }
    
    class func getCurrentCity()->String?{
        if let userInfo = userDefaults.value(forKey: K.KEY.City){
            return userInfo as? String
        }else{
            return nil
        }
    }
    
    // MARK:- Setting and Getting  Zipcode
    class func setCurrentZipcode(state:String?){
        userDefaults.set(state, forKey: K.KEY.Zipcode)
        userDefaults.synchronize()
    }
    
    class func getCurrentZipcode()->String?{
        if let userInfo = userDefaults.value(forKey: K.KEY.Zipcode){
            return userInfo as? String
        }else{
            return nil
        }
    }
    
    
    // MARK:- Setting and Getting  Image Path
    class func setImagePath(imagePath:String?){
        userDefaults.set(imagePath, forKey: K.KEY.ImagePath)
        userDefaults.synchronize()
    }
    
    class func getImagePath()->String?{
        if let userInfo = userDefaults.value(forKey: K.KEY.ImagePath){
            return userInfo as? String
        }else{
            return nil
        }
    }
    
    // MARK:- Setting and Getting Token
    class func setToken(token:String?){
        userDefaults.set(token, forKey: K.KEY.Token)
        userDefaults.synchronize()
    }
    
    class func getToken()->String?{
        if let userInfo = userDefaults.value(forKey: K.KEY.Token){
            return userInfo as? String
        }else{
            return nil
        }
    }
    
    // MARK:- Setting and Getting  isLogin
    class func setIsLogin(isLogin:Bool?){
        userDefaults.set(isLogin, forKey: K.KEY.IsLogin)
        userDefaults.synchronize()
    }
    
    class func getIsLogin()->Bool?{
        if let userInfo = userDefaults.value(forKey: K.KEY.IsLogin){
            return userInfo as? Bool
        }else{
            return false
        }
    }
    
    // MARK:- Setting and Getting Name
    class func setCurrentName(isLogin:String?){
        userDefaults.set(isLogin, forKey: K.KEY.Name)
        userDefaults.synchronize()
    }
    
    class func getCurrentName()->String?{
        if let userInfo = userDefaults.value(forKey: K.KEY.Name){
            return userInfo as? String
        }else{
            return nil
        }
    }
    
    // MARK:- Setting and Getting Address
    class func setCurrentAddress(address:String?){
        userDefaults.set(address, forKey: K.KEY.Address)
        userDefaults.synchronize()
    }
    
    class func getCurrentAddress()->String?{
        if let userInfo = userDefaults.value(forKey: K.KEY.Address){
            return userInfo as? String
        }else{
            return nil
        }
    }
    
    // MARK:- Setting and Getting SocialId
    class func setSocialId(id:String?){
        userDefaults.set(id, forKey: K.KEY.SocialId)
        userDefaults.synchronize()
    }
    
    class func getSocialId()->String?{
        if let userInfo = userDefaults.value(forKey: K.KEY.SocialId){
            return userInfo as? String
        }else{
            return nil
        }
    }
    
    // MARK:- Setting and Getting image
    class func setCurrentImage(image:String?){
        userDefaults.set(image, forKey: K.KEY.Image)
        userDefaults.synchronize()
    }
    
    class func getCurrentImage()->String?{
        if let userInfo = userDefaults.value(forKey: K.KEY.Image){
            return userInfo as? String
        }else{
            return nil
        }
    }
    
    class func removedata()
    {
    let domain = Bundle.main.bundleIdentifier!
    UserDefaults.standard.removePersistentDomain(forName: domain)
    UserDefaults.standard.synchronize()
    }
}
