//
//  MarkerInfoWindow.swift
//  LUX Application
//
//  Created by Yogita on 22/03/18.
//  Copyright © 2018 WildnetTechnologies. All rights reserved.
//

import UIKit

class MarkerInfoWindow: UIView {
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var minsLabel: UILabel!
    
    
    class func instanceFromNib() -> MarkerInfoWindow
    {
        let view =   UINib(nibName: "MarkerInfoWindow", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! MarkerInfoWindow
        return view
    }
    
    func setData(notification: Notification){
     self.timeLabel.text = appDelegate.timeRemaining
    }

}
