//
// WaitingView.swift
//
//
//

import UIKit
import HCSStarRatingView

protocol WaitingViewDelegate : class {
    func cancelButtontapped()
    
}
class WaitingView: UIView {
    weak var delegate : WaitingViewDelegate?
    @IBOutlet weak var carDetailsLabel: UILabel!
    @IBOutlet weak var passengersLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var ratingView: HCSStarRatingView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var ratingViewHeight: NSLayoutConstraint!
    @IBOutlet weak var ratingViewWidth: NSLayoutConstraint!
    @IBOutlet weak var timeLabel: UILabel!
    //var timer : Timer?
    var currentTimeInSeconds = 120
    
    @IBOutlet weak var cardetailsBottomConstraint: NSLayoutConstraint!
    
    class func instanceFromNib() -> WaitingView
    {
        let view = UINib(nibName: "WaitingView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! WaitingView
        view.currentTimeInSeconds = 120
        appDelegate.expireTimer = nil
        appDelegate.expireTimer?.invalidate()
        return view
    }
   
    
    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        self.delegate?.cancelButtontapped()
    }
    
    func startTimer()
    {
        appDelegate.expireTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.timerTicked), userInfo: nil, repeats: true)
    }
    
    @objc func timerTicked()
    {
        checkTime()
        currentTimeInSeconds =  currentTimeInSeconds - 1
        DispatchQueue.main.async {
          self.timeLabel.text = self.formatedtime(totalSeconds: Double(self.currentTimeInSeconds))
        }
    }
    
    func checkTime(){
        if(currentTimeInSeconds <= 2){
            appDelegate.expireTimer?.invalidate()
            appDelegate.expireTimer = nil
            self.timeLabel.text = "-- : --"
          NotificationCenter.default.post(name: Notification.Name("ExpireRide"), object: nil)
            /// currentTimeInSeconds = 120
        }
        
    }
    
    func formatedtime(totalSeconds:Double) ->String{
        let seconds =  totalSeconds.truncatingRemainder(dividingBy: 60)
        let minutes = (totalSeconds / 60).truncatingRemainder(dividingBy: 60)
     //   print(String(format: "%02d", Int(minutes)) + " : " + String(format: "%02d",Int(seconds)))
        return String(format: "%02d", Int(minutes)) + " : " + String(format: "%02d",Int(seconds))
    }
    
    func dateToSecondConvertor(str:String) -> Int {
        let components = str.components(separatedBy: " : ")
        let minutes = Int(components[0])
        let seconds = Int(components[1])
        return (minutes! * 60) + seconds!
    }

    func setData(model:WaitingViewModel)
    {
        if let bookingdate = model.bookingDateTime{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatter.timeZone = TimeZone(identifier: "UTC")
            let date1 = dateFormatter.date(from: bookingdate)
            
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatter1.timeZone = TimeZone.current
            dateFormatter1.locale = Locale.current
            let currentTimeZoneBookingTime = dateFormatter1.string(from: date1!)
            let todaysDate = Date()
            let currentDateStr =  dateFormatter1.string(from: todaysDate)
            let currentdate = dateFormatter1.date(from: currentDateStr)
            let timeInt  = currentdate?.timeIntervalSince(dateFormatter1.date(from: currentTimeZoneBookingTime)!)
            let timeDifference = self.formatedtime(totalSeconds: timeInt!)
            currentTimeInSeconds = 120 - self.dateToSecondConvertor(str: timeDifference)
            
            if(appDelegate.expireTimer == nil){
             self.startTimer()
            }
        }
        
        if UIScreen.main.sizeType == .iPhone5 {
            ratingViewHeight.constant = 10
            ratingViewWidth.constant = 60
            cardetailsBottomConstraint.constant = 5
        }
        Utility.roundView(view:userImage , radius: userImage.frame.size.width/2)
        carDetailsLabel.text = model.carDetails
        passengersLabel.text = model.person
        priceLabel.text = model.totalPrice
        if let image = model.image
        {
            if let url = URL(string : image)
            {
                self.userImage.setImageWith(url, placeholderImage: #imageLiteral(resourceName: "avatar-placeholder"))
            }
        }
        if let rate = model.rating{
            ratingView.value = rate
        }
        userName.text = model.driverName
    }
}
