//
//  ConnectionManager.swift
//  LUX Driver
//
//  Created by Yogita on 25/05/18.
//  Copyright © 2018 WildnetTechnologies. All rights reserved.
//

import Foundation
import TTGSnackbar
class ConnectionManager {
    
    static let sharedInstance = ConnectionManager()
    private var reachability : NetworkReachability!
    static var snackbar = TTGSnackbar.init(message: "No Network Connection", duration: .forever)

    func observeReachability(){
        self.reachability = NetworkReachability()
        NotificationCenter.default.addObserver(self, selector:#selector(self.reachabilityChanged), name: NSNotification.Name.reachabilityChanged, object: nil)
        do {
            try self.reachability.startNotifier()
        }
        catch(let error) {
            print("Error occured while starting reachability notifications : \(error.localizedDescription)")
        }
    }
    
    @objc func reachabilityChanged(note: Notification) {
        let reachability = note.object as! NetworkReachability
        switch reachability.connection {
        case .cellular:
            ConnectionManager.snackbar.dismiss()
            print("Network available via Cellular Data.")
            NotificationCenter.default.post(name: Notification.Name(K.KEY.refreshBookingData), object: nil)
            NotificationCenter.default.post(name: Notification.Name(K.KEY.refreshHistory), object: nil)
            NotificationCenter.default.post(name: Notification.Name(K.KEY.refreshNotification), object: nil)
            NotificationCenter.default.post(name: Notification.Name(K.KEY.refreshLegal), object: nil)
            break
        case .wifi:
            ConnectionManager.snackbar.dismiss()
            print("Network available via WiFi.")
            NotificationCenter.default.post(name: Notification.Name(K.KEY.refreshBookingData), object: nil)
            NotificationCenter.default.post(name: Notification.Name(K.KEY.refreshHistory), object: nil)
            NotificationCenter.default.post(name: Notification.Name(K.KEY.refreshNotification), object: nil)
            NotificationCenter.default.post(name: Notification.Name(K.KEY.refreshLegal), object: nil)
            break
        case .none:
            ConnectionManager.snackbar.backgroundColor = UIColor.red
            ConnectionManager.snackbar.messageTextAlign = .center
            ConnectionManager.snackbar.animationType = .slideFromBottomBackToBottom
            ConnectionManager.snackbar.show()
            print("Network is not available.")
            break
        }
    }
}
