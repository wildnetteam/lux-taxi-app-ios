//
//  LocationHandler.swift
//
//

import UIKit
import CoreLocation
import GooglePlaces

class LocationHandler:NSObject,CLLocationManagerDelegate {
    
    static let sharedInstance = LocationHandler()
    var locationManager: CLLocationManager!
    var handler: (CLLocation?, NSError?, Bool) -> () = {arg,error,success in }
    var geoCoderHandler: (String) -> () = {address in }
    var location:CLLocation?
    
    private override init() {
        super.init()
        locationManager = CLLocationManager()
        locationManager.delegate = self
        //locationManager.distanceFilter =  10
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.requestWhenInUseAuthorization()
    }
    
    func startUpdatingLocation() {
        locationManager.startUpdatingLocation()
    }
    
    func stopUpdatingLocation() {
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        if let currentLocation = locations.last {
            self.handler(currentLocation, nil, true)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        self.handler(nil, error as NSError?, false)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        if status == CLAuthorizationStatus.authorizedAlways || status == CLAuthorizationStatus.authorizedWhenInUse
        {
            locationManager.startUpdatingLocation()
        }
    }
    
    func updateLocation(completion:@escaping(_  location: CLLocation?, _ error : NSError?, _ isLocationEnable: Bool )->()){
        self.handler = completion
        locationManager.startUpdatingLocation()
        
        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.notDetermined
        {
            locationManager.requestWhenInUseAuthorization()
        }
        else if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse || CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways
        {
            locationManager.startUpdatingLocation()
        }
    }
    
    func getAddress(completion:@escaping(_ address : String)->()){
        let placesClient = GMSPlacesClient()
        placesClient.currentPlace { (response, error) in
            if(error != nil){
              return
            }
            if((response?.likelihoods.count)! > 0){
                if let add = ((response?.likelihoods[0])?.place.formattedAddress){
                    completion(add)
                }
            }
           
        }
    }
    
    func reverseGeoCoding(location : CLLocation,completion:@escaping(_ address : String)->())
    {
        getAddress { (address) in
          completion(address)
        }
        /*let longitude = location.coordinate.longitude
        let latitude = location.coordinate.latitude
        print(String(format: "%.5f", longitude))
        print(String(format: "%.5f", latitude))

        let locationss = CLLocation(latitude: latitude, longitude: longitude)
        
        CLGeocoder().reverseGeocodeLocation(locationss, completionHandler: {(placemarks, error) -> Void in
            //77.381186171891613 28.631092975831514
            if error != nil {
                print("Reverse geocoder failed with error" + error!.localizedDescription)
                return
            }
            if placemarks!.count > 0 {
                let pm = placemarks![0]
                
                var address = ""
                if let addDic = pm.addressDictionary
                {
                    if let addArr = addDic["FormattedAddressLines"] as? NSArray
                    {
                        if(addArr.count > 0)
                        {
                            for str in addArr
                            {
                                address = address + (str as! String) + ","
                            }
                            completion(address)
                        }
                        
                    }
                }
              //  print(pm.addressDictionary!["FormattedAddressLines"])
            }
            else {
                print("Problem with the data received from geocoder")
            }
        })*/
    }
    

    
}
