//
//  Constants.swift
//  LUX Application
//
//  Created by Yogita on 30/01/18.
//  Copyright © 2018 WildnetTechnologies. All rights reserved.
//

import Foundation
import TTGSnackbar
typealias completionBlock = (_ isSuccess: Bool,_ result:AnyObject?) -> Void
typealias status = (_ isSuccess: Bool) -> Void
let appDelegate = UIApplication.shared.delegate as! AppDelegate
let googleKEY = "AIzaSyDm5YMCSoGGj9zBu0FnOw6RDoAMSYWTnXc"//"AIzaSyDEIbZSHmKJLyrZ4gVLbAcvkBQWM97BmhE"//"AIzaSyBnSLFqiSBiX9eWz09hCAC-pYTy-w62CK0"
let passengerArr = ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40"]
struct K {
    struct URL {
         static let BaseUrl = "https://api.luxurytransportsinc.com/web_service/"
        //"http://site4demo.com/luxapp-adminv2/web_service/"//"http://site4demo.com/luxapp-admin/web_service/"
        
         static let urlCopyRight = "https://www.google.com"
         static let urlTc = "https://www.luxurytransportsinc.com/riders"
         static let urlPrivacy = "https://www.luxurytransportsinc.com/riders-1"//"https://www.luxurytransportsinc.com/companies-1"
         static let urlLocationInfo = "https://www.luxurytransportsinc.com/riders-2"
         static let SignUp = "sign_up"
         static let Login = "login"
         static let forgotPassword = "forgot_password"
         static let SocialLogin = "social_login_api"
         static let HomeCommon = "getcommonhomeapi"
         static let GetUserDetails = "getuserdetails"
         static let EditUserDetails = "profile_edit"
         static let GetDriversList = "getdriverlist"
         static let BookTaxi = "book_cab"
         static let BookedDriverData = "getbookeddriverdata"
         static let cancelBooking = "usertripaction"
         static let getMyTrips = "getusertrips"
         static let DriverRating = "driverrating"
         static let ChangePassword = "changepassword"
         static let RiderLocation = "riderlocation"
         static let UpdateDeviceToken = "updatedevice"
         static let Getridernotification = "getridernotification"
         static let MailInvoice = "mailinvoice"
         static let LogOut = "riderlogout"
         static let PaymentStatus = "paymentstatus"
         static let PaymentProcess = "paymentprocess"

    }
    struct KEY {
         static let stripeTestKey = "pk_test_mZ9qbA85qyx8jmPQPBKt6sys"
         static let stripeLiveKey = "pk_live_VpM6ZhhpcoiTYlO2SzvoYze4"
         static let ImagePath = "imagePath"
         static let Token = "token"
         static let Email = "email"
         static let Name = "name"
         static let Address = "address"
         static let IsLogin = "isLogin"
         static let Image = "image"
         static let SocialId = "socialId"
         static let Dob = "dob"
         static let State = "state"
         static let Country = "country"
         static let City = "city"
         static let Zipcode = "zipcode"

         static let Mobile = "mobile"
         static let Id = "id"
         static let refreshBookingData = "refreshBookingData"
         static let refreshHistory = "refreshHistory"
         static let refreshNotification = "refreshNotification"
         static let refreshLegal = "refreshLegal"
         static let GoogleAPIKey = "AIzaSyDm5YMCSoGGj9zBu0FnOw6RDoAMSYWTnXc"//"AIzaSyDEIbZSHmKJLyrZ4gVLbAcvkBQWM97BmhE"//"AIzaSyCCRNWGtgUSU2n28FN5HSnB3viwdBWfBk4"
         static let GoogleDirectionKey = "AIzaSyDm5YMCSoGGj9zBu0FnOw6RDoAMSYWTnXc"//"AIzaSyDEIbZSHmKJLyrZ4gVLbAcvkBQWM97BmhE"//"AIzaSyBfs_YwDKpIomLRs4OagTBIMDUPGaaYrjs"
        
    }
    
    struct COLOR {
        static let ongoingColor = UIColor(red:0.3, green:0.55, blue:0, alpha:1)
        static let cancelColor = UIColor(red:0.92, green:0, blue:0, alpha:1)
        static let completedColor = UIColor(red:0.44, green:0.06, blue:0.65, alpha:1)
        static let disabledColor = UIColor(white: 1, alpha: 0.31)
    }
    struct STATUS {
        static let PENDING = "1"
        static let ACCEPTED = "2"
        static let CANCELED = "3"
        static let ONTRIP = "4"
        static let COMPLETED = "5"
        static let ONPICKUP = "6"
        static let EXPIRED = "7"

    }
    
    struct MESSAGE {
        static let locationError = "Please enable your location settings.  Go Setting->Privacy->Location Services->LUX "
        static let k_password_updated = "Password Updated. Please Login to continue."
        static let k_app_name = "LUX"
        static let k_confirm_title = "Confirmation"
        static let k_cancelled_ride = "Your booking is cancelled."
        static let k_email_Invalid = "Please enter valid email."
        static let k_email_Empty = "Please enter your email."
        static let k_name_Empty = "Please enter your name."
        static let k_name_Invalid = "Please enter valid name."
        static let k_pass_length = "Password should be minimum 6 & maximum 20 character long."
        static let k_pass_Empty = "Please enter a password."
        static let k_new_pass_Empty = "Please enter a new password."
        static let k_pass_2_Empty = "Please re-enter password."
        static let k_pass_Match = "Password doesn't match."
        static let k_current_Pass_Empty = "Please enter current password"
        static let k_cancelRide = "Are you sure you want to cancel the Ride?"
        static let k_cancel_booking = "Are you sure you want to cancel the booking?"
        static let k_cancelled_booking = "Your booking is cancelled."
        static let k_expired_booking = "Your booking has expired."
        static let k_new_Pass_Empty = "Please enter new password."
        static let k_Pass_Valid = "Please enter valid password."
        static let k_current_Pass_Valid = "Please enter valid current password."
        static let k_invalid_New_Pass = "Current and new password cannot be same."
        static let k_old_pass_Empty = "Please enter old password."
        static let k_confirm_pass_Empty = "Please enter confirm password."
        static let k_dob_placeholder = "    DD/MM/YYYY"
        static let k_provide_rating = "Please provide rating."
        static let k_provide_rideType = "Please select the ride type."
        static let k_leave_comment = "Please leave a comment."
        static let k_rating_done = "Thanks for rating."
        static let k_mob_Invalid = "Please enter valid mobile no."
        static let k_mob_Empty = "Please enter your mobile no."
        static let k_dob_Empty = "Please enter your DOB."
        static let k_state_Empty = "Please enter your state."
        static let k_address_Empty = "Please enter your address."
        static let k_country_Empty = "Please enter your country."
        static let k_city_Empty = "Please enter your city."
        static let k_zipcode_Empty = "Please enter zipcode."
        static let k_country_Invalid = "Please enter valid country."
        static let k_city_Invalid = "Please enter valid city."
        static let k_state_Invalid = "Please enter valid state."
        static let k_zipcode_Invalid = "Please enter valid zipcode."
        static let k_terms_accept = "Please accept Terms and Conditions."
        static let k_address_Valid = "Please enter a valid address."
        static let k_forgotPassLink = "A new generated password has been sent to your email."
        static let k_Something_wrong = "Sorry! Sonething went wrong."
        static let k_Server_Problem = "Server isn't responding. Please try again!"
        static let k_network_error = "No Internet Connection.Retry!"
        static let k_logout = "Are you sure you want to logout?"
        static let k_mailInvoice = "Mail invoice has been sent. Please check your mail."
        static let k_paymentDone = "Payment has been done successfully."
        static let k_paymentFailure = "Payment failure."
        static let k_cancellationPayment = "Are you sure ,You want to cancel your ride? if you cancel , you have to pay the cancellation charge of Amount: "
        static let k_cardEmpty = "Please enter a card number"
        static let k_expiryEmpty = "Please enter a expiry date"
        static let k_cvvEmpty = "Please enter a cvv"
        
        static let k_cardValid = "Please enter a valid card number"
        static let k_expiryValid = "Please enter a valid expiry date"
        static let k_cvvValid = "Please enter a valid CVV"

        


    }
}
