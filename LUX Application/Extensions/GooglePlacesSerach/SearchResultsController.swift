//
//  SearchResultsController.swift
//  PlacesLookup
//
//  Created by Malek T. on 9/30/15.
//  Copyright © 2015 Medigarage Studios LTD. All rights reserved.
//

import UIKit

protocol LocateOnTheMap{
    func locateWithLongitude(lon:Double, andLatitude lat:Double, andTitle title: String)
}

class SearchResultsController: UITableViewController {

    var searchResults: [String]!
    var delegate: LocateOnTheMap!
    var appDelegate : AppDelegate!


    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.appDelegate = UIApplication.shared.delegate as? AppDelegate
        self.searchResults = Array()
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cellIdentifier")

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return self.searchResults.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier", for: indexPath as IndexPath)
        cell.textLabel?.text = self.searchResults[indexPath.row]
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            self.dismiss(animated: true, completion: nil)
            // 2
            let correctedAddress:String! = self.searchResults[indexPath.row].addingPercentEncoding(withAllowedCharacters: .symbols)
        let url = NSURL(string: "https://maps.googleapis.com/maps/api/geocode/json?address=" + correctedAddress + "&key=" + K.KEY.GoogleAPIKey)
            print(url)
            let task = URLSession.shared.dataTask(with: url! as URL) { (data, response, error) -> Void in
                // 3
                do {
                    if data != nil{
                        let dic = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves) as! NSDictionary
                    //    print("results****",dic["results"])
                        let lat = (((((dic["results"] as AnyObject).value(forKey: "geometry") as AnyObject).value(forKey: "location") as AnyObject).value(forKey: "lat") as AnyObject)as! NSArray)[0] as! Double
                        let lon = (((((dic["results"] as AnyObject).value(forKey: "geometry") as AnyObject).value(forKey: "location") as AnyObject).value(forKey: "lng") as AnyObject) as! NSArray)[0] as! Double
                        // 4
                        self.delegate.locateWithLongitude(lon: lon, andLatitude: lat, andTitle: self.searchResults[indexPath.row])
//                        if(self.appDelegate.isOriginOrDest == "Origin")
//                        {
//                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateOriginAddress"), object: self.searchResults[indexPath.row])
//
//                        }
//                        if(self.appDelegate.isOriginOrDest == "Destination")
//                        {
//                           NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateDestinationAddress"), object: self.searchResults[indexPath.row])
//                        }
                       

                    }
                 
                }catch {
                    print("Error")
                }
            }
            // 5
            task.resume()
    }
    
    
    func reloadDataWithArray(array:[String]){
        self.searchResults = array
        self.tableView.reloadData()
    }

  
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
